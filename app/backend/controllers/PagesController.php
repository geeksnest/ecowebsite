<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PagesController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
    
    public function managepagesAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
		
    }
    public function createpageAction()
    {
   
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		
    }


    public function editpageAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }


    public function otherpagesAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

}

