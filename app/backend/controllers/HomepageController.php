<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;


class HomepageController extends ControllerBase
{

    public function indexAction(){

    }

    public function createhomepageAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function managehomepageAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function edithomepageAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function toppageAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

