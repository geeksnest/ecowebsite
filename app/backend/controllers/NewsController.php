<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class NewsController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function createnewsAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function managenewsAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
		
    }

    public function editnewsAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function categoryAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }


    public function createauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function manageauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function newstagsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}

