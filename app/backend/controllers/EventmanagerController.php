<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class EventmanagerController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function createeventAction()
    {
        $this->view->defaultreceipt = "<p>Hi,&nbsp;[name]<br />
                                    <br />
                                    Thank you for your contribution to [title]. This letter serves as your transaction receipt.<br />
                                    <br />
                                    Here&rsquo;s your contribution info:<br />
                                    <br />
                                    Transaction ID: [TransactId]<br />
                                    <br />
                                    Payment Type: [pay_type]<br />
                                    <br />
                                    Amount: $ [amount]<br />
                                    <br />
                                    Date: [date]</p>";
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

    public function eventlistAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
        
    }

    public function eventcontentsAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
        
    }

    public function eventscollectionAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
		
    }

   


}

