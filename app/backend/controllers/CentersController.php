<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class CentersController extends ControllerBase
{

    public function intialize(){

    }
    public function centersAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

