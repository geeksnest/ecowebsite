<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;
use Modules\Backend\Models\Roles as Roles;
use Modules\Backend\Models\Userroles as Userroles;
use Modules\Backend\Models\Members as Members;
use Modules\Backend\Models\Profimg as Profimg;

class UsersController extends ControllerBase {

    public function indexAction() {
        
    }

    public function createAction() {
        $dlttemp = Profimg::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
                // $data = array('success' => 'Photo has Been deleted');
            }
        }
        
        $form = new CreateuserForm();
        $this->view->form = $form;
        $tblroles = Roles::find();
        $this->view->tblroles = $tblroles;
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function edituserAction() {
        $form = new CreateuserForm();
        $this->view->form = $form;

        $tblroles = Roles::find();
        $this->view->tblroles = $tblroles;
        $this->view->ulevel = $this->session->get('ulevel');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function createSaveAction() {
        $img = '';
        $form = new CreateuserForm();
        $data = array();
        
        $getImage= Profimg::find();
        foreach ($getImage as $getImages) {
            $img=$getImages->img;
        }

        if (isset($_POST['username'])) {
            
            $roles = array();
            $roles['calendarrole'] = $calendarrole = $_POST['calendarrole'];
            $roles['donationsrole'] = $donationsrole = $_POST['donationsrole'];
            $roles['imagesrole'] = $imagesrole = $_POST['imagesrole'];
            $roles['newslettersrole'] = $newslettersrole = $_POST['newslettersrole'];
            $roles['newsrole'] = $newsrole = $_POST['newsrole'];
            $roles['pagesrole'] = $pagesrole = $_POST['pagesrole'];
            $roles['pearmarksrole'] = $pearmarksrole = $_POST['pearmarksrole'];
            $roles['projectsrole'] = $projectsrole = $_POST['projectsrole'];
            $roles['proposalesrole'] = $proposalesrole = $_POST['proposalesrole'];
            $roles['testimonialsrole'] = $testimonialsrole = $_POST['testimonialsrole'];
            $roles['usersrole'] = $usersrole = $_POST['usersrole'];
            if (empty($calendarrole) && empty($donationsrole) && empty($imagesrole) && empty($newslettersrole) && empty($newsrole) && empty($pagesrole) && empty($pearmarksrole) && empty($projectsrole) && empty($proposalesrole) && empty($testimonialsrole) && empty($usersrole)) {
                $data['error']= "<div class='label label-danger'>At least one role should be selected.</div>";
                
             } else {
                //if ($form->isValid($_POST) != false) {
                    $userName = Users::findFirst("username='" .  $_POST['username'] . "'");
                    $userEmail = Users::findFirst("email='" . $_POST['emailaddress'] . "'");
                    if ($userName == true || $userEmail == true) {
                        ($userName == true) ? $data["usernametaken"] = "Username already taken." : '';
                        ($userEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
                    } else {
                        $user = new Users();
                        $password = sha1($_POST['password']);
                        $user->assign(array(
                            'username' => $_POST['username'],
                            'email' => $_POST['emailaddress'],
                            'password' => $password,
                            'hdyha' => $_POST['firstname'],
                            'referal' => 'superagent',
                            'firstname' => $_POST['firstname'],
                            'lastname' => $_POST['lastname'],
                            'birthday' => $_POST['birthday'],
                            'gender' => $_POST['gender'],
                            'state' => $_POST['state'],
                            'country' => 'country',
                            'agentType' => 'Semi Admin',
                            'userLevel' => '2',
                            'img' => $img
                        ));
                        if (!$user->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "User profile has been stored.";
                            $this->_insertRoles($user->userid, $roles);
                        }
                    }
//                } else {
//                    $data['error'] = "Something went wrong in validating the data, please try again.";
//                }
            }
        }
        echo json_encode($data);

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
    }
    private function _insertRoles($id, $roles){
        //Delete if there is existing
        $this->modelsManager->executeQuery('DELETE FROM Modules\Backend\Models\Userroles WHERE userID = "'.$id.'"');
        // Inserting using placeholders
        $phql = "INSERT INTO Modules\Backend\Models\Userroles ( userID, userRoles) "
                . "VALUES (:userid:, :role:)";
        foreach($roles as $r => $v){
            if(!empty($v)){
            $this->modelsManager->executeQuery($phql,
                array(
                    'userid'     => $id,
                    'role' => $r,
                    )
                );        
            }
        }     
    }
    public function listAction() {


        $this->view->sessionUserID = $this->session->get('userid');

        $userName = Users::findFirst("userid='" .  $this->session->get('userid') . "'");
        
        if($userName->userLevel == 2){
            $this->view->listnum = 1;
        }else{
            $this->view->listnum = 0;
        }
        
        $numberPage = $this->request->getQuery("page", "int");

        $builder = $this->modelsManager->createBuilder()
                ->columns('username , userid, email, firstname, lastname, userLevel, agentType')
                ->from('Modules\Backend\Models\Users');

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $builder,
            "limit" => 10,
            "page" => $numberPage
        ));
        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        $list = array();
        foreach ($page->items as $item) {
            $list[] = array(
                'username' => $item->username,
                'userid' => $item->userid,
                'email' => $item->email,
                'firstname' => $item->firstname,
                'lastname' => $item->lastname,
                'userLevel' => $item->userLevel,
                'agentType' => $item->agentType
            );
        }
        $data['users'] = $list;
        $this->view->userLevel = $item->userLevel;

        

        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'user');
        }
        $data['pages'] = $p;
        $data['index'] = $page->current;
        $this->view->data = json_encode($data);
        $this->view->ulevel = $this->session->get('ulevel');
        //echo json_encode($data);
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function membersAction() {
        $members = Members::find();
        $data = array();
        foreach ($members as $m) {
            $data[] = array(
                'username' => $m->username,
                'firstname' => $m->firstname,
                'lastname' => $m->lastname,
                'email' => $m->email,
                'birthday' => $m->birthday,
                'location' => $m->location,
                'zipcode' => $m->zipcode
            );
        }
        $this->view->data = str_replace("\\/", "/", json_encode($data));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function testAction() {
        echo 'test';
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
