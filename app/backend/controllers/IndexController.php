<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as Users;
use Modules\Backend\Models\Roles as Tblroles;
use Modules\Backend\Models\Userroles as Tbluserroles;

class IndexController extends ControllerBase
{
    public function initialize()
    {
    	parent::initialize();
    }
    public function logoutAction(){
        session_destroy();
        $this->flash->warning('You have successfully logout.');
        $this->view->pick("index/index");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function indexAction($logout=1)
    {

        $auth = $this->session->get('auth');

        if ($auth){
            $this->response->redirect('ecoadmin/admin');
        }

		$this->view->error = null;
		if ($this->request->isPost()) {
			$username = $this->request->getPost('username');
			$password = $this->request->getPost('password');
			//$hashpass = $this->security->hash($password);
            $user = Users::findFirst("username='".$username."' AND status=1");
            if($user){
                $shapass = sha1($password);
                if($shapass == $user->password){
                    $this->_registerSession($user);
                    $this->response->redirect('ecoadmin/admin');
                }else{
                    $this->flash->warning('Wrong Username/Password');
                }
            }else{
                $this->flash->warning('This account was disabled. Please contact your Administrator.');
            }

        }

    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    private function _registerSession($user)
    {
        $this->session->set('auth', array(
            'user_id' => $user->userid,
            'user_fullname' => $user->firstname .' '.$user->lastname,
            'user_username' => $user->username
        ));
        // $phql = 'SELECT * FROM Modules\Backend\Models\Userroles as Tbluserroles INNER JOIN Modules\Backend\Models\Roles as Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = "'.$user->userid.'"';
        // $result = $this->modelsManager->executeQuery($phql);
        $sessrole = array();
        $sesspage = array();

        if($user->userLevel == 1){
            $phql = 'SELECT * FROM Modules\Backend\Models\Roles as Tblroles ';
            $result = $this->modelsManager->executeQuery($phql);
            foreach($result as $r){
                $sessrole[] = $r->roleCode;
                $sesspage[$r->roleGroup] = explode(',', str_replace(' ', '', $r->rolePage));
            }
        }else{
            $phql = 'SELECT * FROM Modules\Backend\Models\Userroles as Tbluserroles WHERE userID = "'.$user->userid.'"';
            $result = $this->modelsManager->executeQuery($phql);
            foreach($result as $r){
                $sessrole[] = $r->userRoles;
                $sesspage[$r->roleGroup] = explode(',', str_replace(' ', '', $r->roleAccess));

            }
        }
        $roles = array('roles' => $sessrole, 'page' => $sesspage);
        $this->session->set('roles', $roles);
        $this->session->set('page', $roles);

        //Set SuperAdmin
        $this->session->set('SuperAdmin', true );
        $this->session->set('ulevel', $user->userLevel);
        $this->session->set('userid', $user->userid);
        $this->session->set('imgg', $user->img);

        if($user->userLevel==1){
            $this->session->set('MegaAdmin', true );
        }else{
            $this->session->set('MegaAdmin', false );
        }
    }
}

