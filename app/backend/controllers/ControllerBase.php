<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;
use Modules\Backend\Models\Users as Users;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
        $this->createJsConfig();
        
    	if(@$this->session->get('userid')){
    		$user = Users::findFirst("userid = '" . $this->session->get('userid') ."'");
            $this->view->fullname = $user->firstname.' '.$user->lastname;
            $this->view->agentType = $user->agentType;
            $this->view->imgg = $user->img;
        }
        
        $this->tag->setTitle("ECO Content Management System");
        $this->view->username = $this->session->get('auth');
        $this->view->userid = $this->session->get('userid');
        $this->view->base_url = $this->config->application->baseURL;
        $this->view->api_url = $this->config->application->apiURL;
        
    }
    private function createJsConfig(){
        $script="app.constant('Config', {
        // This is a generated Config
        // Any changes you make in this file will be replaced
        // Place you changes in app/config.php file \n";
        foreach( $this->config->application as $key=>$val){
            $script .= $key . ' : "' . $val .'",' . "\n";
        }
        $script .= "});";
        $fileName="../public/js/config/config.js";

        $exist =  file_get_contents($fileName);
        if($exist == $script){

        }else{
            @file_put_contents($fileName, $script);
        }

        $this->view->base_url = $this->config->application->baseURL;
    }
}