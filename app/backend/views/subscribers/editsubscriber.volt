{{ content() }}
<script type="text/ng-template" id="editMemberModal.html">
<form class="bs-example form-horizontal" name="form" novalidate>
<div class="modal-header"><h3 class="modal-title">Are You sure you want to delete this Photo?</h3></div>
<div class="modal-body" ng-show="process==true">
<p ng-show="success==false">Updating user profile record.</p>
<p ng-show="success==true">Success!</p>
</div>
<div class="modal-body" ng-show="process==false">ARE YOU SURE YOU WANT TO DELETE?</div>
<div class="modal-footer" ng-hide="process==true">                  
<button class="btn btn-default" ng-click="cancel()">Cancel</button>
<button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
</div>
</form>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Subscriber</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updatepage(page)" name="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">            
            <div class="panel-heading font-bold">Subscriber email</div>
            <div class="panel-body">
              <label class="col-sm-2 control-label"><label for="NMSemail">Update Email</label> </label>
              <div class="col-sm-10">
                <input type="text" id="NMSemail" name="NMSemail" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.NMSemail" required="required" ng-keyup = "onpagetitle(page.NMSemail)">
                <br>
              </div>
              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="checkbox">
                <label class="i-checks">
                  <input type="checkbox" ng-model='page.NMSstat'  ng-true-value="1" ng-false-value="0">
                  <i></i>Subscriber
                </label>
              </div>
              <div class="panel-body">
               <footer class="panel-footer text-right bg-light lter">
                <button type="button" class="btn btn-default" ui-sref="subscriberslist">Cancel</button>
                <button type="submit" class="btn btn-success" ng-disabled="form.$invalid">Submit</button>
              </footer>
            </div>
          </div>      
        </div>
      </div>  
    </div>
  </div>
</div>
</fieldset>