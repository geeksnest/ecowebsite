<?php echo $this->getContent(); ?>
<style type="text/css">
	.colorpicker-colorbox{
		margin-left: 5px;
    border-radius: 4px;
	}
	.width90px{
		width: 119px;
	}
	.pointer {
		cursor: pointer!important;
	}
</style>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Top page</h1>
	<a id="top"></a>
</div>
<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="row">
		<div class="panel-body">
			<tabset class="tab-container">
				<tab heading="Update 1st Box">
					<div class="panel-body">
						<div class="loader" ng-show="imageloader">
							<div class="loadercontainer">
								<div class="spinner">
									<div class="rect1"></div>
									<div class="rect2"></div>
									<div class="rect3"></div>
									<div class="rect4"></div>
									<div class="rect5"></div>
								</div>
								Uploading your images please wait...
							</div>
						</div>
						<div class="col-sm-12">
							<form ng-submit="Save(msg,files0)">
							<div class="row">
									<div class="form-group col-sm-12">
									<label for="title" class="pull-left col-sm-12">Background color</label>
										<div class="col-sm-12 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg.bgcolor" type="text" required>
										<div class="colorpicker-colorbox form-control pull-left" style="width:68px; background: {[{ msg.bgcolor }]}"> </div>
									</div>
									</div>
									<div class="col-sm-12">
									<div class="col-sm-1" for="title">Main Title</div>
									<div class="col-sm-5" style="text-align:left">
									<input type="text" ng-model="msg.title" required class="form-control">
									</div>
									</div>
							</div>
							<div class="col-sm-12 form-group" ng-show="imagecontent">
								<div class="col-sm-4" ng-repeat="p in projImg">
								<br> Row {[{ $index + 1}]}: 
								<div style="margin-bottom:20px" class="bg-light">
									&nbsp;&nbsp;<i class="fa fa-file-image-o"></i> Image
									<div class="row"> 
									    <div class="col-sm-12 create-proj-thumb" ng-if="num == $index">
											<alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeimgAlerts($index)"><span ng-bind="imgAlerts.msg"></span></alert>
										</div>

										<div class="col-sm-12 propic create-proj-thumb" ng-if="projImg[$index].image!=null && imageselected[$index] == false" style="text-align:center">
											<div class="line line-dashed b-b line-lg"></div>
											<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/toppage/{[{ projImg[$index].image }]}" ng-if="projImg[$index].image!=null"  style="width:160px;height:160px;background: {[{ msg.bgcolor }]}"> 
											<input type="hidden" ng-model="projImg[$index].image" required>
										</div>	

										<div class="col-sm-12 propic create-proj-thumb" ng-if="projImg[$index].image!=null && imageselected[$index] == true" style="text-align:center">
											<div class="line line-dashed b-b line-lg"></div>
											<img ngf-src="projImg[$index].files[0]" style="width:160px;height:160px;
											background: {[{ msg.bgcolor }]}">
											<input type="hidden" ng-model="projImg[$index].files[0]">
										</div>
										<div class="col-sm-12 propic create-proj-thumb">
											<fieldset class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare($index,files0)" ngf-select ng-model="files0" ngf-multiple="false"  ng-disabled="loadingg == true">
												<a href="">Choose an image from your computer</a><br>
												<label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
												<label>At least 1024x768 pixels</label>

											</fieldset>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input id="title" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="projImg[$index].title" name="title" ng-disabled="loadingg == true" placeholder="Title" required />
								</div>
								<div class="form-group">
									<input id="amount" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="projImg[$index].amount" only-digits name="amount" ng-disabled="loadingg == true" placeholder="Total Number" required />
								</div>
								<div class="form-group">
									<textarea id="desc" name="desc" class="form-control textarea ng-pristine ng-invalid ng-valid-pattern" ng-model="projImg[$index].desc" placeholder="Image ALT" required></textarea>
								</div>
							</div>
							<!-- <div class="col-sm-4">
								<br> Row 2:
								<div style="margin-bottom:20px" class="bg-light">
									&nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
									<div class="row"> 
									<div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
											<alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlerts($index)"><span ng-bind="imgAlerts.msg"></span></alert>
										</div>
										<div class="col-sm-12 propic create-proj-thumb" ng-if="imageselected == true">
											<div class="line line-dashed b-b line-lg"></div>
											<img ngf-src="projImg[1][0]" style="width:100%" ng-if="imageselected == true">
										</div>
										<div class="col-sm-12 propic create-proj-thumb">
											<fieldset class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(1,files)" ngf-select ng-model="files" ngf-multiple="false" required="required" ng-disabled="loadingg == true">
												<a href="">Choose an image from your computer</a><br>
												<label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
												<label>At least 1024x768 pixels</label>

											</fieldset>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1" ng-disabled="loadingg == true" placeholder="Title" />
								</div>
								<div class="form-group">
									<input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1" ng-disabled="loadingg == true" placeholder="Total Number" />
								</div>
								<div class="form-group">
									<textarea id="characteristics" name="char" class="form-control textarea ng-pristine ng-invalid ng-valid-pattern" ng-model="club.characteristics" placeholder="Short Description"></textarea>
								</div>
							</div>
							<div class="col-sm-4">
								<br> Row 3:
								<div style="margin-bottom:20px" class="bg-light">
									&nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
									<div class="row"> 
									<div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
											<alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlerts($index)"><span ng-bind="imgAlerts.msg"></span></alert>
										</div>
										<div class="col-sm-12 propic create-proj-thumb" ng-if="imageselected == true">
											<div class="line line-dashed b-b line-lg"></div>
											<img ngf-src="projImg[2][0]" style="width:100%" ng-if="imageselected == true">
										</div>
										<div class="col-sm-12 propic create-proj-thumb">
											<fieldset class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(2,files)" ngf-select ng-model="files" ngf-multiple="false" required="required" ng-disabled="loadingg == true">
												<a href="">Choose an image from your computer</a><br>
												<label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
												<label>At least 1024x768 pixels</label>

											</fieldset>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1" ng-disabled="loadingg == true" placeholder="Title" />
								</div>
								<div class="form-group">
									<input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1" ng-disabled="loadingg == true" placeholder="Total Number" />
								</div>
								<div class="form-group">
									<textarea id="characteristics" name="char" class="form-control textarea ng-pristine ng-invalid ng-valid-pattern" ng-model="club.characteristics" placeholder="Short Description"></textarea>
								</div>
							</div> -->
								

							</div>
							<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Update</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							

							
								<!-- <div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-2 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-right" style="width:68px; background: {[{ msg.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor" id="myeditor" ng-model="msg.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div> -->	
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update 2nd Box">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save1(msg1)">
								<div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-12 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg1.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-left" style="width:68px; background: {[{ msg1.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor1" id="myeditor1" ng-model="msg1.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update 3rd Box">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save2(msg2)">
								<div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-12 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg2.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-left" style="width:68px; background: {[{ msg2.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor2" id="myeditor2" ng-model="msg2.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</div>