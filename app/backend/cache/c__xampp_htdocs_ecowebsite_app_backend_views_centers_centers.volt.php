<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="manageCenter.html">
  <div ng-include="'/tpl/manageCenter.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Centers</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Center List
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" ng-click="search(searchtext)" type="button">Go!</button>
          </span>
        </div>
      </div>
      <div class="col-sm-1 m-b-xs">
        <div class="input-group">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
      <button class="btn m-b-xs btn-sm btn-success btn-addon pull-right" ng-click="manageCenter('Add','Add')"><i class="fa fa-plus"></i>Add Center</button>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>

            <th style="width:40%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'DESC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'ASC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="center in data.data">
            <td>
              {[{ center.centername }]}
            </td>
            <td>
              <a href="" ng-click="manageCenter(center.id,'Edit')"><span class="label bg-warning" >Edit</span></a>
              <a href="" ng-click="manageCenter(center.id,'Delete')"> <span class="label bg-danger">Delete</span>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-8">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small> 
        </div>
        <div class="col-sm-4">        
          <div class="input-group">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
</div>
</div>
