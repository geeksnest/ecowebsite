<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{

	protected function initialize(){
       
		$this->view->base_url = $this->config->application->baseURL;
        $this->view->api_url = $this->config->application->apiURL;
        $this->view->amazonlink = $this->config->application->amazonlink;
        $this->view->paypalURL = $this->config->application->paypalURL;
        $this->view->paypalAccount = $this->config->application->paypalAccount;

        

		// GOOGLE ANALYTICS
		$service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->script_google = $decoded->gscript;

	}   


    public function onConstruct(){

        // Get Menu Contents
        $MENU_SHORT_CODE = '[mainmenu]';
        $curl = curl_init($this->config->application->apiURL. '/menu/viewfrontendmenu/' .$MENU_SHORT_CODE );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->MENU_CONTENTS = $decoded->data;

        $this->view->mainheader = $_SERVER['DOCUMENT_ROOT'] . '/app/frontend/views/header.html';

        $this->view->base_url = $this->config->application->baseURL;
        $this->view->api_url = $this->config->application->apiURL;
        $this->view->amazonlink = $this->config->application->amazonlink;
        $this->view->paypalURL = $this->config->application->paypalURL;
        $this->view->paypalAccount = $this->config->application->paypalAccount;
        $this->view->ECObaseURL = 'https://earthcitizens.org/';
    }

    public function dbSelect($phql) {
        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare($phql);
        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $searchresult;
    }

    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= '<script type="text/javascript" src="'.$val.'"></script>';
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }

}