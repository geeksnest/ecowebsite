<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;
use Modules\Frontend\Models\Memberconfirmation as Memberconfirmation;
use Modules\Frontend\Models\Featuredprojects as Featuredprojects;
use Modules\Frontend\Models\Featuredphoto as Featuredphoto;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Donationloghouston as Donationloghouston;
use Modules\Frontend\Models\Donationlogseattle as Donationlogseattle;
use Modules\Frontend\Models\Donationlogothers as Donationlogothers;
use Modules\Frontend\Models\Donationlogothers1 as Donationlogothers1;
use Modules\Frontend\Models\Members as Members;
use Modules\Frontend\Models\Album as Album;
use Modules\Frontend\Models\Image as Image;
use Modules\Frontend\Models\Settings as Settings;
use Modules\Frontend\Models\Vendorlog as Vendorlog;
use Modules\Frontend\Models\Eventslog as Eventslog;
use Modules\Frontend\Models\Projectsdonations as Projectsdonations;
use Modules\Frontend\Models\Heroesleadershipcollection as Heroesleadershipcollection;

class IndexController extends ControllerBase {

    public function initialize() {

    }
    
    public function indexAction() { 

        $this->angularLoader(array(
            'featuredblogCtrl' => '/js/fr/scripts/controllers/featuredNews.js',            
            'configJS' => '/js/config/config.js',             
            )); 

    // $this->view->phpinfo = phpinfo();  

    $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {        
           return $this->response->redirect('maintenance/');
        // $this->view->message = $msg ;
        }

        // main slider        
        $curl = curl_init($this->config->application->apiURL.'/utility/mainslides');
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded;
        
        
        $service_url2 = $this->config->application->apiURL.'/utility/mainbanners';
        $curl = curl_init($service_url2);

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded2 = json_decode($curl_response);
        $this->view->bannerimgs = $decoded2->banners;
        

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';
        $curl = curl_init($service_url_news);


        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function donationAction() {
        $service_url = $this->config->application->apiURL. '/pages/getpage/2' ;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->title = $decoded->title;
        $this->view->title = $decoded->title;
        echo  $this->view->pageid = $decoded->pageid;
        $this->view->body = $decoded->body;

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  
        ///////////////////////////////////////////////////////////////////////////////////////////// 
        $data = "";
        $this->view->usersdonated = Donationlog::count(array("distinct" => "useremail"));
        $amount = Donationlog::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);
       
    }

public function confirmationAction($id, $code) {
    $confirm = Memberconfirmation::findFirst("members_id='" . $id . "' ");
    if (@$confirm->members_code === $code) {
        $member = Members::findFirst("userid='" . $id . "' ");
        $member->status = 1;
        $member->save();
        $confirm->delete();
        echo '<section"> SUCCESS</section>';
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->message = $msg ;
    }

    

    $service_url = $this->config->application->apiURL . '/pages/getpage/2' ;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->title = $decoded->title;
        $this->view->title = $decoded->title;
        $this->view->pageid = $decoded->pageid;
        $this->view->body = $decoded->body;

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

    }

public function donationcompleteAction() {

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/script';

        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded[0]->gscript;
        
        $pp_hostname = $this->config->application->paypalURL; // Change to www.sandbox.paypal.com to test against sandbox
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-synch';

        $tx_token = $_GET['tx'];
        $auth_token = "0rzCfhe1S-6EqhrSGQeDtLZFgmvCGocjHCoZQ-TmrmXWUBopdSou2ysltOe";
        $req .= "&tx=$tx_token&at=$auth_token";

        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        //set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
        //if your server does not bundled with default verisign certificates.
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
        $res = curl_exec($ch);
        curl_close($ch);
        if (!$res) {
            //HTTP ERROR
        } else {
            // parse the data
            $lines = explode("\n", $res);
            $keyarray = array();
            if (strcmp($lines[0], "SUCCESS") == 0) {
                for ($i = 1; $i < count($lines); $i++) {
                    list($key, $val) = explode("=", $lines[$i]);
                    $keyarray[urldecode($key)] = urldecode($val);
                }
                // check the payment_status is Completed
                // check that txn_id has not been previously processed
                // check that receiver_email is your Primary PayPal email
                // check that payment_amount/payment_currency are correct
                // process payment
                $firstname = $keyarray['first_name'];
                $lastname = $keyarray['last_name'];
                $customemail = $keyarray['custom'];
                //$itemname = $keyarray['item_name'];
                $amount = $keyarray['mc_gross'];

                $donate = new Donationlog();

                $donate->assign(array(
                    'useremail' => $customemail,
                    'transactionId' => $tx_token,
                    'datetimestamp' => date("Y-m-d H:i:s"),
                    'amount' => $amount
                    ));
                $donate->save();

                echo '	<section class="top-social emailconfirmation" id="main-navbarr" style="background-color: #528AC2; color: white">
                <div class="container top-nav clearfix">
                <div class="span12 address pull-left" style="text-align:center; padding-top: 7px;">
                THANK YOU ' . $firstname . ' ' . $lastname . '! WE HAVE RECEIVED YOUR DONATION! 
                </div>
                </div>
                </section>';
            } else if (strcmp($lines[0], "FAIL") == 0) {
                echo "FAIL! DUE TO TOKEN ERROR";
            }
        }
    }

    public function paypalipnAction() {
// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
        define("DEBUG", 0);
// Set to 0 once you're ready to go live
        // define("USE_SANDBOX", 1);
        // define("LOG_FILE", "./ipn.log");
// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post IPN data back to PayPal to validate the IPN data is genuine
        // Without this step anyone can fake IPN data
        //if (USE_SANDBOX == true) {
            $paypal_url = $this->config->application->paypalURL; //paypal URL saved on the app/config/config.php
        //} else {
        //$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
        // }
        $ch = curl_init($paypal_url);
        if ($ch == FALSE) {
            return FALSE;
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if (DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }
// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
// Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.
//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);
        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) { // cURL error
            if (DEBUG == true) {
                error_log("Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL);
            }
            curl_close($ch);
            exit;
        } else {
// Log the entire HTTP response if debug is switched on.
            if (DEBUG == true) {
                error_log("HTTP request of validation request:" . curl_getinfo($ch, CURLINFO_HEADER_OUT) . " for IPN payload: $req" . PHP_EOL);
                error_log("HTTP response of validation request: $res" . PHP_EOL);
            }
            curl_close($ch);
        }
// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));
        error_log(" ============================================================================================ " . PHP_EOL);
        if (strcmp($res, "VERIFIED") == 0) {
// check whether the payment_status is Completed
// check that txn_id has not been previously processed
// check that receiver_email is your PayPal email
// check that payment_amount/payment_currency are correct
// process payment and mark item as paid.
// assign posted variables to local variables
//$item_name = $_POST['item_name'];
//$item_number = $_POST['item_number'];
//$payment_status = $_POST['payment_status'];
//$payment_amount = $_POST['mc_gross'];
//$payment_currency = $_POST['mc_currency'];
//$txn_id = $_POST['txn_id'];
//$receiver_email = $_POST['receiver_email'];
//$payer_email = $_POST['payer_email'];
//                $firstname = $keyarray['first_name'];
//                $lastname = $keyarray['last_name'];
            $firstname = 'EFREN';
            $lastname = 'BAUTISTA';
            $customemail = $_POST['custom'];
            //$itemname = $keyarray['item_name'];
            $amount = $_POST['mc_gross'];



            $val = explode(' ', $customemail);

            if(isset($val[1])){
                if($val[1]=='nepal'){
                    $donate = new Donationlogothers();
                    $donate->assign(array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  'Nepal',
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    ));
                    $donate->save();
                }elseif($val[1]=='other'){
                    $donate = new Donationlogothers1();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  'Other',
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[2] != ''){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[2]);
                    }
                    if($val[3] != 'undefined' && $val[3] != ''){
                        $data['cname'] = str_replace("*", " ", $val[3]);
                    }
                    $donate->assign($data);

                    $donate->save();
                }elseif($val[1]=='houston'){
                    $donate = new Donationloghouston();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  'Earth Citizens Walk in Houston',
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[2] != ''){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[2]);
                    }
                    if($val[3] != 'undefined' && $val[3] != ''){
                        $data['cname'] = str_replace("*", " ", $val[3]);
                    }
                    $donate->assign($data);

                    $donate->save();
                }elseif($val[1]=='seattle'){
                    $donate = new Donationlogseattle();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  'Earth Citizens Walk in Seattle',
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[2] != ''){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[2]);
                    }
                    if($val[3] != 'undefined' && $val[3] != ''){
                        $data['cname'] = str_replace("*", " ", $val[3]);
                    }
                    $donate->assign($data);

                    $donate->save();
                
                }elseif($val[1]=='events'){

                    $donate = new Eventslog();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  $val[2],
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[3] != ''){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[3]);
                    }
                    if($val[4] != 'undefined' && $val[4] != ''){
                        $data['cname'] = str_replace("*", " ", $val[4]);
                    }
                    $donate->assign($data);

                    $donate->save();
                
                }elseif($val[1]=='projects'){

                    $donate = new Projectsdonations();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'donatedto' =>  $val[2],
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[3] != ''){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[3]);
                    }
                    if($val[4] != 'undefined' && $val[4] != ''){
                        $data['cname'] = str_replace("*", " ", $val[4]);
                    }
                    $donate->assign($data);

                    $donate->save();
                
                }elseif($val[1]=='hlt'){
                    $donate = new Heroesleadershipcollection();
                    $data = array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $_POST['mc_gross1'],
                        'trainingDate' =>  str_replace("*", " ", $val[4]),
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                    );
                    if($val[2] != 'null'){
                        $data['howdidyoulearn'] = str_replace("*", " ", $val[3]);
                    }
                    if($val[3] != 'null'){
                        $data['cname'] = str_replace("*", " ", $val[3]);
                    }
                    $donate->assign($data);

                    $donate->save();
                
                }elseif($val[1]=='seattleexpovendor'){


                    if($val[2] != ''){ $orgname = str_replace("*", " ", $val[2]); }
                    if($val[3] != ''){ $expooffer = str_replace("*", " ", $val[3]); }
                    if($val[4] != ''){ $contactperson = str_replace("*", " ", $val[4]); }
                    if($val[5] != ''){ $phone = str_replace("*", " ", $val[5]); }
                    if($val[6] != ''){ $email = str_replace("*", " ", $val[6]); }
                    if($val[7] != ''){ $address = str_replace("*", " ", $val[7]); }
                    if($val[8] != ''){ $table = str_replace("*", " ", $val[8]); }
                    if($val[9] != ''){ $totalpayment = str_replace("*", " ", $val[9]); }
                    if($val[10] != ''){ $electricity = str_replace("*", " ", $val[10]); }
                    if($val[11] != ''){ $vendortype = str_replace("*", " ", $val[11]); }
                    if($val[12] != ''){ $howdidyoulearn = str_replace("*", " ", $val[12]); }
                    if($val[13] != 'undefined' && $val[13] != ''){ $cname = str_replace("*", " ", $val[13]); }

                    if($_POST['mc_gross'] != null){
                        $amount1 = $_POST['mc_gross'];
                    }else{
                        $amount1 = $_POST['mc_gross1'];
                    }


                    $vendor = new Vendorlog();

                    $vendor->assign(array(
                        'id' => $_POST['txn_id'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'orgname' => $orgname,
                        'expooffer' => $expooffer,
                        'contactperson' => $contactperson,
                        'phone' => $phone,
                        'email' => $email,
                        'address' => $address,
                        'vendortype' => $vendortype,
                        'table' => $table,
                        'electricity' => $electricity,
                        'totalpayment' => $amount1,
                        'paymentmethod' => $vendortype,
                        'amount' => $amount1,
                        'paymentmode' => 'Paypal',
                        'ccemail' => $_POST['payer_email'],
                        'billinginfofname' => $_POST['first_name'],
                        'billinginfolname' => $_POST['last_name'],
                        'typeoftransaction' => 'SEATTLE NATURAL HEALING EXPO VENDORS',
                        'howdidyoulearn' => $howdidyoulearn,
                        'cname' => $cname,
                        'status' => 1
                        ));
                    $vendor->save();
                }elseif($val[1]=='icare'){
                    $donate = new Donationlog();
                    $donate->assign(array(
                        'useremail' => $_POST['payer_email'],
                        'transactionId' => $_POST['txn_id'],
                        'datetimestamp' => date("Y-m-d H:i:s"),
                        'amount' => $amount,
                        'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                        'paymentmode' => 'Paypal'
                        ));
                    if($donate->save()){

                        $curl = curl_init( $this->config->application->apiURL. '/reg/paypalreg/'.$val[0]);

                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
                        $curl_response = curl_exec($curl);

                        if ($curl_response === false) 
                        {
                            $info = curl_getinfo($curl);
                            curl_close($curl);
                            die('error occured during curl exec. Additional info: ' . var_export($info));
                        }

                        curl_close($curl);

                        $decoded = json_decode($curl_response);

                    } 
                }

            }
            else{
                $donate = new Donationlog();
                $donate->assign(array(
                    'useremail' => $val[0],
                    'transactionId' => $_POST['txn_id'],
                    'datetimestamp' => date("Y-m-d H:i:s"),
                    'amount' => $amount,
                    'billinginfofname' => $_POST['first_name'].' '.$_POST['last_name'],
                    'paymentmode' => 'Paypal'
                ));
                
                $donate->save();
            }

            //if (DEBUG == true) {
            //    error_log("Verified IPN: $req " . PHP_EOL);
            //}
        } 
        else if (strcmp($res, "INVALID") == 0) {
            // log for manual investigation
            // Add business logic here which deals with invalid IPN messages
            //if (DEBUG == true) {
           var_dump("Invalid IPN: $req" . PHP_EOL);
            //}
        }
    }

    public function maintenanceAction() {   


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function page404Action() { 

        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {        
            return $this->response->redirect('maintenance/');
        }

     // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';
        $curl = curl_init($service_url_news);


        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        
        $this->view->script_google = $decoded->gscript;


        $this->view->donation = 1;

    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 }

    public function comingsoonAction() { 

        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {        
            return $this->response->redirect('maintenance/');
        }

     // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';
        $curl = curl_init($service_url_news);


        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        
        $this->view->script_google = $decoded->gscript;


        $this->view->donation = 1;
        $this->view->news = 1;

    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 }

}
