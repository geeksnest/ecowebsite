<?php

namespace Modules\Frontend\Controllers;

class ProposalController extends ControllerBase
{

	public function proposalAction()
	{
		$service_url = $api_url;

// Getting a request instance
$request = new \Phalcon\Http\Request();

// Check whether the request was made with method POST

		$var1 = $_POST['name']
		$array = array(
			"name" => $_POST['name'],
			'email' => $_POST['email'],
			'company' => $_POST['company'],
			'proposal_body' => $_POST['proposal_body']
			)
		$submitJson = JSON_ENCODE($array);
		
		// GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;
        
	}

}