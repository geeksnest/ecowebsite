<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;
use Modules\Frontend\Models\Memberconfirmation as Memberconfirmation;
use Modules\Frontend\Models\Members as Members;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Settings as Settings;


class IcareController extends ControllerBase {

    public function initialize() {
        
    }
    public function indexAction() {

        $this->angularLoader(array(
            'iCareCtrl' => '/js/fr/scripts/controllers/icare/index.js',             
            'iCareRegistrationCtrl' => '/js/fr/scripts/controllers/icare/iCareRegistrationCtrl.js',             
            'iCareDonateCtrl' => '/js/fr/scripts/controllers/icare/iCareDonateCtrl.js',             
            'configJS' => '/js/config/config.js',             
        )); 

        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
             
           return $this->response->redirect('maintenance/');
        }      

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);      
        $this->view->script_google = $decoded->gscript;


        $this->view->donation = 1; 
        $this->view->metatitle = 'ICARE2020';
        $this->view->metadesc = 'Create substantial changes by 2020 to change the world and save the Earth by building an Extensive Network of 10,000 Organizations in more than 100 Countries, 1 Million Leaders, 100 Million Earth Citizens (1% of the World Population) and 1 Trillion Dollar Fund (1% of the World Money) dedicated to the cause of Recovering the Earth.';
        $this->view->metacat = 'ICARE2020';
        $this->view->metatag = 'ICARE2020';
        $this->view->metakeyword = 'ICARE2020';


        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function registeredAction() {

        $this->angularLoader(array(
            'iCareCtrl' => '/js/fr/scripts/controllers/icare/index.js',             
            'iCareRegistrationCtrl' => '/js/fr/scripts/controllers/icare/iCareRegistrationCtrl.js',             
            'iCareDonateCtrl' => '/js/fr/scripts/controllers/icare/iCareDonateCtrl.js',             
            'configJS' => '/js/config/config.js',             
        )); 

        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
             
           return $this->response->redirect('maintenance/');
        }      

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);      
        $this->view->script_google = $decoded->gscript;


        $this->view->donation = 1; 
        $this->view->metatitle = '';
        $this->view->metadesc = '';
        $this->view->metacat = '';
        $this->view->metatag = '';
        $this->view->metakeyword = '';


        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    

}
