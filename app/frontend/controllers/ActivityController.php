<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;
use Modules\Frontend\Models\Featuredprojects as Featuredprojects;
use Modules\Frontend\Models\Calendar as Calendar;
use Modules\Frontend\Models\Pages as Pages;
use Modules\Frontend\Models\News as News;

class ActivityController extends ControllerBase
{
	public function initialize() {
    }

    public function calendarAction($pageslugs) {

    	// activity lists -------------------------------------------------------------
    	$service_url = $this->config->application->apiURL.'/calendar/listview';
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->viewcalendar = $decoded;
        $this->view->disonli = 2;

    	// page slugs -------------------------------------------------------------
        $service_url = $this->config->application->apiURL.'/pages/getpage/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->title = $decoded->title;
        $this->view->pageid = $decoded->pageid;
        $this->view->pageslugs = $decoded->pageslugs;
        $this->view->pagefeatures = $decoded->pagefeatures;
        $this->view->body = $decoded->body;

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
                
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;
    }

    public function viewerAction($pageid, $pageslugs) {

        $service_url = $this->config->application->apiURL.'/calendar/editactivity/' . $pageid;
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        
        // $this->view->actid = $decoded->pageid;
        $this->view->acttitle = $decoded->title;
        $this->view->actloc = $decoded->loc;
        $this->view->df = $decoded->dfrom;
        $this->view->dt = $decoded->dto;
        $this->view->mt = $decoded->mytime2;
        $this->view->actdesc = $decoded->body;
        $this->view->actinfo = $decoded->shortdesc;
        $this->view->actbanner = $decoded->banner;
        $this->view->disonli = 2;

        // get pageid
        $service_url = $this->config->application->apiURL.'/pages/getpageproject';        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->title = $decoded->title;
        $this->view->pageid = $decoded->pageid;
        $this->view->pageslugs = $decoded->pageslugs;
        $this->view->pagefeatures = $decoded->pagefeatures;
        $this->view->body = $decoded->body;

        // page slugs
        $service_url = $this->config->application->apiURL.'/pages/getpage/'. $pageid . '/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;
    }
}

