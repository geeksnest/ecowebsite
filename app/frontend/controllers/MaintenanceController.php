<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;
use Modules\Frontend\Models\Memberconfirmation as Memberconfirmation;
use Modules\Frontend\Models\Members as Members;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Settings as Settings;

class MaintenanceController extends ControllerBase {

    public function initialize() {

    }
    public function indexAction() {

        date_default_timezone_set('America/Los_Angeles');
        $date = gmdate('Y m d H i s');

         $date_arr= explode(" ", $date);
         // $expiretime = $date_arr[0] ."-". $date_arr[1] ."-". $addday ." ". $add .":". $date_arr[4] .":". $date_arr[5];

         $expiretime = $date_arr[0] ."-". $date_arr[1] ."-". $date_arr[2] ." ". $date_arr[3].":". $date_arr[4] .":". $date_arr[5];
         $expiretimenow = $date_arr[0] ."". $date_arr[1] ."". $date_arr[2] ."". $date_arr[3]."". $date_arr[4];


            $setting = Settings::findFirst("id=" . 1);

            $timeexpire = explode(" ", $setting->value4);
            $timeexpire0 = explode("-", $timeexpire[0]);
            $timeexpire1 = explode(":", $timeexpire[1]);

            if($timeexpire1[0] < 10){
                $hournow = "0".$timeexpire1[0];
            }else{
                $hournow = $timeexpire1[0];
            }

            $expiretimeset = $timeexpire0[0] ."". $timeexpire0[1] ."". $timeexpire0[2] ."". $hournow ."". $timeexpire1[1];




            if ($expiretimeset <= $expiretimenow) {

             $setting->value1 = 0;

            }


            // if ($setting->value4 <= $expiretime) {

            //  $setting->value1 = 0;

            // }
           
            
            if (!$setting->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";                
            }  
            echo json_encode($setting);



        $page = Settings::findFirst("id=1");
        if ($page->value1 == 0) {
             
           return $this->response->redirect( $this->view->base_url );
        // $this->view->message = $msg ;
        }
        $this->view->value1 = $page->value1;
        $this->view->value2 = $page->value2;
         $this->view->value3 = $page->value3;
         $this->view->value4 = $page->value4;
         $this->view->value5 = $page->value5;
         $this->view->client_timezone = $page->client_timezone;


         // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;
             



        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    } 



}
