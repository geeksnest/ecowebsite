<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;
use Modules\Frontend\Models\Testimonial as Testimonial;
use Modules\Frontend\Models\Pages as Pages;

class TestimonialController extends ControllerBase {

    public function initialize() {

    }

     public function indexAction($pageslugs) {

        // PAGESLUGS
        $service_url = $this->config->application->apiURL.'/pages/getpage/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->title = $decoded->title;
        $this->view->pageid = $decoded->pageid;
        $this->view->pageslugs = $decoded->pageslugs;
        $this->view->pagefeatures = $decoded->pagefeatures;
        $this->view->body = $decoded->body;

        // TESTIMONIAL OFFSET
        $service_url = $this->config->application->apiURL.'/utility/testimonial/0';       
        $curl = curl_init($service_url);       
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);        
        if ($curl_response === false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->msginfo = $decoded;

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;
    }

}
