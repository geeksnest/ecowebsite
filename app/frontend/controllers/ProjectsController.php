<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;
use Modules\Frontend\Models\Featuredprojects as Featuredprojects;
use Modules\Frontend\Models\Calendar as Calendar;
use Modules\Frontend\Models\Pages as Pages;
use Modules\Frontend\Models\News as News;

class ProjectsController extends ControllerBase
{

 
    public function initialize() {
        $curl = curl_init($this->config->application->apiURL.'/projects/banner');

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
          echo curl_errno($curl);
          curl_close($curl);
          die();
      }
      $decoded2 = json_decode($curl_response);

      $this->view->banner = $decoded2[0]->img;
      $this->view->bannertitle = $decoded2[0]->title;
      $this->view->bannertext = $decoded2[0]->description;
      $this->view->titlefontsize = $decoded2[0]->titlefontsize;
      $this->view->descriptionfontsize = $decoded2[0]->descriptionfontsize;
      $this->view->showtext = $decoded2[0]->showtext;
      $this->view->tbgcolor = $decoded2[0]->tbgcolor;
      $this->view->tfcolor = $decoded2[0]->tfcolor;
      $this->view->dfcolor = $decoded2[0]->dfcolor;
      $this->view->btnname = $decoded2[0]->linkname;
      $this->view->btnlink = $decoded2[0]->linkpath;
      $this->view->news = 3;
      $this->view->button = true;

      //breadcrumbs
        $pageslugs='projects';
        $service_url = $this->config->application->apiURL.'/pages/breadcrumbs/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $bread = json_decode($curl_response);
        $this->view->parent = $bread->parent;
        $this->view->child = $bread->child;
        $this->view->parentname =  $this->view->parent->subname;
        $this->view->parentlink =  $this->view->parent->sublink;
        $this->view->childname =  $this->view->child->subname;
        $this->view->childlink =  $this->view->child->sublink;
    }
    
    public function signupAction() {
        $this->view->donation = 1;
        $this->view->project = 1;
    }
    public function loginAction() {


        $this->angularLoader(array(
            'LoginCtrl' => '/js/fr/scripts/controllers/projects/login.js',             
            'Login' => '/js/fr/scripts/factory/login.js',             
            'User' => '/js/fr/scripts/factory/user.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 1;
        $this->view->project = 1;
    }
    public function forgotpasswordAction() {
        $this->angularLoader(array(
            'LoginCtrl' => '/js/fr/scripts/controllers/projects/login.js',             
            'Login' => '/js/fr/scripts/factory/login.js',             
            'User' => '/js/fr/scripts/factory/user.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 1;
        $this->view->project = 1;
    }
    public function changepasswordAction($email,$token) {
         $this->view->email = $email;
        $this->view->token = $token;
        $this->angularLoader(array(
            'LoginCtrl' => '/js/fr/scripts/controllers/projects/login.js',             
            'Login' => '/js/fr/scripts/factory/login.js',             
            'User' => '/js/fr/scripts/factory/user.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 1;
        $this->view->project = 1;
    }

    public function indexAction() {
        $this->angularLoader(array(
            'createprojectCtrl' => '/js/fr/scripts/controllers/projects/index.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 2;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href>ECO Projects</a>";
    }

    public function viewAction() {
        $this->angularLoader(array(
            'createprojectCtrl' => '/js/fr/scripts/controllers/projects/view.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 2;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href>ECO Project</a>";
    }

    public function createprojectAction() {

        $this->angularLoader(array(
            'createprojectCtrl' => '/js/fr/scripts/controllers/projects/createproject.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 2;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href>My ECO Project</a>";
    }

    public function editprojectAction() {

        $this->angularLoader(array(
            'editprojectCtrl' => '/js/fr/scripts/controllers/projects/editproject.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 2;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href>My ECO Project</a>";
    }

    public function projectdonationAction() {

        $this->angularLoader(array(
            'donationProjCtrl' => '/js/fr/scripts/controllers/projects/projectdonation.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 2;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href>My ECO Project</a>";
    }

    
}

