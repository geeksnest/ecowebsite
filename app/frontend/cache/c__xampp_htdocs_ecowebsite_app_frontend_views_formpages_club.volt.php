<style type="text/css">
  .help-block,.p{
    font-size: 14px;
  }
  .n{
    font-size: 12px;
  }
</style>
      <div class="col-sm-12 create-form" ng-controller="Createclubctrl" style="padding-left: 0px;margin-top: 0px;"> 
        <!-- <form>       -->
        <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitclub(club,files)">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-success" ng-show="successget">
                <span class="" ng-bind="successget"></span>
              </div>
              <div class="form-group has-feedback">
               <!--  <input type="hidden" ng-init="club.createdby = userAgent" ng-model="club.createdby"/> -->
                Name of Club : <label class="req">*</label>
                <input id="name" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.name" name="clubname" ng-disabled="loadingg == true"/>
               
              </div>
              <div class="form-group has-feedback">
                Club Description <label class="req">*</label>
                <textarea id="description" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.description" name="description" ng-disabled="loadingg == true" placeholder="Make it engaging so that people get interested and join." style="height: 150px;"></textarea>
                
              </div>

               <div class="form-group has-feedback">
                Contact Person: <label class="req">*</label>
                <input id="contactperson" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.contactperson" name="contactperson" ng-disabled="loadingg == true"/>
              </div>
              
              <div class="form-group has-feedback">
                Contact Email Address: <label class="req">*</label>
                <input id="emailadd" type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.emailadd" name="email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-disabled="loadingg == true">
              </div>

              <div class="form-group has-feedback">
                Contact Phone Number: 
                <input id="contactnumber" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" ng-model="club.contactnumber" placeholder="(XXX) XXX XXXX" phone ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" only-digits name="contactnumber" ng-disabled="loadingg == true" />
              </div>

            
               <div class="form-group has-feedback">
                <br>  Geographic Area of Club Activities
              </div>
              <div style="padding:10px 0px 10px 30px">
              <div class="form-group has-feedback">
                Address 1 :
                <input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1" ng-disabled="loadingg == true" />
                
              </div>
              

             <div class="form-group has-feedback">
                Address 2 : 
                <input id="address2" type="text" class="form-control ng-pristine ng-invalid ng-valid-pattern" ng-model="club.address2" name="adress2" ng-disabled="loadingg == true">
               
              </div>
              

              <div class="form-group has-feedback">
                City : <label class="req">*</label>
                <input id="city" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.city" name="city" ng-disabled="loadingg == true" />
                
              </div>
              

              <div class="form-group has-feedback">
                State : <label class="req">*</label>
                <input id="state" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.state" name="state" ng-disabled="loadingg == true"/>
                
              </div>
              

              <div class="form-group has-feedback">
                Zip/Postal Code : <label class="req">*</label>
                <input id="postal" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.postal" name="zip" ng-disabled="loadingg == true" />
                
              </div>
              

              <div class="form-group has-feedback">
                Country :<label class="req">*</label>
                <select id="country" class="location form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.country" name="country" ng-disabled="loadingg == true">
                  <option ng-repeat="cn in countries" value="{[{cn.name}]}, {[{cn.code}]}">
                  {[{cn.name}]}, {[{cn.code}]} </option>
                </select>
              <label ng-show="nd && !club.country" class="control-label p" style="color:#a94442;" ng-cloak>
                 This field is required.</label>
              </div>
            
          </div>
              <div class="form-group has-feedback">
                <br> Club Characteristics
               <!--  <textarea id="characteristics" name="char" class="form-control textarea ng-pristine ng-invalid ng-valid-pattern" ng-model="club.characteristics"></textarea> -->
              </div>
              <div style="padding:10px 0px 10px 30px">
               <div class="form-group has-feedback">
               <div style="paddint-bottom:15px">
                    Member Age Range :
               </div>
              <div class="row">
                <div class="col-sm-2">
                  <input id="low" type="text" class="form-control numberonly ng-pristine ng-invalid ng-valid-pattern" ng-model="club.low" name="low"  only-digits maxlength="4" ng-disabled="loadingg == true" placeholder="Low">
                </div>
                <div class="col-sm-1 text-center"> - </div>
                 <div class="col-sm-2">
                 <input id="high" type="text" class="form-control numberonly ng-pristine ng-invalid ng-valid-pattern" ng-model="club.high" name="high"  only-digits maxlength="4" ng-disabled="loadingg == true"  placeholder="High">
                 </div>
                 <label ng-show="edge" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid Age Range.</label>
              </div>
                
                
              </div>

              <div class="form-group has-feedback">
                Members' Common Interests :
                <textarea id="interests" class="form-control textarea ng-pristine ng-invalid ng-valid-pattern" name="interests" ng-model="club.interests" ng-disabled="loadingg == true"
                placeholder="Less than 500 words" style="height:150px"></textarea>
              </div>
             <!--  <div class="form-group has-feedback">
                Date of Creation
                <div class="input-group">
                  <div class="row">
                    <div class="col-md-5">
                    <div class="col-xs-6" style="padding-left:0px;">
                       <span class="datelabel">Year: <label class="req">*</label></span>
                    <select ng-init="club.byear = year[0]" ng-model="club.byear" name="byear" class="form-control" ng-disabled="loadingg == true">
                      <?php for ($year= date('Y') ; $year >= 1900; $year--) { echo "<option value='".$year."'>".$year."</option>";}?>
                    </select>
                    <label ng-show="n && !club.byear" class="control-label p" style="color:#a94442;" ng-cloak>
                     This field is required.</label><input type="text" id="year" style="opacity:0;">
                    </div>
                    <div class="col-xs-6" style="margin-top: 25px;">
                      / (Optional)
                    </div>
                    </div>
                   <div class="col-md-6" style="margin-top: 7px;">

                    <div class="col-xs-6" style="padding-left:0px;">
                      <span class="datelabel">Month: </span>
                      <select  ng-model="club.bmonth" class="form-control ng-pristine ng-invalid ng-invalid-required" name="month" required="true" ng-disabled="loadingg == true">
                        <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                        <?php foreach ($formonths as $index => $formonth) { echo "<option value='".$index."'>".$formonth."</option>";}?>
                      </select>
                    </div>
                    <div class="col-xs-6" style="padding-right:0px;">
                      <span class="datelabel">Day:  </span>
                      <select ng-model="club.bday" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="day" required="true" ng-disabled="loadingg == true">
                        <?php for ($day= 1 ; $day < 31; $day++) { 
                          (strlen($day)==1)? $_day = trim("0".$day) : $_day = $day;
                          echo "<option value='".$_day."'>".$_day."</option>";
                        }?>
                      </select>
                    </div>
                  </div>
                </div>               
                 
                </div>
              </div> -->
              

              <div class="form-group has-feedback">
                Meeting plan : <label class="req">*</label>
                <textarea id="meeting" name="meeting" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.meeting" ng-disabled="loadingg == true" placeholder="Example: When are you going to start? How often are you going to meee? Where?"
                style="height:150px"></textarea>
              </div>
              

             <div class="form-group has-feedback">
                Website address :
                <input id="website" type="text" placeholder="http://" class="form-control ng-pristine ng-invalid ng-valid-pattern" ng-model="club.website" name="website" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" ng-disabled="loadingg == true">
                <label ng-show="form.website.$invalid && !form.website.$pristine && form.website.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
              </div>
              

             <div class="form-group has-feedback">
                Facebook page :
                <input id="facebook" type="text" class="form-control ng-pristine ng-invalid ng-valid-pattern" ng-model="club.facebook" name="facebook" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" ng-disabled="loadingg == true" />
                <label ng-show="form.facebook.$invalid && !form.facebook.$pristine && form.facebook.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
              </div>
              
               <div class="form-group">
                    <label>Club Profile Photo</label> <label class="req">*</label>
                    <div style="clear:both;"></div>
                    <div class="row col-sm-6"> 
                      <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                        <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlerts($index)"><span ng-bind="imgAlerts.msg"></span></alert>
                      </div>
                      <div class="col-sm-12 create-proj-thumb">
                        <div class="line line-dashed b-b line-lg"></div>                    
                        <img src="{[{base_url}]}/images/default_images/default_image.jpg" ng-if="imageselected == false">
                        <img ngf-src="projImg[0]" ng-if="imageselected == true">
                      </div>
                      <div class="col-sm-12 propic create-proj-thumb">
                        <fieldset class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required" ng-disabled="loadingg == true">
                          <a href="">Choose an image from your computer</a><br>
                          <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                          <label>At least 1024x768 pixels</label>

                        </fieldset>
                      </div>
                    </div>
                  </div>
               <input type="hidden"  ng-model="club.image" name="image" ng-value="club.image='dummy'" >
              </div>

               <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
               <span ng-bind="alert.msg"></span> </alert>
                <span>
                  We will review your information for registration. Once registered, you Club will be listed in our network information so that others can find you. For the registered clubs, ECO will provide systems for Club management and Networking and collaborating with other clubs.
                </span>

              <footer class="panel-footer text-right bg-light lter">
              <div class="col-md-12 center" ng-show="loadingg == true" ng-cloak>
                  <span ng-bind="loadingMsg"></span>
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                <button type="submit" ng-disabled="loadingg == true" class="btn btn-info">Submit</button>
              </footer>
              </div>
            </div>
          </div>
          </form>
      </div>
