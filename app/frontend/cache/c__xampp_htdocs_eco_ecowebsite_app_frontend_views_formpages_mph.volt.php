 <div class="container-fluid">
  <div class="eco-container">
    <div class="row padding-bot30 padding-top30" id="circle-links">
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/our-mission">
          <img src="/images/template_images/more-about-eco-circle.jpg" class="sublink-img">
          <p class="title1">Our Mission</p>
        </a>
      </div>
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/friends-and-allies">
          <img src="/images/template_images/partners-and-supporters-circle.jpg" class="sublink-img">
          <p class="title1">Friends and Allies</p>
        </a>
      </div>
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/how-to-start-change">
          <img src="/images/template_images/how-to-start-to-change-circle.jpg" class="sublink-img">
          <p class="title1">How to Start to Change</p>
        </a>
      </div>
    </div>
  </div>
</div>