<style type="text/css">
  .help-block,.p{
    font-size: 14px;
  }
  .n{
    font-size: 12px;
  }
</style>
      <div class="col-sm-12 create-form" ng-controller="Createclubctrl" style="padding-left: 0px;margin-top: 0px;"> 
        <!-- <form>       -->
        <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitclub(club,files)">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-success" ng-show="successget">
                <span class="" ng-bind="successget"></span>
              </div>
              <div class="form-group has-feedback">
               <!--  <input type="hidden" ng-init="club.createdby = userAgent" ng-model="club.createdby"/> -->
                Name of Club : <label class="req">*</label>
                <input id="name" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.name" name="clubname"/>
               
              </div>
              <div class="form-group has-feedback">
                Club Description <label class="req">*</label>
                <textarea id="description" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.description" name="description"></textarea>
                
              </div>
              

              <div class="form-group has-feedback">
                Address 1 : <label class="req">*</label>
                <input id="address1" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address1" name="address1"/>
                
              </div>
              

             <div class="form-group has-feedback">
                Address 2 : <label class="req">*</label>
                <input id="address2" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.address2" name="adress2" required />
               
              </div>
              

              <div class="form-group has-feedback">
                City : <label class="req">*</label>
                <input id="city" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.city" name="city"/>
                
              </div>
              

              <div class="form-group has-feedback">
                State : <label class="req">*</label>
                <input id="state" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.state" name="state"/>
                
              </div>
              

              <div class="form-group has-feedback">
                Zip/Postal Code : <label class="req">*</label>
                <input id="postal" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.postal" name="zip"/>
                
              </div>
              

              <div class="form-group has-feedback">
                Country :<label class="req">*</label>
                <select id="country" class="location form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.country" name="country">
                  <option ng-repeat="cn in countries" value="{[{cn.name}]}, {[{cn.code}]}">
                  {[{cn.name}]}, {[{cn.code}]}
                </select>
              <label ng-show="nd && !club.country" class="control-label p" style="color:#a94442;" ng-cloak>
                 This field is required.</label>
              </div>
              

              <div class="form-group has-feedback">
                Club Characteristics : <label class="req">*</label>
                <textarea id="characteristics" name="char" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.characteristics" required></textarea>
              </div>
              

              <div class="form-group has-feedback">
                Age : <label class="req">*</label>
                <input id="age" type="text" class="form-control numberonly ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.age" name="age"  only-digits maxlength="4"/>
              
              </div>
          
              <div class="form-group has-feedback">
                Common Interests : <label class="req">*</label>
                <textarea id="interests" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" name="interests" ng-model="club.interests"></textarea>
              </div>
              

              <div class="form-group has-feedback">
                Date Created
                <div class="input-group w-md">               
                  <span class="input-group-btn">
                    <input type="hidden" ng-model="club.datecreated">
                    <input id="datecreated" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="club.datecreated" is-open="opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" type="text" disabled>
                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon icon-calendar"></i></button>  
                  </span>
                </div>
                 <label ng-show="n && !club.datecreated" class="control-label p" style="color:#a94442;" ng-cloak>
                 This field is required.</label>
              </div>
              

              <div class="form-group has-feedback">
                Meeting plan : <label class="req">*</label>
                <textarea id="meeting" name="meeting" class="form-control textarea ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.meeting"></textarea>
              </div>
              

             <div class="form-group has-feedback">
                Website address : <label class="req">*</label>
                <input id="website" type="url" placeholder="http://" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.website" name="website" pattern="https?://.+">
              </div>
              

             <div class="form-group has-feedback">
                Facebook page : <label class="req">*</label>
                <input id="facebook" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.facebook" name="facebook"/>
              </div>
              

              <div class="form-group has-feedback">
                Contact Person: <label class="req">*</label>
                <input id="contactperson" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.contactperson" name="contactperson"/>
              </div>
              

              <div class="form-group has-feedback">
                Contact Phone Number: <label class="req">*</label>
                <input id="contactnumber" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" ng-model="club.contactnumber" placeholder="(XXX) XXXX XXX" phone ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" only-digits name="contactnumber"/>
              </div>
              

              <div class="form-group has-feedback">
                Contact Email Address: <label class="req">*</label>
                <input id="emailadd" type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="club.emailadd" name="email"/>
              </div>
               <div class="form-group">
                    <label>Club Profile Photo</label>
                    <div style="clear:both;"></div>
                    <div class="row col-sm-6"> 
                      <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                        <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)"><span ng-bind="imgAlerts.msg"></span></alert>
                      </div>
                      <div class="col-sm-12 create-proj-thumb">
                        <div class="line line-dashed b-b line-lg"></div>                    
                        <img src="{[{base_url}]}/images/default_images/default_image.jpg" ng-if="imageselected == false">
                        <img ngf-src="projImg[0]" ng-if="imageselected == true">
                      </div>
                      <div class="col-sm-12 propic create-proj-thumb">
                        <div class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">
                          <a href="">Choose an image from your computer</a><br>
                          <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                          <label>At least 1024x768 pixels</label>

                        </div>
                      </div>
                    </div>
                  </div>
               <input type="hidden"  ng-model="club.image" name="image" ng-value="club.image='dummy'" >
             
               <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
               <span ng-bind="alert.msg"></span> </alert>
                <span>
                  We will review your information for registration. Once registered, you Club will be listed in our network information so that others can find you. For the registered clubs, ECO will provide systems for Club management and Networking and collaborating with other clubs.
                </span>

              <footer class="panel-footer text-right bg-light lter">
              <div class="col-md-12 center" ng-show="loadingg == true" ng-cloak>
                  <span ng-bind="loadingMsg"></span>
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                <button type="submit" class="btn btn-info">Submit</button>
              </footer>
              </div>
            </div>
          </div>
          </form>
      </div>
