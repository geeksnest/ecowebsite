<?php echo $this->getContent() ?>
<div class="container-fluid">
  <div class="eco-container signup-container">
    <div class="wrapper-form-login" ng-controller="ChangepassCtrl">

      <!-- <div class="list-group list-group-sm" ng-show="authError" style="text-align: center;">
        {[{ alerts }]}
      </div> -->
      <div class="wrapper text-center tokenexpired" ng-show="authError">
        <h3>The Password Reset Token has expired.</h3>
        <br>
      </div>
      <div class="wrapper text-center" ng-show="authsuccess">
        <h3>Password Change Success</h3>
        <br>
        <p>
        Back to <strong><a href="/login">Log in</a></strong> Site
        </p>
      </div>

       <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)" class="text-center">
       {[{ alert.msg }]}</alert>
      <div class="m-b-lg" ng-show="validform">
        <div class="wrapper text-center">
          <strong>Reset Your Password
          </strong><br>
          <span style="font-size:12px">  <?php echo $email; ?> </span>
        </div>
        <form name="form" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="res(forgot)">
          <div class="list-group list-group-sm">
               <input type="hidden" ng-model="forgot.email"  ng-init="forgot.email='<?php echo $email ?>'">
               <input type="hidden" ng-model="forgot.token" ng-init="forgot.token='<?php echo $token ?>'">
               <input type="hidden" value="<?php echo $token ?>" id="token">

               <div class="list-group-item">
               <label class="col-sm-12 control-label"> <span class="label bg-danger" ng-if="passwordMin == true">Passwords is too small, minimum of 6 digits.</span></label>
                <input type="password" id="password" class="form-control {[{passwordMin == true ? 'red-border': ''}]}" ng-model="forgot.password" name="forgot.password" required="required" placeholder="Password" ng-change="minPass(forgot)">
               </div>
              <div class="list-group-item">
              <label class="col-sm-12 control-label"><span class="label bg-danger" ng-if="passwordInvalid == true">Passwords do not match.</span></label>
                <input type="password" id="repassword" class="form-control" ng-model="forgot.repassword" name="forgot.repassword" required="required" placeholder="Re-Type Password" ng-change="chkPass(forgot)">
              </div>

          
          </div>
          <button type="submit" ng-disabled='sending == true' class="btn btn-primary btn-block btn-success" style="text-align:center">Reset Password</button>
          <div class="line line-dashed"></div>
          <p class="text-center"><small>Dont share your password to anyone.</small></p>

          <div class="list-group list-group-sm" ng-if="sending == true" ng-cloak style="text-align: center;">
           <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
          </div>
          <div class="col-md-12 center">
            <span class="list-group list-group-sm" >Processing your request...</span>
          </div>
        </div>

        </form>
      </div>
    
    </div>

     <div style="padding-bottom:50px"> </div>
  </div>
</div>

<!-- log in token ang ginagawa ko pero bakit di naman na encrypt -->