<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <a href="#">ECO</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php if($pageslugs == 'eco-around-the-world'){ ?>
      <a href="/#/whatiseco">WHAT IS ECO</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php } else{?>
      <a href="/#/#serve">SERVE</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }?>
      <?php if($pageslugs == 'heroes-on-duty' || $pageslugs == 'call-hero' ){ ?>
      <a href="/call-heroes">CALL HEROES</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }?>
      <a href=""><?php echo $title ?></a> 
    </div>
  </div>
</div>
<div id="atw"> </div>

<?php echo $body; ?>

<?php if($pageslugs == 'eco-around-the-world' ){ ?>

<div class="row" ng-controller="AtwCtrl">
        <div class="col-sm-12">
          <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitatw(user)">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>Name of Organization</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.org" name="organization" required="true"  >
          </div>
          <div class="form-group">
            <label>Organization’s Tax ID</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.orgid" name="taxid" required="true" >
          </div>
          <div class="form-group">
            <label>Contact Phone Number</label>
            <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" >
          </div>
          <div class="form-group">
            <label>Official Address</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.address" name="address" required="true" >
          </div>
          <div class="checkbox">
            <label class="i-checks">
              <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)"><i></i> 
              <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
              <label ng-show="chkerror"  class="control-label" style="color:#a94442; margin-left:-40px;">This field is required.</label>
            </label>
          </div>
        </div>
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <footer class="panel-footer text-right bg-light lter">
          <button type="submit" class="btn btn-success"  >Submit</button>
        </footer>
      </div>
    </form>
</div>
</div>

<?php } ?>

<?php if($pageslugs == 'heroes-on-duty' ){ ?>
<div class="row" ng-controller="HeroesOnDutyCtrl">
 <p class="text-muted">If you are a graduate of the HEROES program and desire to develop leadership and reinforce what you learned through the training, please click “HEROES on Duty” to apply for opportunities to help other and serve your community.</p>
 <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="saved(user)">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="form-group">
        <label  >Name:</label>
        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.name" name="username" required="true">
      </div>
      <div class="form-group">
        <label >Email</label>
        <input type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="user.email"  name="email" required="true">
      </div>
      <div class="form-group">
        <label >Phone</label>
        <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone-input  placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true">
      </div>
      <div class="form-group" required="true">
        <label class="control-label">Gender</label>
        <div class="radio" style="margin-left:15px;">
          <label class="i-checks">
            <input class="radioinput" type="radio" value="male" ng-model="user.gender" name="3" ng-click="gndr(user.gender)" >
            <i></i>
            Male
          </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <label class="i-checks">
            <input id="female" class="radioinput" type="radio" value="female" ng-model="user.gender" name="4" ng-click="gndr(user.gender)">
            <i></i>
            Female
          </label>
        </div>
        <label ng-show="gender" class="control-label" style="color:#a94442;">Gender fields is required.</label> 
      </div>
      <div class="form-group">
       <div style="padding-bottom:10px;">Date of birth:</div>
       <div class="col-sm-6">
        <div class="row">
          <div class="col-md-3">
            <label class="control-label" >Year</label>
            <select ng-model="user.byear" class="form-control ng-pristine ng-invalid ng-invalid-required " class="required" name="selyear" id="selyear" required="true">
              <?php for ($year= 1960 ; $year < date('Y'); $year++) { echo "<option value='".$year."'>".$year."</option>";}?>
            </select>
          </div>
          <div class="col-md-9">
            <div class="col-xs-8" style="padding-left:0px;">
              <label class="control-label" >Month</label>
              <select  ng-model="user.bmonth" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="month" required="true">
                <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                <?php foreach ($formonths as $index => $formonth) { echo "<option value='".$index."'>".$formonth."</option>";}?>
              </select>
            </div>
            <div class="col-xs-4" style="padding-right:0px;">
              <label class="control-label">Day</label>
              <select ng-model="user.bday" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="day" required="true">
                <?php for ($day= 1 ; $day < 31; $day++) { 
                  (strlen($day)==1)? $_day = trim("0".$day) : $_day = $day;
                  echo "<option value='".$_day."'>".$_day."</option>";
                }?>
              </select>
            </div>
          </div>
        </div>
        <label ng-show="nbday"  class="control-label" style="color:#a94442;">This Birthdate fields is required.</label>
      </div> 
    </div>
    <div class="line line-dashed b-b line-lg pull-in"></div>
    <div class="form-group">
      <label >When did you take the HEROES leadership training?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.leadtrain" name="require" required="true">
    </div>
    <div class="form-group">
      <label>Your skills or expertise</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.skill" name="skill" required="true">
    </div>
    <div class="form-group">
      <label  >How many hours can you service for a week?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.service"  name="service" required="true">
    </div>
    <div class="form-group">
      <label >For how many weeks can you be committed?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.commit"  name="commit" required="true">
    </div>
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>
</form>
</div>
<?php }?>





<?php if($pageslugs == 'call-hero' ){ ?>
<div class="row" ng-controller="HeroesCallCtrl">
  <div class="col-sm-12">
    <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="callHereSave(user)">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>Name of Organization</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.organization" name="organization" required="true"  >
          </div>
          <div class="form-group">
            <label>Organization’s Tax ID</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.taxid" name="taxid" required="true" >
          </div>
          <div class="form-group">
            <label>Contact Phone Number</label>
            <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone-input  placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" >
          </div>
          <div class="form-group">
            <label>Official Address</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.address" name="address" required="true" >
          </div>
          <div class="form-group">
            <label>Help Location (if different from above)</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.helplocaiton" name="helplocaiton" required="true">
          </div>
          <div class="form-group">
            <label>Reasons for needing help</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.reasonhelp" name="reasonhelp" required="true" >
          </div>
          <div class="form-group">
            <label>When you need help</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.whyhelp" name="whyhelp" required="true" >
          </div>
          <div class="form-group">
            <label>Desired qualification for helpers</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.qualification" name="qualification" required="true" >
          </div>
          <div class="checkbox">
            <label class="i-checks">
              <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)"><i></i> 
              <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
              <label ng-show="chkerror"  class="control-label" style="color:#a94442; margin-left:-40px;">This fields is required.</label>
            </label>
          </div>
        </div>
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <footer class="panel-footer text-right bg-light lter">
          <button type="submit" class="btn btn-success"  >Submit</button>
        </footer>
      </div>
    </form>
  </div>
</div>
<?php }?>


<!-- ===== START of WHAT IS ECO ===== -->

<?php if($pageslugs == 'more-about-eco' || $pageslugs == 'partners-and-sponsors' || $pageslugs == 'how-to-start-change' || $pageslugs == 'join'){ ?>

<div class="container-fluid">
  <div class="eco-container">
    <div class="row padding-bot30 padding-top30" id="circle-links">
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/more-about-eco">
          <img src="/images/template_images/more-about-eco-circle.jpg" class="sublink-img">
          <p class="title1">More About ECO</p>
        </a>
      </div>
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/partners-and-sponsors">
          <img src="/images/template_images/partners-and-supporters-circle.jpg" class="sublink-img">
          <p class="title1">Partners & Supporters</p>
        </a>
      </div>
      <div class="col-xs-4 cen">
        <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/how-to-start-change">
          <img src="/images/template_images/how-to-start-to-change-circle.jpg" class="sublink-img">
          <p class="title1">How to Start to Change</p>
        </a>
      </div>
    </div>
  </div>
</div>

<?php }?>


<!-- ===== END of WHAT IS ECO ===== -->

<!-- ===== START of JOIN ===== -->



<!-- ===== END of JOIN ===== -->

<!-- ===== START of LEARN ===== -->

<?php if($pageslugs == 'curriculum' || $pageslugs == 'programs' || $pageslugs == 'support-for-graduates'){ ?>


<div style="height: 30px;"></div>

<div class="eco-container"> 
  <div class="row padding-bot30 padding-top30" id="circle-links">
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/curriculum">
        <img src="/images/template_images/curriculum-circle.jpg" class="sublink-img">
        <p class="title1">Curriculum</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/programs">
        <img src="/images/template_images/programs-circle.jpg" class="sublink-img">
        <p class="title1">Programs</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/support-for-graduates">
        <img src="/images/template_images/support-for-graduates-circle.jpg" class="sublink-img">
        <p class="title1">Support for Graduates</p>
      </a>
    </div>
  </div>
</div>

<?php }?>

<!-- ===== END of LEARN ===== -->


<!-- ===== START of SERVE ===== -->
<?php if($pageslugs == 'heroes-news'){ ?>


<?php }?>

<!-- ===== END of SERVE ===== -->


<!-- ===== START of STORE ===== -->

<!-- ===== END of STORE ===== -->

<!-- ===== START of DONATE ===== -->

<?php if($pageslugs == 'project-funds' || $pageslugs == 'ways-of-giving'){ ?>
<div style="height: 30px;"></div>
<div class="eco-container"> 
  <div class="row padding-bot30 padding-top30" id="circle-links">
    <div class="col-xs-2 cen">
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/project-funds">
        <img src="/images/template_images/eco-project-funds-thumb.jpg" class="sublink-img">
        <p class="title1">Project Funds</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/ways-of-giving">
        <img src="/images/template_images/eco-ways-of-giving-thumb.jpg" class="sublink-img">
        <p class="title1">Ways of Givving</p>
      </a>
    </div>
    <div class="col-xs-2 cen">
    </div>
  </div>
</div>
<?php }?>



<!-- ===== END of DONATE ===== -->