<div class="row" ng-controller="HeroesOnDutyCtrl">
 <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="saved(user)">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="form-group">
        <label  >Name:</label>
        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.name" name="username" required="true">
      </div>
      <div class="form-group">
        <label >Email</label>
        <input type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="user.email"  name="email" required="true">
      </div>
      <div class="form-group">
        <label >Phone</label>
        <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone-input  placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true">
      </div>
      <div class="form-group" required="true">
        <label class="control-label">Gender</label>
        <div class="radio" style="margin-left:15px;">
          <label class="i-checks">
            <input class="radioinput" type="radio" value="male" ng-model="user.gender" name="3" ng-click="gndr(user.gender)" >
            <i></i>
            Male
          </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <label class="i-checks">
            <input id="female" class="radioinput" type="radio" value="female" ng-model="user.gender" name="4" ng-click="gndr(user.gender)">
            <i></i>
            Female
          </label>
        </div>
        <label ng-show="gender" class="control-label" style="color:#a94442;">Gender fields is required.</label> 
      </div>
      <div class="form-group">
       <div style="padding-bottom:10px;">Date of birth:</div>
       <div class="col-sm-6">
        <div class="row">
          <div class="col-md-3">
            <label class="control-label" >Year</label>
            <select ng-model="user.byear" class="form-control ng-pristine ng-invalid ng-invalid-required " class="required" name="selyear" id="selyear" required="true">
              <?php for ($year= 1960 ; $year < date('Y'); $year++) { echo "<option value='".$year."'>".$year."</option>";}?>
            </select>
          </div>
          <div class="col-md-9">
            <div class="col-xs-8" style="padding-left:0px;">
              <label class="control-label" >Month</label>
              <select  ng-model="user.bmonth" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="month" required="true">
                <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                <?php foreach ($formonths as $index => $formonth) { echo "<option value='".$index."'>".$formonth."</option>";}?>
              </select>
            </div>
            <div class="col-xs-4" style="padding-right:0px;">
              <label class="control-label">Day</label>
              <select ng-model="user.bday" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="day" required="true">
                <?php for ($day= 1 ; $day < 31; $day++) { 
                  (strlen($day)==1)? $_day = trim("0".$day) : $_day = $day;
                  echo "<option value='".$_day."'>".$_day."</option>";
                }?>
              </select>
            </div>
          </div>
        </div>
        <label ng-show="nbday"  class="control-label" style="color:#a94442;">This Birthdate fields is required.</label>
      </div> 
    </div>
    <div class="line line-dashed b-b line-lg pull-in"></div>
    <div class="form-group">
      <label >When did you take the HEROES leadership training?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.leadtrain" name="require" required="true">
    </div>
    <div class="form-group">
      <label>Your skills or expertise</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.skill" name="skill" required="true">
    </div>
    <div class="form-group">
      <label  >How many hours can you service for a week?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.service"  name="service" required="true">
    </div>
    <div class="form-group">
      <label >For how many weeks can you be committed?</label>
      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.commit"  name="commit" required="true">
    </div>
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>
</form>
</div>