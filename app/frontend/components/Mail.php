<?php
use Phalcon\Mvc\User\Component,
require_once __DIR__ . '/../../vendor/Swift/swift_required.php';
/**
*
* Sends e-mails based on pre-defined templates
*/
class Mail extends Component
{
	protected $_transport;

/**
* Sends e-mails via gmail based on predefined templates
*
* @param array $to
* @param string $subject
* @param string $name
* @param array $params
*/
public function send($to, $subject, $name, $params)
{
//Settings
	$mailSettings = $this->config->mail;
	$template = $name . $params;
// Create the message
	$message = Swift_Message::newInstance()
	->setSubject($subject)
	->setTo($to)
	->setFrom(array(
		$mailSettings->fromEmail => 'noreply@earthcitizenorganization.com'
		))
	->setBody($template, 'text/html');
	if (!$this->_transport) {
		$this->_transport = Swift_SmtpTransport::newInstance(
			'smtp.gmail.com',
			465,
			'ssl'
			)
		->setUsername('efrenbautistajr@gmail.com')
		->setPassword('blk21lot82010');
	}
// Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($this->_transport);
	return $mailer->send($message);
}
}