<div class="container margin-top40 ">


  <div class="col-sm-9" ng-controller="author">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">

          <div class="col-sm-12 margin-bot10 center">
            <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ photo }]}" class="author-img2">
          <!-- </div>
          <div class="col-sm-3 ">
          </div>
          <div class="col-sm-8 padding-author-head"> -->
            <p class="authorname"><span ng-bind="name"></span></p>
            <p class=""><strong class="orange">Location:</strong><span ng-bind="location"></span><br> 
            <strong class="orange">Occupation:</strong><span ng-bind="occupation"></span></p>
          </div>
          <div class="col-sm-12 margin-top20 a-little-about-me">
            <h4>A Little About Me</h4>
          </div>
          <div class="col-sm-12 margin-top20"><br><br></div>           
          <div class="col-sm-12 margin-top20" ng-bind-html="about">
          </div>
        </div>
        <div class="row margin-top20">
          <h4 class="blog-title-list orange padding20">News and Articles</h4>
          <div class="col-md-12 greyborder">
            <div class="row list-title-blog" ng-repeat="mem in data.data" >


              <div class="col-md-3 news-thumb-container">
               
                <a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}" class="news-thumb-link">

                  <div ng-show="mem.videothumb" class="featured-blog-img" style="background-image: url({[{ mem.videothumb | returnYoutubeThumb }]});">
                    <div class="youtube-play" style="background-image: url(/images/template_images/yt.png);"></div>
                  </div>

                  <div ng-show="mem.imagethumb">
                    <div class="featured-blog-img" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/newsimages/{[{mem.imagethumb}]});"></div>
                  </div>

                </a>

              </div>

              <div class="col-md-7">
                <div class="row">
                  <div class="col-sm-12"> 
                    <div class="row">
                      <div class="col-sm-12">
                        <a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}"><span class="size23 font1 news-title"><span ng-bind="mem.title"></span></span></a>

                      </div>

                      <div class="col-sm-12">
                        <strong>                      
                          <span class="orange thin-font1"ng-repeat="category in mem.categories">
                            <a href="<?php echo $this->config->application->baseURL; ?>/news/category/{[{ category.categoryslugs}]}">
                              <span ng-bind="category.categoryname"></span><span ng-bind="$last ? '' : ', '"></span>                              
                            </a>
                          </span>                      
                        </strong><br>
                        <span ng-show="news.name !=''" class="thin-font1">by 
                          <strong><span class="orange"><a href="<?php echo $this->config->application->baseURL; ?>/news/author/{[{ mem.authorid }]}"><span ng-bind="mem.name"></span></a></span></strong>
                          <span class="boxdate2"> <span ng-bind="mem.date"></span></span>
                        </span> 
                        <br/><br/>
                      </div>
                      <div class="col-sm-12">
                        <div class="font1 size12 word-wrap">
                          <span ng-bind="mem.summary"></span>
                          <br/><br/>
                        </div>
                      </div>

                <!-- <div class="col-sm-12 font1 size15">
                <br/>
                  <?php   
                  $removedimg = strip_tags($getginfo[$key]->body, '');
                  $limitcontent = substr($removedimg,0,275);
                  echo $limitcontent;
                  ?>
                </div> -->
              </div>
            </div>
          </div>

          <div style="clear:both"></div>
          <br>
        </div>

        <div class="col-xs-2 boxdate">
          <div class="datebox">
            <div >
              <div style="font-size:35px;"><span ng-bind="mem.date | day"></span></div>
              <span ng-bind="mem.date | getmonth "></span>   
              <span ng-bind="mem.date | year"></span>               
            </div>
          </div>
        </div>
      </div>

      <button ng-hide="hideloadmore" class="news-list-show-more" ng-click="showmorenews()" ng-disabled="loading"><span ng-show="loading"><img src="/images/template_images/newsloading.gif"></span><span ng-hide="loading">Show More</span></button>
      <button ng-show="nomorenews" class="news-list-show-more" ng-disabled="nomorenews"><span>end of the list</span></button>
      <hr>
    </div>
  </div>
</div>
</div>
</div>

<div class="col-xs-3">


</div>

</div>
