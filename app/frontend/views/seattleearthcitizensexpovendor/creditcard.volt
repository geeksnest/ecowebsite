
<!-- CREDIT CARD BILLING-->
<div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
    <br/>
    <div class="form-group row" >
        <div class="col-md-4">
        </div>
        <div class="col-md-8">
            <img src="/images/template_images/visa.png" style="width:65px;">
            <img src="/images/template_images/amex.png" style="width:65px;">
            <img src="/images/template_images/mastercard.png" style="width:65px;">
            <img src="/images/template_images/discover.png" style="width:65px;">
        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-4">
            Credit Card Number: *
        </div>
        <div class="col-md-8">
            <input class="form-control" type="text" ng-model="cc.ccn" name="" required="required" only-digits />
        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-4">
            CVV Number: *
        </div>
        <div class="col-md-8">
            <input type="text" class="form-control " ng-model="cc.cvvn" name="" required="required" only-digits />
        </div>
        <div class="col-md-4">                            
        </div>
        <div class="col-md-8">

        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-4">
            Credit Card Expiration: *
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-xs-6">
                    <span>Month</span><br/>
                    <select ng-model="cc.expiremonth" class="form-control" required="required">
                        <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                        <?php foreach ($formonths as $index => $formonth) { 
                            echo "<option value='".$index."'>".$formonth."</option>";
                        }?>
                    </select>
                </div>
                <div class="col-xs-6">
                    <span>Year</span><br/>
                    <select ng-model="cc.expireyear" class="form-control" required="required">
                        <?php for ($year=date('Y'); $year < 2050; $year++) { 
                            echo "<option value='".$year."'>".$year."</option>";
                        }?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-12">
            <span style="font-weight:bold;">Billing Information</span>
        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-4">
            First Name: *
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.billingfname" name="billingfname" required="required"/>
        </div>
    </div>
    <div class="form-group row" >
        <div class="col-md-4">
            Last Name: *
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.billinglname" name="billinglname" required="required"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            Address Line 1: *
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.al1" name="al1" required="required"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            Address Line 2:
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.al2" name="al2"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            City: *
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.city" name="city" required="required"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            State:
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.state" name="city"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            ZIP/Postal Code: *
        </div>
        <div class="col-md-8">
            <input class="form-control " type="text" ng-model="cc.zip" name="zip" required="required"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            Country: *
        </div>
        <div class="col-md-8">
            <select ng-model="cc.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required="required">
            </select>
        </div>
    </div>                            
    <div class="form-group row" >
        <div class="col-md-4">
            Email Address: *
        </div>
        <div class="col-md-8">
            <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
            <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
        </div>
    </div>
    <div class="form-group">
        How did you learn about the Natural Healing Expo 2015 in Seattle?
        <select class="form-control m-b" ng-model="cc.howdidyoulearn" ng-change="changeme()">
            <option value="ECO event">ECO event</option>
            <option value="ECO Program Graduates">ECO Program Graduates</option>
            <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
            <option value="Invitation Email">Invitation Email</option>
        </select>
    </div>
    <div class="form-group" ng-if="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
        Center Name
        <select class="location form-control m-b" ng-model="cc.cname" required="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
            <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
        </select>
    </div> 