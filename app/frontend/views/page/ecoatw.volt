<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="#">WHAT IS ECO?</a> <a><i class="fa fa-arrow-right"></i></a><a href=""><?php echo $title ?></a> 
    </div>
  </div>
</div>
<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <p>Caring for people and the planet is not a concern of just a few people or a few organizations. ECO is working with its associated groups and organizations, and will continuously expand its networking with other organizations. Our goal is establishing ECO National Chapters in 100 Countries by 2017.</p>
      <p>For more information about the associated organizations, please visit the site below.</p>
      <p>
        <b>Korea: Earth Citizen Movement Alliance</b><br>
        <a href="http://www.earthact.org">http://www.earthact.org</a>
      </p>
      <p>
        <b>Japan: Earth Citizen School</b><br>
        <a href="http://earthcitizen.jp">http://earthcitizen.jp</a>
      </p>
      <p></p>
      <p><b>Create ECO Action Group</b></p>
      <p>You can also create an ECO action group and create an Earth Citizen Movement in your community. Once created, you group will be listed in our network information so that others can find you.</p>

      <div class="row" ng-Controller="AtwCtrl">
        <div class="col-sm-12">
          <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="callHereSave(user)">

            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <label>Name of Organization</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.org" name="organization" required="true"  >
                </div>
                <div class="form-group">
                  <label>Organization’s Tax ID</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.orgid" name="taxid" required="true" >
                </div>
                <div class="form-group">
                  <label>Contact Phone Number</label>
                  <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone-input  placeholder="(XXX) XXXX XXX" ng-model="user.contactno" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" >
                </div>
                <div class="form-group">
                  <label>Official Address</label>
                  <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.address" name="address" required="true" >
                </div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)"><i></i> 
                    <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
                    <label ng-show="chkerror"  class="control-label" style="color:#a94442; margin-left:-40px;">This field is required.</label>
                  </label>
                </div>

              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

