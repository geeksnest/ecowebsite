<style type="text/css">
  .help-block,.p{
    font-size: 14px;
  }
  .n{
    font-size: 12px;
  }
</style>

<div class="row" ng-controller="FaCtrl">
  <div class="col-sm-12">
    <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitfa(user,files)">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group has-feedback">
            <label>Name of Organization * </label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.organization" name="organization" required="true" ng-disabled="loadingg == true">
            <i ng-show="form.organization.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.organization.$invalid && !form.organization.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Address 1 * </label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.add1" name="add1" required="true" ng-disabled="loadingg == true">
            <i ng-show="form.add1.$valid" class="glyphicon glyphicon-ok form-control-feedback" ng-cloak></i>
            <i ng-show="form.add1.$invalid && !form.add1.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Address 2 </label> <label class="n">(optional)</label> <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.add2" name="add2" ng-disabled="loadingg == true">
            <i ng-show="form.add2.$valid && user.add2" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
           <!--  <i ng-show="form.add2.$invalid && !form.add2.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i> -->
          </div>
          <div class="form-group has-feedback">
            <label>Country *</label> <input type="text" id="country" style="opacity:0;">
            <select ng-model="user.country" class="location form-control m-b" ng-disabled="loadingg == true">
            <!-- <option ng-init="user.country = 'United States'" ng-repeat="cn in countries" value="{[{cn.name}]}"> {[{cn.name}]} </option> -->
            <option ng-init="user.country = 'United States'" ng-repeat="cn in countries" value="{[{cn.name}]}" > {[{cn.name}]}</option>
            </select>
            <i ng-show="form.country.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.country.$invalid && !form.country.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
            <label ng-show="nd && !user.country" class="control-label p" style="color:#a94442;" ng-cloak>
                 This field is required.</label>
          </div>
          <div class="form-group has-feedback">
            <label>City *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.city" name="city" required="true" ng-disabled="loadingg == true">
            <i ng-show="form.city.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.city.$invalid && !form.city.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>State *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.state" name="state" required="true" ng-disabled="loadingg == true">
            <i ng-show="form.state.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.state.$invalid && !form.state.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Zip/Postal Code *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.zip" name="zip" required="true" maxlength="20" ng-disabled="loadingg == true">
            <i ng-show="form.zip.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.zip.$invalid && !form.zip.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          
          <div class="form-group">
            <div class="">Organization Category *</div> <br>
            <div class="btn-group">
             <label class="i-checks">
            <input name="cat" type="radio" ng-model="user.cat" value="Profit" class="ng-dirty ng-invalid ng-invalid-required" ng-disabled="loadingg == true"><i></i>
            Profit
            </label>  
            &nbsp; &nbsp;&nbsp;
            <label class="i-checks">
            <input name="cat" type="radio" ng-model="user.cat" value="Nonprofit" class="ng-dirty ng-invalid ng-invalid-required" ng-disabled="loadingg == true"><i></i>
             NonProfit
            </label> 
            <br>
             <label ng-show="n && !user.cat" class="control-label p" style="color:#a94442;" ng-cloak>
            Category field is required.</label>

            </div>
          </div>
          <div class="form-group">
            <div class=""> Year of Establishment * </div> <br>
            <select ng-model="user.year" name="year" class="input-sm form-control w-sm inline v-middle" style="width:80px" required="true" ng-disabled="loadingg == true">
            <?php for ($year= 1960 ; $year <= date('Y'); $year++) { echo "<option value='".$year."'>".$year."</option>";}?>
            </select> <br> 
            <label ng-show="nbday && !user.year" class="control-label p" style="color:#a94442;" ng-cloak>
            Year field is required.</label>
          </div>
          <div class="form-group has-feedback">
            <label>Organization Website Address * </label>
            <input type="text" class="form-control" ng-model="user.web" name="website" required="true" placeholder="http://" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" ng-disabled="loadingg == true">
            <i ng-show="form.website.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.website.$invalid && !form.website.$pristine && form.website.$error" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
            <label ng-show="form.website.$invalid && !form.website.$pristine && form.website.$error" class="control-label p" style="color:#a94442;" ng-cloak>
              Invalid URL Pattern.</label>
          </div>
          <div class="form-group has-feedback">
            <label>Organization Mission Statement * </label> <label class="n">(Less than 500 words)</label>
            <textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.mission" name="mission" required="true" placeholder="" maxlength="500" ng-disabled="loadingg == true">
            </textarea>
            <i ng-show="form.mission.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.mission.$invalid && !form.mission.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Contact Person * </label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.contactperson" name="contactperson" required="true" ng-disabled="loadingg == true">
            <i ng-show="form.contactperson.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.contactperson.$invalid && !form.contactperson.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>

          <div class="form-group has-feedback">
            <label>Contact Phone Number * </label>
            <input type="tel" name="phone" class="form-control ng-pristine ng-invalid ng-invalid-required ng-invalid-pattern input-phone {[{user.phone==undefined && !form.phone.$pristine || user.phone=='' ? 'red-border' : ''}]} " phone  placeholder="(XXX) XXX XXXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" ng-keypress="ph(user.phone)" id="ph" ng-disabled="loadingg == true">
            <i ng-show="form.phone.$valid && user.phone!=undefined && user.phone!=''" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.phone.$invalid && !form.phone.$pristine || user.phone==''" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
            <label ng-show="form.phone.$error.pattern || user.phone=='' " class="control-label p" style="color:#a94442;" ng-cloak>
            Invalid pattern.</label>
          </div>

          <div class="form-group has-feedback">
            <label>Contact Email Address * </label>
             <input type="email" class="form-control" ng-model="user.email"  name="email" required="true" 
             ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-disabled="loadingg == true">
             <i ng-show="form.email.$valid && user.email" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
             <i ng-show="form.email.$invalid && !form.email.$pristine && form.email.$error" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
        
          </div>

          <div class="form-group has-feedback">
            <label>Reason for Partnership * </label> <label class="n">(Less than 500 words)</label>
            <textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.reason" name="reason" required="true" placeholder="" maxlength="500" ng-disabled="loadingg == true">
            </textarea>
            <i ng-show="form.reason.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.reason.$invalid && !form.reason.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
           <div class="form-group">
                    <label>Organization Profile Photo</label>
                    <div style="clear:both;"></div>
                    <div class="row col-sm-6"> 
                      <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                        <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlerts($index)"><span ng-bind="imgAlerts.msg"></span></alert>
                      </div>
                      <div class="col-sm-12 create-proj-thumb">
                        <div class="line line-dashed b-b line-lg"></div>                    
                        <img src="{[{base_url}]}/images/default_images/default_image.jpg" ng-if="imageselected == false">
                        <img ngf-src="projImg[0]" ng-if="imageselected == true">
                      </div>
                      <div class="col-sm-12 propic create-proj-thumb">
                        <fieldset class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required" ng-disabled="loadingg == true">
                          <a href="">Choose an image from your computer</a><br>
                          <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                          <label>At least 1024x768 pixels</label>

                        </fieldset>
                      </div>
                    </div>
                  </div>
               <input type="hidden" ng-model="user.image" name="image" ng-value="user.image='dummy'" >

          <div class="form-group">
            <div class="col-xs-9 col-xs-offset-3">
              <div id="messages"></div>
            </div>
          </div>
          <div class="">
            <label class="i-checks">
              <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)" ng-init="user.agree=false" ng-disabled="loadingg == true"><i></i> 
              <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
              <label ng-show="chkerror && user.agree==false" class="control-label p" style="color:#a94442; margin-left:-40px;" ng-cloak>This field is required.</label>
            </label>
          </div>
           
        <!--   <div class="col-sm-6">
            <div class="col-sm-2">
             <button type="submit" class="btn btn-success">Submit</button>
           </div>
           <div class="col-sm-2">  
           <div class="loadercontainer" ng-cloak ng-show="loader">
              <div class="spinner" style="margin:0px;width:auto;height:30px">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
              </div>
            </div>
          </div>
        </div> -->
          <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
               <span ng-bind="alert.msg"></span> </alert>
               
              <footer class="panel-footer text-right bg-light lter">
              <div class="col-md-12 center" ng-show="loadingg == true" ng-cloak>
                  <span ng-bind="loadingMsg"></span>
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                <button type="submit" ng-disabled="loadingg == true" class="btn btn-info">Submit</button>
              </footer>
              </div>

        </div>
      </div>
    </form>
  </div>
</div>