
<?php echo $this->getContent(); ?>

	<div class="container-fluid" ng-controller='TestimonialCtrl'>
        <div class="eco-container">
            <?php 
                $getginfo = $msginfo;
                if($getginfo){
            ?>
                <div class="eco-container">
                    <p style="font-family:verdana,geneva,sans-serif"><span style="font-size:36px">Testimonials</span></p>
                </div>

                <?php foreach ($getginfo as $key => $value) { ?>
                <div class="eco-container" id="testimonials">
                    <div class="media" style="background-color:#eee">
                        <span class="pull-left thumb-sm"><img src="<?php echo $this->config->application->apiURL; ?>/images/user.png" alt="..."></span>
                        <div class="media-body">
                            <div class="pull-right text-center text-muted"></div>
                            <a href="" class="h4"><?php echo $getginfo[$key]->name ?></a>

                            <div class="block">
                                <em class="text-xs">Company/Organization: <span class="text-danger"><?php echo $getginfo[$key]->comporg ?></span></em>
                                <i class="fa fa-envelope fa-fw m-r-xs"></i>:<?php echo $getginfo[$key]->comporg ?>
                            </div>
                            <blockquote>
                                <small class="block m-t-sm"><?php echo $getginfo[$key]->message ?></small>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="eco-container">
                    <div style="padding-top:20px" ng-show="notendresult">
                        <div class="spinner" ng-show="showloading">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                        <a ng-click="showmore()" class="btn bg-info">show more</a>
                    </div>
                    <div style="padding-top:20px" ng-show="endresult">
                        <a class="btn bg-info" disabled>no more to show</a>
                    </div>
                </div>
                <?php } ?>
                <div style="padding-top:20px">
                    <form name="formtesti" method="post" class="form-validation ng-pristine ng-invalid ng-invalid-required">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="h4">Sent us your Testimony</span>
                                <span class="formerror" ng-show="Emali != ''" style="color: #f80000;">Invalid E-mail Address.</span>
                                <span class="formerror" ng-show="testi != ''" style="color: #0C0;">Testimony Sent.</span>
                            </div>
                            <div class="panel-body">
                                <p class="text-muted">Need support? please fill the fields below.</p>                        
                                <div class="form-group pull-in clearfix">
                                    <div class="col-sm-6">
                                        <label>Your name</label>
                                        <input type="text" name="name" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Name" ng-model="user.name" required="">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" placeholder="Enter email" ng-model="user.email" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Company/Organization</label>
                                    <input type="text" name="comporg" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="" ng-model="user.comporg" required="">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control"  ng-model="user.message" name="message" rows="6" placeholder="Type your message"></textarea>
                                </div>
                            </div>
                            <footer class="panel-footer text-right bg-light lter">
                                <input type="button" name="" id="abouteco" value="Submit"  class="btn btn-sm btn-primary" ng-click="submit(user)" ng-disabled="formtesti.$invalid" />
                            </footer>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<script type="text/javascript" src="/js/angular/angular.min.js"></script> 
<script type="text/javascript" src="/js/angular/angular-ui-router.min.js"></script> 
<script type="text/javascript" src="/js/angular/ui-validate.js"></script> 
<script type="text/javascript" src="/js/fr/app.js"></script> 




