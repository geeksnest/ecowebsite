<?php echo $this->getContent() ?>
<style type="text/css">
  iframe {
    width: 100%;
    height: 100%;
  }
</style>

<!-- ===== ECOCLUBS ===== -->
<div class="container-fluid" ng-controller="ClubDetailsctrl"> 
<!--   <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#join">JOIN</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/eco-clubs">ECO Clubs</a> <a><i class='fa fa-arrow-right'></i></a> <a href="">ECO Clubs list</a> 
      </div>
    </div>
  </div> -->


  <div class="eco-container" id="newslist">   
    <div class="row"> 
      <div class="col-sm-12">
        <p class="club-name word-wrap"><?php echo $name; ?></p>        
      </div>
      <div class="col-xs-12 greyborder1" id="latestlist">      
      <ul class="nav nav-tabs responsive">
      <li class="wd20">
      <a href="#comingsoon" data-toggle="tab" style="border-radius: 20px 20px 0px 0px;"> MEETINGS </a>
      </li>
      <li class="wd20">
      <a href="#comingsoon" data-toggle="tab" style="border-radius: 20px 20px 0px 0px;" ng-click="socialz=false">MEMBERS</a>
      </li>
      <li class="wd20">
      <a href="#comingsoon" data-toggle="tab" style="border-radius: 20px 20px 0px 0px;" ng-click="socialz=false">CALENDAR</a>
      </li>
       <li class="wd20">
      <a href="#comingsoon" data-toggle="tab" style="border-radius: 20px 20px 0px 0px;" ng-click="socialz=false">DISCUSSION</a>
      </li>
       <li class="active wd20">
      <a href="#info" data-toggle="tab" style="border-radius: 20px 20px 0px 0px;" ng-click="socialz=false">CLUB INFO</a>
      </li>
    </ul>

     <div class="tab-content responsive">
      <div class="tab-pane active" id="info">
                <div class="panel-body">

                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Name of Club: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> <?php echo $name; ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Description: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> <?php echo $description; ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Contact Name: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> <?php echo $contactperson; ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Email: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> <a class="clubemail" href="mailto:'<?php echo $emailadd; ?>'">
                     <?php echo $emailadd; ?> </a>  </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

               <!--    <?php if ($contactnumber) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Contact Number: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $contactnumber ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?>  -->


                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Area of Activities: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $city.", ".$state." ".$postal.", ".$country ?> </label>
                  </div>
                  </div>
                  <!-- <div class="line line-dashed b-b line-lg"></div>
                  <?php if ($address1) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Address: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $address1 ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?> 
                  <?php if ($address2) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Address2: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $address2 ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?> 

                  <?php if ($age != '0-0') { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Member Age Range: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label> 
                     <?php echo $age." years old" ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?>

                  <?php if ($interests) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Members' Common Interest: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $interests ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?>


                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Meeting Plan: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <?php echo $meeting; ?> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <?php if ($website) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Website: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                   <a href="<?php echo $website ?>">  <?php echo $website ?> </a> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?>

                  <?php if ($facebook) { ?>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <span class="text-muted"> Facebook: </span> 
                  </div>
                  <div class="col-sm-9">
                     <label class="word-wrap"> 
                     <a href="<?php echo $facebook ?>">
                     <?php echo $facebook ?> </a> </label>
                  </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                  <?php } ?> -->
                 

                </div>
          
      </div>
      <div class="tab-pane" id="comingsoon">
        <div class="pages-404">
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center m-b-lg">
            <span class="text-big club-text-big">coming soon</span>
            </div>
          </div>
          <div class="col-sm-12">
            This Tab is under development.
          </div>
        </div>
      </div>

      </div>
    </div>
       
         

        
      </div>
    </div>
  </div>
</div> 

