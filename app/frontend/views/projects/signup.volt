<?php echo $this->getContent() ?>
<div class="container-fluid">
  <div class="eco-container signup-container">
    <div class="wrapper-form" ng-controller="">
      
      <div class="m-b-lg">
        <div class="wrapper text-center">
          <strong>Sign up to to make amazing things.</strong>
        </div>
        <form name="formSignup" class="form-validation ng-pristine ng-invalid ng-invalid-required">
          <div class="text-danger wrapper text-center ng-binding ng-hide" ng-show="authError">

          </div>
          <div class="form-group">
            <div class="list-group-item">
              <input type="text" placeholder="Name" class="form-control no-border" ng-model="user.name" required="">
            </div>
            <div class="list-group-item">
              <input type="email" placeholder="Email" class="form-control no-border" ng-model="user.email" required="">
            </div>
            <div class="list-group-item">
              <input type="password" placeholder="Password" class="form-control no-border" ng-model="user.password" required="">
            </div>
          </div>
          <div class="checkbox m-b-md m-t-none">
            <label class="i-checks">
              <input type="checkbox" ng-model="agree" required="" class="ng-pristine ng-invalid ng-invalid-required"><i></i> Agree the <a href="">terms and policy</a>
            </label>
          </div>
          <button type="button" class="btn btn-success button-w btn-block" ng-click="signup()" ng-disabled="formSignup.$invalid">Sign up</button>
          <div class="line line-dashed"></div>
          <p class="text-center"><small>Already have an account?</small></p>
          <a class="btn button-w-fb btn-block" href=""> Log in with <i class="fa fa-facebook"></i>acebook</a>
        </form>
      </div>
      
    </div>
  </div>
</div>