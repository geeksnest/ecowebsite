$(function () {
  $('#demo1').scrollbox();
  $('#demo2').scrollbox({
    linear: true,
    step: 1,
    delay: 0,
    speed: 100
  });
  $('#demo3').scrollbox({
    switchItems: 5,
    distance: 144
  });
  $('#demo4').scrollbox({
    direction: 'h',
    switchItems: 5,
    distance: 670
  });
  $('#demo5').scrollbox({
    direction: 'h',
    distance: 134
  });
  $('#demo5-backward').click(function () {
    $('#demo5').trigger('backward');
  });
  $('#demo5-forward').click(function () {
    $('#demo5').trigger('forward');
  });
});