<?php

error_reporting(E_ALL);

ini_set("display_errors", 0);
try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);
			
		//Router for page
		$router->add('/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'index'
		));	

		//Router for projects
		$router->add('/projects', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'index'
		));	

		$router->add('/chapters/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'ecoaroundtheworld'
		));	

		//Router for earth-citizens-clubs
		$router->add('/earth-citizens-clubs', array(
			'module' => 'frontend',
			'controller' => 'clubs',
			'action' => 'index'
		));	

		//Router for view earth-citizens-clubs
		$router->add('/earth-citizens-clubs/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'clubs',
			'action' => 'view'
		));	

		//Router for projects agent view
		$router->add('/projects/{usernameAgent}/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'editproject'
		));	

		//Router for projects view
		$router->add('/projects/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'view'
		));	

		//Router for projects donation
		$router->add('/projects/donation/{slugs}/{amount}', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'projectdonation'
		));	

		//Router for projects sign up
		$router->add('/projects/signup', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'signup'
		));

		//Router for projects login
		$router->add('/login', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'login'
		));

		//forgot password
		$router->add('/forgotpassword', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'forgotpassword'
		));

		//forgot password changepassword
		$router->add('/forgotpassword/changepassword/{email}/{token}', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'changepassword'
		));

		//Router for projects create
		$router->add('/projects/createproject', array(
			'module' => 'frontend',
			'controller' => 'projects',
			'action' => 'createproject'
		));

		//Router for my medialibrary
		$router->add('/medialibrary', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'index'
		));

		//Router for medialibrary user view 
		$router->add('/medialibrary/{usernameAgent}/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'editvideo'
		));	

		//Router for medialibrary view
		$router->add('/medialibrary/preview/{usernameAgent}/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'previewvideo'
		));

		//Router for medialibrary view clients
		$router->add('/medialibrary/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'view'
		));

		//Router for medialibrary view clients tag
		$router->add('/medialibrary/tag/{tag}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'tag'
		));

		//Router for medialibrary view clients category
		$router->add('/medialibrary/category/{category}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'category'
		));

		//Router for medialibrary view clients archives
		$router->add('/medialibrary/archives/{archives}', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'archives'
		));


		//Router for my medialibrary
		$router->add('/medialibrary/mymedialibrary', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'mymedialibrary'
		));

		//Router for my medialibrary
		$router->add('/medialibrary/addvideo', array(
			'module' => 'frontend',
			'controller' => 'medialibrary',
			'action' => 'addvideo'
		));

		//Router for news
		$router->add('/news', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'index'
		));

		//Router for new member
		$router->add('/newmember/{userid}', array(
			'module' => 'frontend',
			'controller' => 'newmember',
			'action' => 'index'
		));

		//Router for news view
		$router->add('/news/{newsslugs}', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'view'
		));

		//Router for news category
		$router->add('/news/category/{catname}', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'category'
		));

		//Router for news tags
		$router->add('/news/tags/{tagname}', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'tags'
		));

		//Router for news author
		$router->add('/news/author/{author}', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'author'
		));

		//Router for news archive
		$router->add('/news/archive/{month}/{day}', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'archive'
		));

		//Router for RSS
		$router->add('/news/rss', array(
			'module' => 'frontend',
			'controller' => 'rss',
			'action' => 'index'
		));

		//Router for news preview
		$router->add('/news/preview', array(
			'module' => 'frontend',
			'controller' => 'news',
			'action' => 'preview'
		));


		// $router->add('/:controller/:action/:params', array(
		// 	'module' => 'frontend',
		// 	'controller' => 1,
		// 	'action' => 2,
		// 	'params' => 3,
		// ));
		
		// $router->add('/:controller', array(
		// 	'module' => 'frontend',
		// 	'controller' => 1
		// ));

		$router->add('/index/paypalipn', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'paypalipn'
		));
		
		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
		));
		
		$router->add('/maintenance', array(
			'module' => 'frontend',
			'controller' => 'maintenance',
			'action' => 'index'
		));

		$router->add('/complete/{amount}', array(
			'module' => 'frontend',
			'controller' => 'donation',
			'action' => 'complete'
		));

		$router->add('/registrationcomplete/{amount}', array(
			'module' => 'frontend',
			'controller' => 'donation',
			'action' => 'registrationcomplete'
		));

		$router->add('/confirmation/{id}/{code}', array(
			'module' => 'frontend',
			'controller' => 'donation',
			'action' => 'confirmation'
		));

		$router->add('/donation', array(
			'module' => 'frontend',
			'controller' => 'donation',
			'action' => 'index'
		));

		$router->add('/donation/complete', array(
			'module' => 'frontend',
			'controller' => 'donation',
			'action' => 'complete'
		));

		$router->add('/earth-citizenship', array(
			'module' => 'frontend',
			'controller' => 'join',
			'action' => 'index'
		));

		//Icare
		$router->add('/iCare2020', array(
			'module' => 'frontend',
			'controller' => 'icare',
			'action' => 'index'
		));
		//Icare
		$router->add('/icare2020', array(
			'module' => 'frontend',
			'controller' => 'icare',
			'action' => 'index'
		));

		//Icare
		$router->add('/ICARE2020', array(
			'module' => 'frontend',
			'controller' => 'icare',
			'action' => 'index'
		));

		//Icare complete pledge
		$router->add('/iCare2020/registered', array(
			'module' => 'frontend',
			'controller' => 'icare',
			'action' => 'registered'
		));

		$router->add('/nyearthcitizenswalk', array(
			'module' => 'frontend',
			'controller' => 'nyearthcitizenswalk',
			'action' => 'index'
		));

		$router->add('/houstonearthcitizenswalk', array(
			'module' => 'frontend',
			'controller' => 'houstonearthcitizenswalk',
			'action' => 'index'
		));

		$router->add('/seattlenaturalhealingexpo', array(
			'module' => 'frontend',
			'controller' => 'seattlenaturalhealingexpo',
			'action' => 'index'
		));

		$router->add('/seattlenaturalhealingexpovendor', array(
			'module' => 'frontend',
			'controller' => 'seattleearthcitizensexpovendor',
			'action' => 'index'
		));

		$router->add('/heroes-leadership-training-registration', array(
			'module' => 'frontend',
			'controller' => 'heroesleadershiptrainingregistration',
			'action' => 'index'
		));

		// $router->add('/eco-around-the-world', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'ecoatw'
		// ));


		$router->add('/events', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'eventslist'
		));

		$router->add('/find-a-hero', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'findahero'
		));
		// $router->add('/friends-and-allies/registration', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'faregistration'
		// ));


		$router->add('/page404', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'page404'
		));

		$router->add('/coming-soon', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'comingsoon'
		));

		




		$router->add('/ecoadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/ecoadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1
		));

		$router->add('/ecoadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
		));

		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	    return new Phalcon\Mvc\Model\Manager();
	});

        $config = include "../app/config/config.php";
        // Store it in the Di container
        $di->set('config', function () use ($config) {
            return $config;
        });
        
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));



                $config = include __DIR__ . "/../app/config/config.php";
                //rainier path windows


        
	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
