/* global console:false, google:false */
/*jshint unused:false */
'use strict';

app.controller('MapCtrl', ['$scope', '$http', 'API_URL', '$timeout', '$stateParams', '$state', function ($scope, $http, API_URL, $timeout, $stateParams, $state) {

        $scope.myMarkers = [];
        var editPage = false;
        $scope.saveMarker = false;
        $scope.mapOptions = {
            center: new google.maps.LatLng(35.784, -78.670),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        if ($stateParams.hasOwnProperty('id')) {
            console.log($stateParams.id);
            $http({
                url: API_URL + "/peacemap/getmap/" + $stateParams.id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                console.log(data);
                $scope.map = {
                    title: data[0].title,
                    description: data[0].description
                };
                for (var n in data[0].peacelocations) {
                    var PinMap = "";
                    var myLatlng = new google.maps.LatLng(data[0].peacelocations[n]['lataaa'], data[0].peacelocations[n]['longaaa']);
                    console.log(myLatlng);
                    PinMap = new google.maps.Marker({
                        map: $scope.myMap,
                        position: myLatlng
                    });
                    PinMap.set('labelContent', data[0].peacelocations[n]['labelContent']);
                    $scope.myMarkers.push(PinMap);
                    $scope.currentMarker = PinMap;
                    $scope.currentMarkerTitle = PinMap.get('labelContent');
                }
                // $scope.map = {};
                // $scope.form1.$setPristine(true);
                // for (var i = 0; i < $scope.myMarkers.length; i++) {
                //     $scope.myMarkers[i].setMap(null);
                // }
                // $scope.alerts.push({type: 'success', msg: 'Successfully created peacemap.'});
                // $scope.myMarkers = {};
                editPage = true;
            }).error(function (data, status, headers, config) {
                $scope.status = status;
            });
        }

        $scope.addMarker = function ($event, $params) {
            console.log($params);
            var PinMap = "";
            PinMap = new google.maps.Marker({
                map: $scope.myMap,
                position: $params[0].latLng
            });
            $scope.myMarkers.push(PinMap);
            $scope.currentMarker = PinMap;
            $scope.currentMarkerTitle = PinMap.get('labelContent');
            $scope.myInfoWindow.open($scope.myMap, PinMap);
        };

        $scope.setZoomMessage = function (zoom) {
            $scope.zoomMessage = 'You just zoomed to ' + zoom + '!';
            console.log(zoom, 'zoomed');
        };

        $scope.openMarkerInfo = function (marker) {
            $scope.currentMarker = marker;
            $scope.currentMarkerTitle = marker.get('labelContent');
            $scope.myInfoWindow.open($scope.myMap, marker);
        };

        $scope.setMarkerPosition = function (marker, title) {
            $scope.saveMarker = true;
            $timeout(function () {
                $scope.saveMarker = false;
            }, 1000);
            marker.set('labelContent', title);
        };

        $scope.deleteMark = function (index) {
            console.log(index);
            var marker = $scope.myMarkers[index];
            marker.setMap(null);
            $scope.myMarkers.splice(index, 1);
            console.log($scope.myMarkers);
        }

        $scope.saveMap = function (map) {
            var error = true;
            $scope.alerts = [];
            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
            for (var key in $scope.myMarkers) {
                if (hasOwnProperty.call($scope.myMarkers, key)) {
                    error = false;
                }
            }

            if ($scope.myMarkers.length === 0) {
                error = true;
            }

            if (error) {
                $scope.alerts.push({type: 'warning', msg: 'Please mark a pin on the map!'});
            } else {
                var maps = $scope.myMarkers;
                var mapcoordinates = Array();
                for (var mar in maps) {
                    mapcoordinates.push({'long': maps[mar].position.B, 'lat': maps[mar].position.k, 'labelContent': maps[mar]['labelContent']});
                }

                map['coordinates'] = mapcoordinates;
                console.log(map);

                var postUrl = "/peacemap/create";
                var message = 'Successfully created peacemap.';
                if (editPage) {
                    postUrl = "/peacemap/update";
                    message = 'Successfully updated peacemap.';
                    map['id'] = $stateParams.id;
                }

                $http({
                    url: API_URL + postUrl,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(map)
                }).success(function (data, status, headers, config) {
                    console.log(data);

                    for (var i = 0; i < $scope.myMarkers.length; i++) {
                        $scope.myMarkers[i].setMap(null);
                    }
                    if (editPage) {

                    } else {
                        $scope.map = {};
                        $scope.form1.$setPristine(true);

                        $scope.myMarkers = {};
                    }
                    $scope.alerts.push({type: 'success', msg: message});
                }).error(function (data, status, headers, config) {
                    $scope.status = status;
                });
            }
        }
    }]);

app.directive('uiEvent', ['$parse',
    function ($parse) {
        return function ($scope, elm, attrs) {
            var events = $scope.$eval(attrs.uiEvent);
            angular.forEach(events, function (uiEvent, eventName) {
                var fn = $parse(uiEvent);
                elm.bind(eventName, function (evt) {
                    var params = Array.prototype.slice.call(arguments);
                    //Take out first paramater (event object);
                    params = params.splice(1);
                    fn($scope, {$event: evt, $params: params});
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
            });
        };
    }]);