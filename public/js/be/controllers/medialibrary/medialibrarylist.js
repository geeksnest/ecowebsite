'use strict';

/* Controllers */

app.controller('medialibrarylistCtrl', function($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout, $sce, $q, Config, Upload, $filter,$window) {

  console.log("video");
  $scope.data = {};

  $scope.bannerFolder = 'medialibraryImages';
  var loadBanner = function () {
    $http({
      url: API_URL+"/medialibrary/banner",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.dataBanner = data[0];
      console.log(data[0]);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadBanner();

  $scope.toview = function(slugs){
    console.log('click');
    window.open('/medialibrary/' + slugs, '_blank');
  }

  $scope.editimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'editNewsBanner.html',
      controller: editImageCTRL,
      windowClass: 'app-modal-window',

      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }

  var editImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.isLoading = false;
    $scope.imgID = imgID;
    $scope.imgtype = 'banner';
    $scope.bannerFolder = 'medialibraryImages';
    $scope.amazonlink = Config.amazonlink;
    $scope.tfsize = [];
    $scope.dfsize = [];
    $scope.imageselected = false;
    $scope.bigImages = false;
    $scope.changepic = false;

    for (var x = 30; x <= 45; x++) {
      $scope.tfsize.push({'val': x})
    }

    for (var y = 12; y <= 20; y++) {
      $scope.dfsize.push({'val': y})
    }

    var loadBannerIMG = function () {
      $http({
        url: API_URL+"/medialibrary/banner",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0];
        console.log(data[0]);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadBannerIMG();

    $scope.save = function (imgdata,banner) {

      if($scope.changepic == true){
        console.log('initializing..');
        console.log('checking image size..');
        console.log(banner);
        var file = banner[0];
        if (file && file.length) {
          if (file.size >= 2000000) {
            console.log('image size too large');
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            cope.imagecontent=true;
          }
        }
        else{
          console.log('uploading image..');
          $scope.isLoading = true;
          var promises;
          promises = Upload.upload({
            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/medialibraryImages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){
            console.log('saving data..');
            imgdata['img'] = data.config.file.name;
            $http({
              url: API_URL + "/banner/bannerupdateImage",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imgdata)
            }).success(function (data, status, headers, config) {
              $modalInstance.dismiss('cancel');      
              $scope.isLoading = false;
              loadBanner();
            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });
          });
        }
      }else{

        console.log(imgdata);
        $scope.isLoading = true;

        $http({
          url: API_URL + "/banner/bannerupdateImage",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(imgdata)
        }).success(function (data, status, headers, config) {
          $modalInstance.dismiss('cancel');      
          $scope.isLoading = false;
          loadBanner();
        }).error(function (data, status, headers, config) {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        });

      }

      

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.prepare = function(file) {

      if (file && file.length) {
        if (file[0].size >= 2000000) {
          console.log('File is too big!');          
          $scope.imgbanner = [];
          $scope.changepic = false;
          $scope.bigImages = true;
        } else {
          console.log("below maximum");
          $scope.imgbanner = file;
          $scope.imgname = file[0].name;
          $scope.imageselected = true;
          $scope.changepic = true;
          $scope.bigImages = false;
          console.log(file);
        }
      }
    }
  };

  var num = 10;
  var off = 1;
  var keyword = undefined;
  var stat = undefined;
  $scope.advancesearch = false;
  var sort = 'datecreated';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword , stat) {
    $scope.loadingdata = true;

    if (keyword == ''){ keyword == null; }
    
    $http({
      url: API_URL+"/medialibrary/list/" + num + '/' + off + '/' + keyword + '/' + stat  + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    });
  }

  paginate(off, keyword , stat);

  $scope.sortType = function(keyword,stat, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword , stat);
  }

  $scope.refresh = function() {
    num = 10;
    off = 1;
    keyword = undefined;
    stat = undefined;
    $scope.status = "";
    $scope.searchtext = undefined;
    $scope.recurring = [];
    $scope.recurring.year = "";
    $scope.recurring.month = "";
    $scope.recurring.day = "";
    paginate(off, 'undefined' , 'undefined');
  }

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };


  $scope.searchbydatepub = function(datepublish){
    console.log(datepublish);
    var year,month,day;
    if(datepublish.year==undefined || datepublish.year==""){
      year = "";
    }else{
      year = datepublish.year;
    }
    if(datepublish.month==undefined || datepublish.month==""){
      month = "";
    }else{
      month = "-" + datepublish.month + "-";
    }
    if(datepublish.day==undefined || datepublish.day==""){
      day = "";
    }else{
      day = datepublish.day;
    }
    if(datepublish.year!=undefined && datepublish.month==undefined && datepublish.day!=undefined){
      $scope.alerts.push({type: 'danger', msg: 'Invalid Search of Date'});
      keyword = undefined;
    }else{
      keyword = year + month + day;
    }
    paginate(off, keyword , stat);
  } 

  $scope.search = function (keywordz,stat) {
    if(stat == ""){stat = undefined} 
    if(keywordz == ""){keywordz = undefined}  
    keyword = keywordz;
    paginate(off, keyword,stat);
  }


  $scope.setPage = function (pageNo, keywordz , stat) {
    if(stat == ""){stat = undefined}  
    var off = pageNo;
    paginate(off, keyword , stat);
  };

  $scope.review = function(id){
    $scope.id = id;
    var modalInstance = $modal.open({
      templateUrl: 'reviewVideo.html',
      controller: reviewVideoCTRL,
      windowClass: 'app-modal-window',
      resolve: {
        id: function () {
          return id
        }
      }
    });
  }

  var reviewVideoCTRL = function ($scope, $modalInstance, id, $state) {
    $scope.id = id;
    $scope.amazonlink = Config.amazonlink;

    var loadVids = function(){
      $http({
        url: API_URL+"/mymedialibrary/viewvideo/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0]; 
        $scope.datadescription = $sce.trustAsHtml(data[0].description);
        $scope.video = data[0].video; 
        $scope.countlikes = data[0].likes;
        $scope.views = data[0].viewscount;
        $scope.preview = $sce.trustAsHtml(data[0].embed);
        if(data[0].videotype == 'upload'){
          var playerInstance = jwplayer("myElement");
          playerInstance.setup({
            file: $scope.amazonlink+"/videos/"+data[0].video,
            image: $scope.amazonlink+"/videos/thumbs/"+data[0].videothumb,
            title: data[0].title,
            description: data[0].title,
            mediaid: data[0].id
          });
        }
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadVids();

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    $scope.contact = function(){
      var modalInstance = $modal.open({
            templateUrl: 'mediacontact.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
              var loadVids = function(){
                $http({
                  url: API_URL+"/mymedialibrary/viewvideo/" + id,
                  method: "GET",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                  $scope.data = data[0];
                }).error(function (data, status, headers, config) {
                  $scope.status = status;
                });
              }
              loadVids();

                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }    

    $scope.setfeatured = function(featid){
      $http({
        url: API_URL+"/medialibrary/setfeatured/" + featid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        loadVids();
        paginate(off, keyword , stat);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }


    $scope.resolution = function(id, status){
      $scope.id = id;
      $scope.status = status;
      var modalInstance = $modal.open({
        templateUrl: 'appDisappProj.html',
        controller: appDisappProjCTRL,
        resolve: {
          id: function () {return id},
          status: function () {return status}
        }
      });
    }
    var appDisappProjCTRL = function ($scope, $modalInstance, id, status, $state) {
      $scope.id = id;
      $scope.status = status;
      $scope.msgsent = false;
      if(status == 'Approved'){
        $scope.modalTitle = "Approve Video";
        $scope.panelMode = "panel-success";
        $scope.note = "Approve this Video?";
        $scope.disapprove = false;
        $scope.bott = 'Yes';
        $scope.send = function (msg) {
          var data = [];
          data['id'] = id;
          data['status'] = status;
          $http({
            url: API_URL+"/medialibrary/resolution/" + id + "/" + status,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.msgsent = true;
            loadVids();
            paginate(off, keyword , stat);
            $modalInstance.dismiss('cancel');
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        }
      }else{
        $scope.modalTitle = "Disapprove Video";
        $scope.panelMode = "panel-danger";
        $scope.note = "Please compose a letter why you disapprove this Video to inform the user.";
        $scope.disapprove = true;
        $scope.bott = 'Send';
        $scope.send = function (msg) {
        console.log(msg);
        $http({
          url: API_URL+"/medialibrary/resolution/" + id + "/" + status,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(msg)
        }).success(function (data, status, headers, config) {
          console.log(data);
          $scope.msgsent = true;
          loadVids();
          paginate(off, keyword , stat);
          $modalInstance.dismiss('cancel');
        }).error(function (data, status, headers, config) {
          $scope.status = status;
        });

      }
      };
      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };
    }
  }

  $scope.delete = function (id) {
    var modalInstance = $modal.open({
      templateUrl: 'deleteMyVideo.html',
      controller: deletevidCTRL,
      resolve: {
        id: function () {
          return id
        }
      }
    });
  };
  var deletevidCTRL = function ($scope, $modalInstance, id) {
    $scope.id = id;
    $scope.ok = function (id) {
      $http({
        url: API_URL + "/mymedialibrary/deleteMyVideo/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword , stat);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.managetags = function (){
    var modalInstance = $modal.open({
      templateUrl: 'managetags.html',
      controller: managetagsCTRL,
      windowClass: 'helper-modal-window'
    });
  }
  var managetagsCTRL = function ($scope, $modalInstance) {
    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var key;
    $scope.alerts = [];
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.currentstatusshow = '';
    $scope.sortType     = 'tags';
    $scope.sortReverse  = false;
    $scope.searchFish   = '';
    var loadtags = function(off, keyword) {
      $http({
        url: API_URL + "/medialibrary/managetags/" + num + '/' + off + '/' + keyword,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        $scope.tag = data.data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      });
    }
    loadtags(off, keyword);

    $scope.refresh = function () {
      $scope.searchtext = undefined;
      key = document.getElementById('tagtext').value = "";
      keyword = null;
      var off = 0;
      loadtags(off, undefined);
    }

    $scope.search = function (keywordz) {
      key = document.getElementById("tagtext").value;
      if(key==""){
        key = undefined;
      }
      loadtags(off, key);
      keyword = keywordz;
    }

    $scope.numpages = function (off, keywordz) {
      if(key==""){
        key = undefined;
      }
      loadtags(off, key);
    }

    $scope.setPage = function (pageNo) {
      if(key==""){
        key = undefined;
      }
      loadtags(pageNo, key);
      off = pageNo;
    };
    
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.ontags = function convertToSlug(Text)
    {
      if(Text == null){

      }else{
        var text1 = Text.replace(/[^\w ]+/g,'');
        var tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        $scope.mem.slugs = tagslugs;
        console.log(tagslugs);
      }

    }

    var notify = function(type){
       $scope.alerts.splice(0, 1);
       $scope.alerts.push({ type: 'success', msg: 'Tag successfully Deleted.' });
   }

    $scope.updatetags = function(data,memid,tag) {
      if(data != null){
        var text1 = data.replace(/[^\w ]+/g,'');
        var tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        tag['tags'] = data;
        tag['slugs'] = tagslugs;
      }

      $http({
        url: API_URL + "/medialibrary/updatetags",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(tag)
      }).success(function(data, status, headers, config) {
        if(data.hasOwnProperty('error')){
          $scope.alerts = [{ type: 'danger', msg: 'Tag name already in use.' }];
          if(key==""){
            key = undefined;
          }
          loadtags(off, key);
        }else{
          $scope.alerts = [{ type: 'success', msg: 'Tag successfully updated!' }];
          if(key==""){
            key = undefined;
          }
          loadtags(off, key);
        }
      });

    }

    $scope.tagsDelete = function(id) {
      var modalInstance = $modal.open({
        templateUrl: 'tagsDelete.html',
        controller: deletetagsInstanceCTRL,
        resolve: {
          id: function() {
            return id
          }
        }
      });
    }

    var deletetagsInstanceCTRL = function($scope, $modalInstance, id, $state) {

      $scope.ok = function() {
        $scope.alerts = [];
        var tags = {'tags': id};
        $http({
          url: API_URL + "/medialibrary/tagsdelete/" + id,
          method: "POST",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(tags)
        }).success(function(data, status, headers, config) {
          $modalInstance.close();
          notify('delete');
          if(key==""){
            key = undefined;
          }
          loadtags(off, key);
        });
      }

      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.addtags = function() {
      var modalInstance = $modal.open({
        templateUrl: 'addtags.html',
        controller: addtagsCTRL,
        resolve: {

        }
      });
    }

    var addtagsCTRL = function($scope, $modalInstance) {
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      var tagslugs = '';
      $scope.ontags = function convertToSlug(Text){
        if(Text != null){
          var text1 = Text.replace(/[^\w ]+/g,'');
          tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        $scope.disablebutton = false;
        var tagstitle = Text;
        $scope.alerts = [];

        $http({
          url: API_URL + "/medialibrary/tagstitle/"+ tagstitle,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (response) {
          if(key==""){
            key = undefined;
          }
          if (response.hasOwnProperty('tagsexist')) {
             $scope.alerts.splice(0, 1);
            $scope.alerts.push({type: 'danger', msg: 'The tag is already exist. Please enter another tag.'});
            $scope.disablebutton = true;
            loadtags(off, key);
          } else{
            $scope.disablebutton = false;
            loadtags(off, key);
          }

        }).error(function (response) {
          $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
        });
      }
      $scope.ok = function(tags) {
        $scope.alerts = [];
        tags['slugs'] = tagslugs;
        $http({
          url: API_URL + "/medialibrary/savetags",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(tags)
        }).success(function(data, status, headers, config) {
          if(key==""){
            key = undefined;
          }
          loadtags(off, key);
          document.getElementById("title").value = "";
          $scope.tags = [];
          $scope.tags.tags = "";
          $scope.success = true;
          $scope.disablebutton = true;
          $scope.alerts.splice(0, 1);
          $scope.alerts.push({ type: 'success', msg: 'Tag successfully Added' });
        }).error(function(data, status, headers, config) {
          $scope.status = status;
        });
      }
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      }

    }
  }


  ///MANAGE CATEGORY
  $scope.managecategory = function (){
    var modalInstance = $modal.open({
      templateUrl: 'managecategory.html',
      controller: managecategoryCTRL,
      windowClass: 'helper-modal-window'
    });
  }
  var managecategoryCTRL = function ($scope, $modalInstance) {
    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var key;
     $scope.alerts = [];
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.currentstatusshow = '';
    $scope.sortType     = 'tags';
    $scope.sortReverse  = false;
    $scope.searchFish   = '';
    var loadcategory = function(off, keyword) {
      $http({
        url: API_URL + "/medialibrary/managecategory/" + num + '/' + off + '/' + keyword,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        $scope.category = data.data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      });
    }
    loadcategory(off, keyword);

    $scope.refresh = function () {
      $scope.searchtext = undefined;
      key = document.getElementById('categorytext').value = "";
      keyword = null;
      var off = 0;
      loadcategory(off, keyword);
    }

    $scope.search = function (keyword) {
      key = document.getElementById("categorytext").value;
      if(key==""){
        key = undefined;
      }
      var off = 0;
      loadcategory(off, key);
      keyword = keyword;
    }

    $scope.numpages = function (off, keyword) {
      loadcategory(off, keyword);
    }

    $scope.setPage = function (pageNo) {
      if(key==""){
        key = undefined;
      }
      loadcategory(pageNo, key);
      off = pageNo;
    };
    
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.ontags = function convertToSlug(Text)
    {
      if(Text == null){

      }else{
        var text1 = Text.replace(/[^\w ]+/g,'');
        var tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        $scope.mem.slugs = tagslugs;
        console.log(tagslugs);
      }

    }
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    var notify = function(type){
      if(type=='add'){
         $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: 'success', msg: 'Category successfully Added' });
      }else if(type=='delete'){
         $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: 'success', msg: 'Category successfully Deleted' });
      }
    }

    $scope.updatecategory = function(data,memid,category) {
      if(data != null){
        var text1 = data.replace(/[^\w ]+/g,'');
        var catslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        category['categoryname'] = data;
        category['categoryslugs'] = catslugs;
      }

      $http({
        url: API_URL + "/medialibrary/updatecategory",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(category)
      }).success(function(data, status, headers, config) {
        if(data.hasOwnProperty('error')){
           $scope.alerts.splice(0, 1);
          $scope.alerts = [{ type: 'danger', msg: 'Category name already in use.' }];
          if(key==""){
            key = undefined;
          }
          loadcategory(off, key);
        }else{
          $scope.alerts = [{ type: 'success', msg: 'Category successfully updated!' }];
          if(key==""){
            key = undefined;
          }
          loadcategory(off, key);
        }
      });

    }

    $scope.categoryDelete = function(id) {
      var modalInstance = $modal.open({
        templateUrl: 'deletecategory.html',
        controller: deletecategoryInstanceCTRL,
        resolve: {
          id: function() {
            return id
          }
        }
      });
    }

    var deletecategoryInstanceCTRL = function($scope, $modalInstance, id, $state) {
      /*$scope.delcat_alerts = [];
      $scope.delcat_closeAlert = function(index) {
        $scope.delcat_alerts.splice(index, 1);
      }

      $scope.del_maxSize = 5;
      var page = 1;

      $scope.hasNews = true;
      var attempt = function(page) {
        $http({
          url: API_URL + "/categoryml/attemptdelete",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({categoryid:id,page:page})
        }).success(function (data, status, headers, config) {
          if(data.totalitems == 0) {
            $scope.hasNews = false;
            console.log("false")
          } else {
            $scope.hasNews = true;
            $scope.editablenews = data.editable;
            $scope.categories = data.categorylist;
            $scope.del_TotalItems = data.totalitems;
            $scope.del_CurrentPage = data.index;
            console.log(data)
          }
        });
      }
      attempt(page);
      $scope.del_setPage = function(page) {
        attempt(page);
      }

      $scope.newsupdatecategory = function(news) {
        if(news.categories != undefined) {
          NewsFactory.newsupdatecategory(news, function(data){
            $scope.delcat_alerts[0] = { type:data.type, msg:data.msg };
            $timeout(function(){
              $scope.delcat_alerts.splice(0, 1);
            },7000)
            attempt($scope.del_CurrentPage);
            console.log(data);
          })
        }
      }*/
      $scope.ok = function() {
        $http({
          url: API_URL + "/medialibrary/categorydelete/" + id,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config) {
          if(key==""){
            key = undefined;
          }
          loadcategory(off, key);
          $modalInstance.close();
          notify('delete');
        });
      }

      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.addcategory = function() {
      var modalInstance = $modal.open({
        templateUrl: 'addcategory.html',
        controller: addcategoryCTRL,
        resolve: {

        }
      });
    }

    var addcategoryCTRL = function($scope, $modalInstance) {
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      var slugs = '';
      $scope.oncategorytitle = function convertToSlug(Text){
        if(Text != null){
          var text1 = Text.replace(/[^\w ]+/g,'');
          slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        $scope.alerts = [];
        $scope.disablebutton = false;
        var title = Text;
        $http({
          url: API_URL + "/medialibrary/categorytitle/"+ title,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (response) {
          if (response.hasOwnProperty('Categoryexist')) {
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({type: 'danger', msg: 'The Category is already exist. Please enter another Category.'});
            $scope.disablebutton = true;
          } else{
            $scope.disablebutton = false;
          }

        }).error(function (response) {
          $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
        });
      }
      $scope.ok = function(category) {
        $scope.alerts = [];
        category['categoryslugs'] = slugs;
        $http({
          url: API_URL + "/medialibrary/savecategory",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(category)
        }).success(function(data, status, headers, config) {
          if(key==""){
            key = undefined;
          }
          loadcategory(off, key);
          document.getElementById("catnames").value = "";
          $scope.category = [];
          $scope.category.catnames = "";
          $scope.success = true;
          $scope.disablebutton = true;
          $scope.alerts.splice(0, 1);
          $scope.alerts.push({ type: 'success', msg: 'Category successfully Added' });
        }).error(function(data, status, headers, config) {
          $scope.status = status;
        });
      }
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      }

    }
  }

});

app.filter('daysleft', function(){    
  return function (item) {
   var days = item.toString().split(" ");
   return days[1];
 };
});

app.filter('putcomma', function(){    
  return function (item) {
    if(item){
      var parts = item.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }

  };
});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});