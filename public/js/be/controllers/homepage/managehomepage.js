'use strict';

/* Controllers */
app.controller('ManagehomepageCtrl', function ($http, $scope, $stateParams, $modal, $sce) {
  //LIST 
  $scope.loading = false;

  var paginate = function() {
    $scope.loading = true;
    $http({
      url: API_URL +"/homepage/list/",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data, status, headers, config) {
      $scope.data = data.data;
      $scope.loading = false;
    });
  };

  $scope.updatesort = function(wew){
    console.log(wew);
    var content = {
      id : wew
    };
    $http({
      url: API_URL + "/homepage/sortcontent",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(content)
    }).success(function(data, status, headers, config) {
       $scope.alerts.splice(0, 1);
      $scope.alerts.push({
        type: "success",
        msg: "Updated"
      });
    });  
  };


  paginate();

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(data) {
    console.log(data);
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({
      type: data.type,
      msg: data.msg
    });
  };
  //END LISTING


  //DELETE
  $scope.delete = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'delete.html',
      controller: dltCTRL,
      resolve: {
        dlt: function() {
          return id;
        }
      }
    });
  };

  var dltCTRL = function($scope, $modalInstance, dlt) {
    $scope.id = dlt;
    $scope.modaltitle = "Are you sure you want to delete this record?"
    $scope.ok = function() {
      $http({
        url:API_URL +"/homepage/delete/"+dlt,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        paginate();
        $modalInstance.dismiss('cancel');
        alertme(data);
        $scope.keywordshow = false;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
  //UPDATE USER
  $scope.update = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'homepageUpdate.html',
      controller: updateCTRL,
      resolve: {
        update: function() {
          return id;
        }
      }
    });
  };
  var updateCTRL = function($scope, $modalInstance, update, $state) {
    $scope.update = update;
    $scope.ok = function(update) {
      $state.go('edithomepage', {
        contentid: update
      });
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

});
app.directive('customPopover', function () {
    return {
        restrict: 'A',
        template: '<span>{{label}}</span>',
        link: function (scope, el, attrs) {
            scope.label = attrs.popoverLabel;
            $(el).popover({
                trigger: 'click',
                html: true,
                content: attrs.popoverHtml,
                placement: attrs.popoverPlacement
            });
        }
    };
});