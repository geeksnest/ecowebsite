'use strict';

/* Controllers */

/*
⊂_ヽTHIS IS
　 ＼＼ ＿
　　 ＼(　•_•) F
　　　 <　⌒ヽ A
　　　/ 　 へ＼ B
　　 /　　/　＼＼ U
　　 ﾚ　ノ　　 ヽ_つ L
　　/　/ O
　 /　/| U
　(　(ヽ S
　|　|、＼
　| 丿 ＼ ⌒)
　| |　　) /
`ノ )　　Lﾉ
(_／﻿
  */

app.controller('clublistCtrl', function($http, $modal, Countries, $state, $scope, $parse, $location, $anchorScroll, $timeout, $sce, $q, Config, Upload, $filter,$window) {

  var num = 10;
  var off = 1;
  var keyword = undefined;
  $scope.advancesearch = false;
  var sort = 'date_created';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword , stat) {
    $scope.loadingdata = true;

    if (keyword == ''){ keyword == null; }
    
    $http({
      url: API_URL+"/club/list/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    });
  }

  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.refresh = function() {
    num = 10;
    off = 1;
    keyword = undefined;
    $scope.searchtext = undefined;
    $scope.recurring = [];
    $scope.recurring.year = "";
    $scope.recurring.month = "";
    $scope.recurring.day = "";
    paginate(off, 'undefined');
  }

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.searchbydate = function(datepublish){
    console.log(datepublish);
    var year,month,day;
    if(datepublish.year==undefined || datepublish.year==""){
      year = "";
    }else{
      year = datepublish.year;
    }
    if(datepublish.month==undefined || datepublish.month==""){
      month = "";
    }else{
      month = "-" + datepublish.month + "-";
    }
    if(datepublish.day==undefined || datepublish.day==""){
      day = "";
    }else{
      day = datepublish.day;
    }
    if(datepublish.year!=undefined && datepublish.month==undefined && datepublish.day!=undefined){
      $scope.alerts.push({type: 'danger', msg: 'Invalid Search of Date'});
      keyword = undefined;
    }else{
      keyword = year + month + day;
    }
    paginate(off, keyword);
  } 

  $scope.search = function (keywordz) {
    if(keywordz == ""){keywordz = undefined}  
    keyword = keywordz;
    paginate(off, keyword);
  }


  $scope.setPage = function (pageNo, keywordz) { 
    var off = pageNo;
    paginate(off, keyword);
  };

$scope.edit = function(id,off){
    $scope.id = id;
    console.log(id)
    var modalInstance = $modal.open({
      templateUrl: 'editclub.html',
      controller: editclubCTRL,
      size: 'lg',
      resolve: {
        id: function () {
          return id
        },
        off: function () {
          return off
        }
      }
    });
  }

var editclubCTRL = function ($scope, $modalInstance, id, off, $state,$anchorScroll) {
     $scope.amzon = Amazon_link;
     $scope.countries = Countries.list();
     $scope.id = id;
       $scope.onslugs = function(Text){ 
    if(Text != null){
        var text1 = Text.replace(/[^\w -]+/g,'');
       $scope.data.slugs = angular.lowercase(text1.replace(/ +/g,'-')); 
     }
   }
   $scope.uniqueslugs = function(slugs){
    $http({
        url: API_URL+"/club/uniqueslugs/" + slugs,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
      
      });
       
    }
    var loaddata = function(){
      $http({
        url: API_URL+"/club/view/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0]; 
        if (data[0].age) {
          var sr = data[0].age;
          var s = sr.split("-");
          $scope.data.low = s[0];
          $scope.data.high = s[1];
        };
      
      });
    }
    loaddata();

$scope.submitclub = function(club,files){
  if ($scope.validslugs == true) {
     $('#slugs').focus();
  }
  else{
      if (club.low && club.high) {
        if (parseInt(club.low) > parseInt(club.high)) {
          $scope.edge = true;
          $('#high').focus();
        }else{
          club['age'] = club.low+'-'+club.high;
          saveme(club,files);
          $scope.edge = false;
        }
      } else if(club.low  && !club.high){
        $('#high').focus();
        $scope.edge = true;
      } else if(!club.low  && club.high){
       $('#low').focus();
       $scope.edge = true;
      } else{
        club['age'] = 0+'-'+0;
        saveme(club,files);
      }
 }
}
var saveme = function(club,files){
       $scope.edge = false;
       club['id'] = $scope.id;
       $scope.loadingMsg = 'Saving please wait...';
      if(!files){
        console.log(club)
        $scope.loadingg = true;
            $http({
              url: API_URL+"/club/update",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(club)
            }).success(function (data, status, headers, config) {
              console.log(data)
              $scope.loadingg = false;
              $scope.alerts =[{type: data.type, msg: data.msg}];
              loaddata();
              $location.hash('up');
            });
      }else{
        var file = files[0];
          if (file.size >= 2000000) {
            $scope.loadingg = false;
            $scope.alerts =[{type: 'danger', msg: 'Image File is too big.'}];
          }else{
            $scope.loadingg = true;
            $scope.alerts = [];
            var promises;
            var f = file.name;
            var fname=f.replace(/\s+/g, "-");
            promises = Upload.upload({
              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter, AllowedHeader) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/club/' + fname,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
            })
            promises.then(function(data){
             var ff = data.config.file.name;
             var fn = ff;
             club['image']=fn.replace(/\s+/g, "-");
              $http({
              url: API_URL+"/club/update",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(club)
            }).success(function (data, status, headers, config) {
              console.log(data)
              $scope.loadingg = false;
              $scope.alerts =[{type: data.type, msg: data.msg}];
              loaddata();
              $location.hash('up');
            });
              
            });
          }

      }
}

$scope.imageselected = false;
    $scope.prepare = function(file) {
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.imageselected = false;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      } else {
        // console.log("below maximum");
        $scope.data.image = file[0].name;
        $scope.projImg = file;
        console.log($scope.projImg);
        $scope.imgAlerts = [];
        $scope.imageselected = true;
      }
    }
  }

  $scope.closeAlerts = function (index) {
    $scope.imgAlerts.splice(index, 1);
  };
   $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

}

  $scope.review = function(id){
    $scope.id = id;
    var modalInstance = $modal.open({
      templateUrl: 'reviewclub.html',
      controller: reviewclubCTRL,
      size: 'lg',
      resolve: {
        id: function () {
          return id
        }
      }
    });
  }

  var reviewclubCTRL = function ($scope, $modalInstance, id, $state) {
     $scope.amzon = Amazon_link;
    var loaddata = function(){
      $http({
        url: API_URL+"/club/view/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0]; 

      });
    }
    loaddata();


    $scope.resolution = function(id, status){
      $scope.id = id;
      $scope.status = status;
      var modalInstance = $modal.open({
        templateUrl: 'appDisappProj.html',
        controller: appDisappProjCTRL,
        resolve: {
          id: function () {return id},
          status: function () {return status}
        }
      });
    }
    var appDisappProjCTRL = function ($scope, $modalInstance, id, status, $state) {
      $scope.id = id;
      $scope.status = status;
      $scope.msgsent = false;
      if(status == 'Approved'){
        $scope.modalTitle = "Approve Club";
        $scope.panelMode = "panel-success";
        $scope.note = "Approve this Club?";
        $scope.disapprove = false;
        $scope.bott = 'Yes';
        $scope.send = function (msg) {
          var data = [];
          data['id'] = id;
          data['status'] = status;
          $http({
            url: API_URL+"/club/resolution/" + id + "/" + status,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.msgsent = true;
            loaddata();
            paginate(off, keyword);
            $modalInstance.dismiss('cancel');
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        }
      }else{
        $scope.modalTitle = "Disapprove Club";
        $scope.panelMode = "panel-danger";
        $scope.note = "Please compose a letter why you disapprove this Club to inform the user.";
        $scope.disapprove = true;
        $scope.bott = 'Send';
        $scope.send = function (msg) {
          console.log(msg);
          var data = [];
          data['id'] = id;
          data['status'] = status;
          $http({
            url: API_URL+"/club/resolution/" + id + "/" + status,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(msg)
          }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.msgsent = true;
            loaddata();
            paginate(off, keyword);
            $modalInstance.dismiss('cancel');
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });

        }
      };
      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };
    }







    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };    
  }

  var notificationCTRL = function($scope, element, $modalInstance) {
        $scope.message = element.msg;
        $scope.type = element.type;
        $scope.okay= function() {
            $modalInstance.close();
        }
    }

  $scope.delete = function (id) {
    var modalInstance = $modal.open({
      templateUrl: 'delete.html',
      controller: deleteCTRL,
      resolve: {
        id: function () {
          return id
        }
      }
    });
  };
  var deleteCTRL = function ($scope, $modalInstance, id) {
    $scope.modaltitle = "Are you sure you want to delete this record?"
    $scope.id = id;
    $scope.ok = function () {
      $http({
        url: API_URL + "/club/deleteclub/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        var modalInstance = $modal.open({
          templateUrl: 'notification2.html',
          controller: notificationCTRL,
          resolve: {
            element: function() {
              var element = {'msg' : "Successfully Deleted!", 'type' : "info" };
              return element
            }
          }
        });
        paginate(off, keyword);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

});

app.filter('daysleft', function(){    
  return function (item) {
   var days = item.toString().split(" ");
   return days[1];
 };
});

app.filter('putcomma', function(){    
  return function (item) {
    if(item){
      var parts = item.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }

  };
});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});