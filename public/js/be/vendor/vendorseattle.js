'use strict';

/* Controllers */

app.controller('VendorseattleCtrl', function ($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout) {

  console.log("Vendor Seattle");
    
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var listIn = 'normal';
  var sortpaytype = "all";

  var sort = 'num';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  $scope.refresh = function() {
    var num = 10;
    var off = 1;
    var keyword = null;
    var sortpaytype = "all";
    $scope.paytypesort = [];
    paginate(off, keyword, sortpaytype);
  }

  $scope.paytype = function (keyword, type) {
    var off = 1;
    sortpaytype = type;
    paginate(off, keyword, sortpaytype);
    console.log(keyword);
    console.log(sortpaytype);
  }

  var paginate = function (off, keyword, sortpaytype) {

    $scope.loadingdata = true;

    $http({
      url: API_URL+"/vendor/seattlevendorlist/" + num + '/' + off + '/' + keyword + '/' + sortpaytype + '/' + $scope.sortIn + '/' + $scope.sortBy + '/' + listIn,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.totalcollections = putcomma(data.totalcollections);
      $scope.totalvendors = data.totalvendors;
      $scope.loadingdata = false;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword, sortpaytype, sort, sortto, listIn);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sortpaytype, sort, sortto, listIn);
  }

  $scope.search = function (keyword, sortpaytype) {
    if(keyword != undefined || keyword != null){
      var off = 1;
      paginate(off, keyword, sortpaytype, sort, sortto, listIn);
    }
    console.log(keyword);
  }

  $scope.numpages = function (off, keyword, sortpaytype) {
    paginate(off, keyword, sortpaytype);
  }

  $scope.setPage = function (pageNo, keyword, sortpaytype) {
    console.log(pageNo);
    console.log(keyword);
    console.log(sortpaytype);
    var off = pageNo;
    paginate(off, keyword, sortpaytype);
  };

  var deleteVendorCTRL = function ($scope, $modalInstance, vendorid, $state) {
    $scope.vendorid = vendorid;
    $scope.ok = function (vendorid) {
      console.log(vendorid);
      $http({
        url: API_URL + "/vendor/delete/" + vendorid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
  $scope.delete = function(vendorid){
    $scope.vendorid
    var modalInstance = $modal.open({
      templateUrl: 'deleteVendor.html',
      controller: deleteVendorCTRL,
      resolve: {
        vendorid: function () {
          return vendorid
        }
      }
    });
  }

  var viewVendorCTRL = function ($scope, $modalInstance, vendorid, $state) {
    // $scope.vendorid = vendorid;
    console.log(vendorid);
    var vendorview = function () {
      $http({
        url: API_URL + "/vendor/viewvendor/" + vendorid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        console.log(data);
        $scope.data = data;
        $scope.amount = putcomma(data.amount);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    vendorview();


    $scope.ok = function () {
      var stat = 1;
      $http({
        url: API_URL + "/vendor/markpaid/" + vendorid + "/" + stat,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword, sortpaytype);
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
  $scope.view = function(vendorid){
    $scope.vendorid
    var modalInstance = $modal.open({
      templateUrl: 'viewVendor.html',
      controller: viewVendorCTRL,
      resolve: {
        vendorid: function () {
          return vendorid
        }
      }
    });
  }

  var putcomma = function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

})