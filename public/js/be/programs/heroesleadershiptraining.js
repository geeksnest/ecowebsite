'use strict';

/* Controllers */

app.controller('hltCtrl', function ($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout) {

  console.log("Heroes Leadership Training");
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  
  var sort = 'id';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword) {
    $scope.loadingdata = true;

    if (keyword == ''){ keyword == null; }
    
    $http({
      url: API_URL+"/programs/hlt/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.refresh = function(searchtext,year,month,day) {

    num = 10;
    off = 1;
    keyword = null;
    searchtext = null;
    $scope.searchtext = [];
    year = null;
    month = null;
    day = null;
    paginate(off, keyword);
    if($scope.timestamp.year == undefined){
      $scope.timestamp.year = null;
    }else{
      $scope.timestamp.year = [];
    }
    if($scope.timestamp.month == undefined){
      $scope.timestamp.month = null;
    }else{
      $scope.timestamp.month = [];
    }
    if($scope.timestamp.day == undefined){
      $scope.timestamp.day = null;
    }else{
      $scope.timestamp.day = [];
    }
  } 

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword, timestamp, advancesearch) {
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword, timestamp, advancesearch) {
    var off = pageNo;
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  };


  $scope.searchtimestamp = function (timestamp,advancesearch) {

    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{
        var keyword = year + '-' + month + '-' + day;
      }

      console.log(keyword);
      paginate(off, keyword);
    }
  }



  $scope.viewHLT = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'viewHLT.html',
      controller: viewHLTCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }

  var viewHLTCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
     var hltview = function () {
      $http({
        url: API_URL + "/program/hltview/" + pageid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        console.log(data);
        $scope.data = data;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    hltview();


    
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.delete = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'deleteOtherDonationModal.html',
      controller: deletepageInstanceCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }
  var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
    $scope.ok = function (pageid) {
      var page = {'page': pageid};
      $http({
        url: API_URL + "/program/deletehtl/" + pageid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(page)
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }


})