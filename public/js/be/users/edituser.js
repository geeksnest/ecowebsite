'use strict';

/* Controllers */

app.controller('EditUserCtrl', function ($http, $scope, $stateParams, $location, $anchorScroll, Config, Upload, $modal, $filter, $timeout, $sce, $q) {



  $scope.usernametaken = false;
  $scope.emailtaken = false;
  $scope.invalidemail = false;
  $scope.passwordInvalid = false;
  $scope.passwordMin = false;
  $scope.imageselected = false;
  $scope.editusername = false;
  $scope.editemailaddress = false;
  $scope.validemailbot = true;
  $scope.validusernamebot = true;
  $scope.changepassword = false;
  $scope.validpassbot = true;
  $scope.changeRestrictions = false;
  $scope.editUserInfo = false;
  $scope.editInfo = false;
  $scope.changepic = false;




  var loadUserInfo = function(){
    $http({
      url: API_URL + "/users/edituser/" + $stateParams.userid ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.user = data;
      checkThis(data.userroles);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadUserInfo();


  var checksub = function(user) {
    console.log(user);
    if (user['donationlist'] == true || user['donationlist'] == 'donationlist'){
      console.log('donationlist');
      var donationlist = 'donationlist,';
    }else{var donationlist = '';}

    if (user['bnbregistration'] == true || user['bnbregistration'] == 'bnbregistration'){
      console.log('bnbregistration');
      var bnbregistration = 'bnbregistration,';
    }else{var bnbregistration = '';}

    if (user['donationothers'] == true || user['donationothers'] == 'donationothers'){
      console.log('donationothers');
      var donationothers = 'donationothers,';
    }else{var donationothers = '';}

    if (user['houstonearthcitizenswalk'] == true || user['houstonearthcitizenswalk'] == 'houstonearthcitizenswalk'){
      console.log('houstonearthcitizenswalk');
      var houstonearthcitizenswalk = 'houstonearthcitizenswalk,';
    }else{var houstonearthcitizenswalk = '';}

    if (user['nyearthcitizenwalk'] == true || user['nyearthcitizenwalk'] == 'nyearthcitizenwalk'){
      console.log('nyearthcitizenwalk');
      var nyearthcitizenwalk = 'nyearthcitizenwalk,';
    }else{var nyearthcitizenwalk = '';}

    if (user['seattlenaturalhealingexpo'] == true || user['seattlenaturalhealingexpo'] == 'seattlenaturalhealingexpo'){
      console.log('seattlenaturalhealingexpo');
      var seattlenaturalhealingexpo = 'seattlenaturalhealingexpo,';
    }else{var seattlenaturalhealingexpo = '';}

    user['donationAccess'] = donationlist + bnbregistration + donationothers + houstonearthcitizenswalk + nyearthcitizenwalk + seattlenaturalhealingexpo;
    console.log(user['donationAccess']);
  };


  $scope.prepare = function(file) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('File is too big!');
        $scope.alerts = [{
          type: 'danger',
          msg: 'File ' + file.name + ' is too big'
        }];
        $scope.file = '';
        $scope.changepic = false;
      } else {
        console.log("below maximum");
        $scope.file = file;
        $scope.closeAlert();
        $scope.imageselected = true;
        $scope.changepic = true;
      }
    }
  }

  $scope.chkUsername = function(user) {
    console.log(user);
    $http({
      url: API_URL + "/users/chkUsername",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.usernametaken = true;
        $scope.validusernamebot = false;
      } else{
        $scope.usernametaken = false;
        $scope.validusernamebot = true;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };

  $scope.chkEmail = function(user) {
    console.log(user);
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.validemailbot = false;
      $scope.invalidemail = true;
      console.log('invalid');
    }else{
      $scope.invalidemail = false;
      $scope.validemailbot = true;
      console.log('valid');
    }
    if(user.email == undefined || user.email == null){
      $scope.invalidemail = false;
      console.log('invalid wala laman');
    }
    $http({
      url: API_URL + "/users/chkEmail",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.emailtaken = true;
        $scope.validemailbot = false;
      } else{
        $scope.emailtaken = false;
        $scope.validemailbot = true;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };


  $scope.cancelEdit = function() {
    $scope.editusername = false;
    $scope.editemailaddress = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
    $scope.changepic = false;
    $scope.imageselected = false;
    loadUserInfo();
  };

  $scope.editUname = function() {
    $scope.editusername = true;
    $scope.editemailaddress = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveUname = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    $http({
      url: API_URL + "/users/updateusername/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Username already in use.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Username successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editusername = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.editEmail = function() {
    $scope.editemailaddress = true;
    $scope.editusername = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveEmail = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    $http({
      url: API_URL + "/users/updateuseremail/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Email Address already in use.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Email Address successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editemailaddress = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.changepass = function() {
    $scope.changepassword = true;
    $scope.editemailaddress = false;
    $scope.editusername = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveChangePass = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    $http({
      url: API_URL + "/users/changepassword/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Password successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.changepassword = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.minPass = function(user) {

    if(user.password != undefined || user.password != null ){
      if(user.password.length < 6){
        $scope.passwordMin = true;
        $scope.validpassbot = false;
      }else{
        $scope.passwordMin = false;
        // $scope.validpassbot = true;
      }
    }

  };

  $scope.chkPass = function(user) {
    console.log(user.confirm_password.length);
    console.log(user.password.length);
    if(user.password.length === user.confirm_password.length){
      if(user.password != user.confirm_password){
        $scope.passwordInvalid = true;
        $scope.validpassbot = false;
      }else{
        $scope.passwordInvalid = false;
        $scope.validpassbot = true;
      }
    } else if(user.password.length < user.confirm_password.length){
      $scope.passwordInvalid = true;
      $scope.validpassbot = false;
    }

  };

  $scope.chkPass2 = function() {
    $scope.user.confirm_password = [];
    $scope.validpassbot = false;
  };

  $scope.editRestrictions = function() {
    $scope.changeRestrictions = true;
    $scope.changepassword = false;
    $scope.editemailaddress = false;
    $scope.editusername = false;
    $scope.editUserInfo = false;
  };
  $scope.saveRestrictions = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    checksub(user);
    $http({
      url: API_URL + "/users/editRestrictions/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.alerts.push({type: 'success', msg: 'Restrictions successfully updated.'});
      $scope.imageloader = false;
      $scope.changeRestrictions = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };


  $scope.editInfo = function() {
    $scope.editUserInfo = true;
    $scope.changeRestrictions = false;
    $scope.changepassword = false;
    $scope.editemailaddress = false;
    $scope.editusername = false;
  };
  $scope.saveInfo = function(user) {
    console.log(user.birthday);
    console.log(user.birthday2);
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;



    if(user.birthday == user.birthday2){
      user['birthday2'] = 'dontchange';
    }else{
      user['birthday2'] = 'change';
    }

    $http({
      url: API_URL + "/users/updateuserInfo/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again or refresh the page.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'User information successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editUserInfo = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.updatePhoto = function (files, user){
    // console.log(files);
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $anchorScroll();
    console.log('initializing..');

    console.log('checking image size..');
    console.log(files);
    var file = files[0];
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('image size too large');
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        cope.imagecontent=true;
      }
    }
    else{
      console.log('uploading image..');
      $scope.imageloader=true;

      var promises;
      promises = Upload.upload({

        url: Config.amazonlink,
        method: 'POST',
        transformRequest: function (data, headersGetter) {
          var headers = headersGetter();
          delete headers['Authorization'];
          return data;
        },
        fields : {
          key: 'uploads/userimages/' + file.name,
          AWSAccessKeyId: Config.AWSAccessKeyId,
          acl: 'private',
          policy: Config.policy,
          signature: Config.signature,
          "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
        },
        file: file
      })
      promises.then(function(data){
        console.log('saving data..');
        user['image'] = data.config.file.name;
        $http({
          url: API_URL + "/users/updateProfilePhoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(user)
        }).success(function (data, status, headers, config) {
          if (data.hasOwnProperty('error')) {
            $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again or refresh the page.'});
          }else{
            $scope.alerts.push({type: 'success', msg: 'Profile photo successfully updated.'});
          }
          document.body.scrollTop = document.documentElement.scrollTop = 0;
          $scope.imageloader = false;
          $scope.changepic = false;
          loadUserInfo();
        }).error(function (data, status, headers, config) {
          $scope.status = status;
          console.log(data);
        });
        $scope.imageloader=false;
      });
    }


  }


  $scope.chkimage = function(files) {
    // console.log(files[0].name);

    $scope.imageselected = true;
    // files[0].name = 'default-page-thumb.png';
    // console.log(files);
    // $scope.files = files;
  };



  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.datepicker = {'opened': true};
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  var checkThis = function(arrayVal){

    for(var n in arrayVal){
      if($('#' + arrayVal[n]['roleCode'])){
        $scope.user[arrayVal[n]['roleCode']] = true;
        $('#' + arrayVal[n]['roleCode']).prop('checked', true);
      }
    }
  }



})