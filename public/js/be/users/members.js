'use strict';

/* Controllers */

app.controller('MembersCtrl', function ($http, $scope, $parse, $location, $anchorScroll, $modal, Countries, MDY) {

  //List of Members Query
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';

  $scope.sortReverse  = false; 
  $scope.searchFish   = ''; 

  var sort = 'userid';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  var loadmemberlist = function (off, keyword, sort, sortto) {
    $http({
      url: API_URL+"/members/memberslist/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadmemberlist(off, keyword, sort, sortto);


  $scope.sortType = function(sort, sortTo) {
    $scope.sortBy = sortTo;
    $scope.sortIn = sort;
    loadmemberlist(off, keyword, sort, sortto);
  }

  $scope.refresh = function() {
    var off = 1;
    var sort = 'userid';
    var sortto = 'DESC';
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    $scope.searchtext = [];
    $scope.searchbd = [];
    loadmemberlist(off, keyword, sort, sortto);
  }

  $scope.search = function (keyword) {
    loadmemberlist(off, keyword, sort, sortto);
    console.log(keyword)
  }

  $scope.numpages = function (off, keyword) {
    loadmemberlist(off, keyword, sort, sortto);
  }

  $scope.setPage = function (pageNo, keyword) {
    if(keyword == ''){keyword = null}
    loadmemberlist(pageNo, keyword, sort, sortto);
  };

  $scope.searchbydate = function (keyword,searchbd) {

    if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear == undefined){
      
      var birthdate = $scope.searchbd.bmonth;
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear == undefined){
      
      var birthdate = $scope.searchbd.bmonth + '-' + $scope.searchbd.bday;
    }
    else if($scope.searchbd.bmonth == undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear !== undefined){
      
      var birthdate = $scope.searchbd.byear;
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear !== undefined){
      
      var birthdate = $scope.searchbd.byear+ '-' + $scope.searchbd.bmonth + '-' + $scope.searchbd.bday;
    }else{

     var birthdate = '';
    }  


    console.log(birthdate);

    $http({
      url: API_URL+"/members/memberslistsearchbydate/" + num + '/' + off + '/' + birthdate,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });


  }

  $scope.numpagesbydate = function (off, keyword, searchbd) {

    if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear == undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '';
      var by = '';
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear == undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '-' + $scope.searchbd.bday;
      var by = '';
    }
    else if($scope.searchbd.bmonth == undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear !== undefined){
      var bm = '';
      var bd = '';
      var by = $scope.searchbd.byear;
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear !== undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '-' + $scope.searchbd.bday + '-';
      var by = $scope.searchbd.byear;
    }else{
      var bm = '';
      var bd = '';
      var by = '';
    }

    var birthdate = bm + '' + bd + '' + by;

    console.log(birthdate);

    $http({
      url: API_URL+"/members/memberslistsearchbydate/" + num + '/' + off + '/' + birthdate,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;

      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  $scope.setPagebydate = function (pageNo, keyword, searchbd) {

    if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear == undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '';
      var by = '';
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear == undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '-' + $scope.searchbd.bday;
      var by = '';
    }
    else if($scope.searchbd.bmonth == undefined && $scope.searchbd.bday == undefined && $scope.searchbd.byear !== undefined){
      var bm = '';
      var bd = '';
      var by = $scope.searchbd.byear;
    }
    else if($scope.searchbd.bmonth !== undefined && $scope.searchbd.bday !== undefined && $scope.searchbd.byear !== undefined){
      var bm = $scope.searchbd.bmonth;
      var bd = '-' + $scope.searchbd.bday + '-';
      var by = $scope.searchbd.byear;
    }else{
      var bm = '';
      var bd = '';
      var by = '';
    }

    var birthdate = bm + '' + bd + '' + by;

    console.log(birthdate);

    $http({
      url: API_URL+"/members/memberslistsearchbydate/" + num + '/' + pageNo + '/' + birthdate,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });

  };  


  $scope.edituser = function (userid) {
    var modalInstance = $modal.open({
      templateUrl: 'editMemberModal.html',
      controller: ModalInstanceCtrl,
      windowClass: 'helper-modal-window',
      backdrop : 'static',
      resolve: {
        userid: function () {
          return userid;
        }
      }
    });
  }
  var ModalInstanceCtrl = function ($scope, $parse, $modalInstance, userid) {
  

    var loadMemberInfo = function(){  
      $http({
        url: API_URL + "/members/memberinfo/" + userid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.user = data;
        $scope.bday = data.bday;
        $scope.bmonth = data.bmonth;
        $scope.byear = data.byear;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadMemberInfo();

    $scope.tycreditcashcheck = false;
    $scope.tycheck = false;

    $scope.tpaynone = function(){
      $scope.tycreditcashcheck = false;
      $scope.tycheck = false;
    }
    $scope.tpaycreditcash = function(){
      $scope.tycreditcashcheck = true;
      $scope.tycheck = false;
    }
    $scope.tpaycheck = function(){
      $scope.tycreditcashcheck = true;
      $scope.tycheck = true;
    }


    $scope.changemonth = function(bmonth){
      console.log(bmonth);
    }

    var loadCentername = function(){
      $http({
        url: API_URL +"/bnb/cnamelist",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
        $scope.dataCentername = data;
      }).error(function (data) {
        $scope.status = status;
      });
    }
    loadCentername();    

    $scope.usernametaken = '';
    $scope.emailtaken = '';
    $scope.success = false;
    $scope.process = false;
    $scope.countries = Countries.list();
    $scope.passerror = '';
    $scope.status = '';

    $scope.day = [
      {'val': '01'},
      {'val': '02'},
      {'val': '03'},
      {'val': '04'},
      {'val': '05'},
      {'val': '06'},
      {'val': '07'},
      {'val': '08'},
      {'val': '09'},
      {'val': '10'},
      {'val': '11'},
      {'val': '12'},
      {'val': '13'},
      {'val': '14'},
      {'val': '15'},
      {'val': '16'},
      {'val': '17'},
      {'val': '18'},
      {'val': '19'},
      {'val': '20'},
      {'val': '21'},
      {'val': '22'},
      {'val': '23'},
      {'val': '24'},
      {'val': '25'},
      {'val': '26'},
      {'val': '27'},
      {'val': '28'},
      {'val': '29'},
      {'val': '30'},
      {'val': '31'}
      ];
    $scope.month =  [
      {'m': 'January', 'val': '01'},
      {'m': 'February', 'val': '02'},
      {'m': 'March', 'val': '03'},
      {'m': 'April', 'val': '04'},
      {'m': 'May', 'val': '05'},
      {'m': 'June', 'val': '06'},
      {'m': 'July', 'val': '07'},
      {'m': 'August', 'val': '08'},
      {'m': 'September', 'val': '09'},
      {'m': 'October', 'val': '10'},
      {'m': 'November', 'val': '11'},
      {'m': 'December', 'val': '12'}
      ];    
    $scope.year = MDY.year();  


    var oriUser = angular.copy($scope.user);
    var oriPass = angular.copy($scope.pass);
    $scope.ok = function (user) {

      $scope.process = true;
      $http({
        url: API_URL+ "/members/memberupdate/" + userid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(user)
      }).success(function (data, status, headers, config) {
        if (data.hasOwnProperty('usernametaken') || data.hasOwnProperty('emailtaken')) {
          $scope.usernametaken = data.usernametaken;
          $scope.emailtaken = data.emailtaken;
        } else {
          $scope.usernametaken = '';
          $scope.emailtaken = '';
          $scope.success = true;
          $scope.user = angular.copy(oriUser);
          loadMemberInfo();
          $scope.process = false;
          // $modalInstance.close();
          // loadmemberlist(off, keyword);
          var i = 5;
          setInterval(function(){
            i--;
            if(i == 0)
            {
              $scope.currentstatusshow = 0;
              // $modalInstance.close();
            }
          },100)
        }
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };

    $scope.resetpass = function () {
      var modalInstance = $modal.open({
        templateUrl: 'resetpassModal.html',
        controller: resetpassCtrl,
      });       
    };
    var resetpassCtrl = function ($scope, $modalInstance, $state) { 
      $scope.reseting = false;
      $scope.gen = false;
      $scope.sending = false;
      $scope.sent = false;
      $scope.reset = function () {
        $scope.reseting = true;
        $scope.gen = true;
      var i = 5;
          setInterval(function(){
            i--;
            if(i == 0)
            {
              $scope.sending = true;
              $scope.gen = false;             
              $http({
                url: API_URL + "/members/memberupdatepassword/" + userid,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(userid)
              }).success(function (data, status, headers, config) {
                if (data.hasOwnProperty('error')) {
                  $scope.mess = "Updating Failed";
                } else {                  
                  $scope.sent = true;
                  $scope.sending = false;
                }

              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            }
          },100)
      }
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.resendcom = function (email) {

      $http({
        url: API_URL + "/members/completioncms/resendcom/" + email,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(email)
      }).success(function (data, status, headers, config) {
        if (data.hasOwnProperty('error')) {
          console.log("error!");
        } else {
          console.log("chaching complete!");
          var modalInstance = $modal.open({
            templateUrl: 'resendcompleteModal.html',
            controller: ModalInstanceCtrlresendcom,
            resolve: {
              userid: function () {
                return email;
              }
            }
          });
        }
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }

    $scope.resendcon = function (email) {

      $http({
        url: API_URL + "/members/confirmationcms/resendcon/" + email,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(email)
      }).success(function (data, status, headers, config) {
        if (data.hasOwnProperty('error')) {
          console.log("error!");
        } else {
          console.log("chachang confirm!");
          var modalInstance = $modal.open({
            templateUrl: 'resendconfirmModal.html',
            controller: ModalInstanceCtrlresendcom,
            resolve: {
              userid: function () {
                return email;
              }
            }
          });
        }
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
  };



  $scope.resendcom = function (email) {

    $http({
      url: API_URL + "/members/completioncms/resendcom/" + email,
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(email)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        console.log("error!");
      } else {


        console.log("chaching complete!");
        var modalInstance = $modal.open({
          templateUrl: 'resendcompleteModal.html',
          controller: ModalInstanceCtrlresendcom,
          resolve: {
            userid: function () {
              return email;
            }
          }
        });

      }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });


  }

  $scope.resendcon = function (email) {

    $http({
      url: API_URL + "/members/confirmationcms/resendcon/" + email,
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(email)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        console.log("error!");
      } else {

        console.log("chachang confirm!");
        var modalInstance = $modal.open({
          templateUrl: 'resendconfirmModal.html',
          controller: ModalInstanceCtrlresendcom,
          resolve: {
            userid: function () {
              return email;
            }
          }
        });

      }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }


  var ModalInstanceCtrlresendcom = function ($scope, $modalInstance, $state) {
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }



  $scope.deleteuser = function (userid, name){
    var modalInstance = $modal.open({
      templateUrl: 'deleteMemberModal.html',
      controller: DELModalInstanceCtrl,
      resolve: {
        userid: function () {
          return userid;
        },
        userfullname: function () {
          return name;
        },
      }
    });
  }
  var DELModalInstanceCtrl = function ($scope, $modalInstance, userid, userfullname) {
    $scope.userid=userid;
    $scope.userfullname=userfullname;
    $scope.process = false;
    $scope.success = false;
    $scope.ok = function (userid) {
      var user = {'user': userid};
      $scope.process = true;
      $http({
        url: API_URL + "/members/memberdelete/" + userid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(user)
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        loadmemberlist(off, keyword, sort, sortto);
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };


    
})
