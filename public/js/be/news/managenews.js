'use strict';

/* Controllers */

app.controller('ManageNewsCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload, $filter) {

  $scope.data = {};
  $scope.master = {};
  var orig = angular.copy($scope.master);
  var num = 10;
  var off = 1;
  var keyword = null;
  var sortingdate = null;
  

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  $scope.bannerFolder = 'newsimages';
  var loadBanner = function () {
    $http({
      url: API_URL+"/news/newsbanner",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data[0];
      console.log(data[0]);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadBanner();

  $scope.editimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'editNewsBanner.html',
      controller: editImageCTRL,
      windowClass: 'app-modal-window',

      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }

  var editImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.isLoading = false;
    $scope.imgID = imgID;
    $scope.imgtype = 'banner';
    $scope.bannerFolder = 'newsimages';
    $scope.amazonlink = Config.amazonlink;
    $scope.tfsize = [];
    $scope.dfsize = [];
    $scope.imageselected = false;
    $scope.bigImages = false;
    $scope.changepic = false;

    for (var x = 30; x <= 45; x++) {
      $scope.tfsize.push({'val': x})
    }

    for (var y = 12; y <= 20; y++) {
      $scope.dfsize.push({'val': y})
    }

    var loadBannerIMG = function () {
      $http({
        url: API_URL+"/news/newsbanner",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0];
        console.log(data[0]);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadBannerIMG();

    $scope.save = function (imgdata,banner) {

      if($scope.changepic == true){
        console.log('initializing..');
        console.log('checking image size..');
        console.log(banner);
        var file = banner[0];
        if (file && file.length) {
          if (file.size >= 2000000) {
            console.log('image size too large');
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            cope.imagecontent=true;
          }
        }
        else{
          console.log('uploading image..');
          $scope.isLoading = true;
          var promises;
          promises = Upload.upload({
            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/newsimages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){
            console.log('saving data..');
            imgdata['img'] = data.config.file.name;
            $http({
              url: API_URL + "/banner/bannerupdateImage",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imgdata)
            }).success(function (data, status, headers, config) {
              $modalInstance.dismiss('cancel');      
              $scope.isLoading = false;
              loadBanner();
            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });
          });
        }
      }else{

        console.log(imgdata);
        $scope.isLoading = true;

        $http({
          url: API_URL + "/banner/bannerupdateImage",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(imgdata)
        }).success(function (data, status, headers, config) {
          $modalInstance.dismiss('cancel');      
          $scope.isLoading = false;
          loadBanner();
        }).error(function (data, status, headers, config) {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        });

      }

      

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.prepare = function(file) {

      if (file && file.length) {
        if (file[0].size >= 2000000) {
          console.log('File is too big!');          
          $scope.imgbanner = [];
          $scope.changepic = false;
          $scope.bigImages = true;
        } else {
          console.log("below maximum");
          $scope.imgbanner = file;
          $scope.imgname = file[0].name;
          $scope.imageselected = true;
          $scope.changepic = true;
          $scope.bigImages = false;
          console.log(file);
        }
      }
    }
  };

  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;

  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];


  var sort = 'dateedited';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  var paginate = function(off, keyword, sortingdate, sort, sortto) {

    $http({
      url: API_URL+"/news/manage/" + num + '/' + off + '/' + keyword + '/' + sortingdate + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.list = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
      console.log(data);
    });
    
  }
  paginate(off, keyword, sortingdate, sort, sortto);

  var paginatefeat = function(off, keyword, sortingdate, sort, sortto) {

    $http({
      url: API_URL+"/news/managebyfeatcat/" + num + '/' + off + '/' + keyword + '/' + sortingdate + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.datafeat = data;
      console.log(data);
      $scope.featmaxSize = 5;
      $scope.featbigTotalItems = data.total_items2;
      $scope.featbigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
    
  }
  paginatefeat(off, keyword, sortingdate, sort, sortto);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  }

  $scope.refresh = function() {
    var sort = 'dateedited';
    var sortto = 'DESC';
    off = 1;
    num = 10;
    keyword = null;
    sortingdate = null;
    // $scope.featuredcat = angular.copy(orig);
    $scope.searchtext = null;
    $scope.sortbyfeaturednews = null;
    $scope.sortbydate = null;
    paginate(off, keyword);
    paginatefeat(off, keyword);
  }

  $scope.viewfeatcat = function() {
    $scope.searchtext = null;
    $scope.sortbyfeaturednews = null;
    $scope.sortbydate = null;
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  }

  $scope.sortby = function (keyword, sortingdate) {
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  }

  $scope.search = function (keyword, sortingdate) {
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  }

  $scope.numpages = function (off, keyword, sortingdate) {
    console.log(sortingdate);
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  }

  $scope.setPage = function (pageNo, keyword, sortingdate) {
    console.log(sortingdate);

    var off = pageNo;
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
  };

  $scope.sortdate = function (keyword, sortingdate) {
    paginate(off, keyword, sortingdate);
    paginatefeat(off, keyword, sortingdate);
    console.log(keyword);
  }

  var deletenewsInstanceCTRL = function($scope, $modalInstance, newsid, $state) {
    $scope.ok = function() {
      var news = {
        'news': newsid
      };
      $http({
        url: API_URL + "/news/newsdelete/" + newsid,
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(news)
      }).success(function(data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword);
        paginatefeat(off, keyword);
        $scope.success = true;
      }).error(function(data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.deletenews = function(newsid) {
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'deletenews.html',
      controller: deletenewsInstanceCTRL,
      resolve: {
        newsid: function() {
          return newsid
        }
      }
    });
  }

  var updatenewsInstanceCTRL = function($scope, $modalInstance, newsid, $state) {
    $scope.newsid = newsid;
    console.log(newsid);
    $scope.ok = function(newsid) {
      $scope.pageid = newsid;
      $state.go('editnews', {
        newsid: newsid
      });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.editnews = function(newsid) {
    $scope.newsid
    var modalInstance = $modal.open({
      templateUrl: 'updatenews.html',
      controller: updatenewsInstanceCTRL,
      resolve: {
        newsid: function() {
          return newsid
        }
      }
    });
  }

  $scope.onnewstitle = function convertToSlug(Text)
  {
    if(Text == null)
    {

    }
    else
    {
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }

  }

  $scope.setstatus = function (status,newsid,keyword,off,newslocation,newsslugs) {

    if(status == 1)
    {
      $http({
        url: API_URL + "/news/updatenewsstatus/0/" + newsid + '/' + keyword,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.currentstatusshow = newsslugs;
        var i = 2;
        setInterval(function(){
          i--;

          if(i == 0)
          {
            paginate(off, keyword);
            paginatefeat(off, keyword);
            $scope.currentstatusshow = 0;
          }
        },100)
      }).error(function (data, status, headers, config) {

      });
    }
    else
    {
      $http({
        url: API_URL + "/news/updatenewsstatus/1/" + newsid + '/' + keyword,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.currentstatusshow = newsslugs;
        var i = 2;
        setInterval(function(){
          i--;
          if(i == 0)
          {
            paginate(off, keyword);
            paginatefeat(off, keyword);
            $scope.currentstatusshow = 0;
          }
        },100)
      }).error(function (data, status, headers, config) {

      });
    }
  }



  $scope.setfeatstatus = function (feat,featurednews,newsid,keyword,off,newslocation,newsslugs) {

    var stat = feat;

    $http({
      url: API_URL + "/news/updatenewsfeatstatus/" + stat + '/' + featurednews + '/' + newsid + '/' + keyword,
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var i = 2;
      setInterval(function(){
        i--;
        if(i == 0)
        {
          paginate(off, keyword);
          paginatefeat(off, keyword);
        }
      },100)
    }).error(function (data, status, headers, config) {

    });

    // featstatus(stat, newsid, keyword);
    if(feat == 1)
    {
      console.log("feat1");
      var stat = feat;
      // featstatus(stat, newsid, keyword);
    }
    else
    {
      console.log("feat0");
      var stat = feat;
      // featstatus(stat, newsid, keyword);
    }
  }
})