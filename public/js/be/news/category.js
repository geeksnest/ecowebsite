'use strict';

/* Controllers */

app.controller('categoryNewsCtrl', function($scope, $state , $q, $http, $stateParams, $modal) {
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;

  
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.currentstatusshow = '';
  $scope.sortType     = 'categoryname';
  $scope.sortReverse  = false;
  $scope.searchFish   = '';


  var paginate = function(off, keyword) {
    $http({
      url: API_URL + "/news/managecategory/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }).success(function(data, status, headers, config) {
      $scope.data = data;
            // // console.log(data);
          }).error(function(data, status, headers, config) {
            $scope.status = status;
          });
        }
        $scope.refresh = function() {
          var off = 0;
          paginate(off, keyword);
        }
        $scope.search = function(keyword) {
          var off = 0;
          paginate(off, keyword);
        }
        $scope.paging = function(off, keyword) {
          paginate(off, keyword);
        }
        paginate(off, keyword);

        $scope.addcategory = function() {
          var modalInstance = $modal.open({
            templateUrl: 'addcategory.html',
            controller: addcategoryCTRL,
            resolve: {

            }
          });
        }




        var alertclose = function(index) {
          $scope.alerts.splice(index, 1);
        }
        var successloadalert = function(){
          $scope.alerts.push({ type: 'success', msg: 'Category successfully Added!' });
        }
        var updateloadalert = function(){
          $scope.alerts.push({ type: 'success', msg: 'Category successfully Updated!' });
        }

        var deleteloadalert = function(){
          $scope.alerts.push({ type: 'danger', msg: 'Category successfully Deleted!' });
        }
        var errorloadalert = function(){
          $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
        }
        var errorexistalert = function(){
          $scope.alerts.push({ type: 'danger', msg: 'Category name already in use!' });
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var addcategoryCTRL = function($scope, $modalInstance) {

          $scope.disablebutton = false;
          var categoryslugs = '';


          $scope.oncategorytitle = function convertToSlug(Text)
          {


            if(Text != null)
            {
              var text1 = Text.replace(/[^\w ]+/g,'');
              categoryslugs = angular.lowercase(text1.replace(/ +/g,'-'));
            }

            var cattitle = Text;
            $scope.alerts = [];

            $http({
              url: API_URL + "/news/categorytitle/"+ cattitle,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (response) {


              if (response.hasOwnProperty('categoryexist')) {
                $scope.alerts.push({type: 'danger', msg: 'The category is already exist. Please enter another category.'});
                // $scope.catform.title.$setValidity("buttonsubmit",false);
                $scope.disablebutton = true;
              } else{
                // $scope.catform.title.$setValidity("buttonsubmit",true);
                $scope.disablebutton = false;
              }

            }).error(function (response) {
              $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
            });

          }

          $scope.ok = function(category) {

            category['slugs'] = categoryslugs;
                // // console.log('category');
                $http({
                  url: API_URL + "/news/savecategory",
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  },
                  data: $.param(category)
                }).success(function(data, status, headers, config) {
                  alertclose();
                  $modalInstance.dismiss('cancel');
                  paginate(off, keyword);
                  successloadalert();
                }).error(function(data, status, headers, config) {
                  $scope.status = status;
                });


              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            };


            var deletecategoryInstanceCTRL = function($scope, $modalInstance, id, $state) {
              $scope.ok = function() {
                var news = {
                  'news': id
                };
                // // console.log(id);
                $http({
                  url: API_URL + "/news/categorydelete/" + id,
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  },
                  data: $.param(news)
                }).success(function(data, status, headers, config) {
                  alertclose();
                  $modalInstance.dismiss('cancel');
                  paginate(off, keyword);
                  deleteloadalert();
                }).error(function(data, status, headers, config) {
                  $scope.status = status;
                });
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            }

            $scope.deletecategory = function(id) {
              var modalInstance = $modal.open({
                templateUrl: 'deletecategory.html',
                controller: deletecategoryInstanceCTRL,
                resolve: {
                  id: function() {
                    return id
                  }
                }
              });
            }

            $scope.editcategory = function(id,categoryname,categoryslugs) {
              $scope.editcategoryshow = true;
              $scope.category = {id: id,catnames: categoryname, catslugs: categoryslugs};
            }

            $scope.alerts = [];
            $scope.closeAlert = function(index) {
              $scope.alerts.splice(index, 1);
            };

            $scope.updatecategory = function(category,$scope) {
              var slugs = category.catnames;

              if(slugs != null){
                var text1 = slugs.replace(/[^\w ]+/g,'');
                var catslugs = angular.lowercase(text1.replace(/ +/g,'-'));
                category['catslugs'] = catslugs;
              }
              console.log(category);


              $http({
                url: API_URL + "/news/updatecategorynames",
                method: "POST",
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(category)
              }).success(function(data, status, headers, config) {
                alertclose();
                if(data.hasOwnProperty('error')){
                  errorexistalert();
                }else{
                  updateloadalert();
                }
                paginate(off, keyword);
              }).error(function(data, status, headers, config) {


              });
            };

            $scope.cancelupdatecategory = function($scope) {
             window.location.reload();

           };
         })