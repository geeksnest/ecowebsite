'use strict';

/* Controllers */

app.controller('CreateNewsCtrl', function($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload, $filter) {
  $scope.isSaving = false;

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.news = {};
  $scope.tag = [];
  $scope.master = {};
  var orig = angular.copy($scope.master);

  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/newsimages/';
    $scope.news.imagethumb = Text.substring(texttocut.length);
  }

  $scope.thumbnails = "image";

  $scope.imagethumbnails = function(){
    $scope.thumbnails = "image";
    $scope.news.videothumb = [];
    $scope.vidpath = [];
  }

  $scope.videothumbnails = function(){
    $scope.thumbnails = "video";
    $scope.news.imagethumb = [];
  }

  $scope.paste = function(news){
    $scope.vidpath=$sce.trustAsHtml($scope.news.videothumb);
  }

  $scope.news = {
    title: ""
  };

  $scope.onnewstitle = function convertToSlug(Text, news)
  {
    if(Text != null)
    {
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }

    var newstitle = $scope.news.title;
    console.log($scope.news.title);
    $scope.alerts = [];

    $http({
      url: API_URL + "/news/checknewstitles/"+ newstitle,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (response) {

      if (response.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'danger', msg: 'The news title is already exist. Please enter a different title.'});
        $scope.formNews.title.$setValidity("buttonsubmit",false);
      } else{
        $scope.formNews.title.$setValidity("buttonsubmit",true);
      }

    }).error(function(response) {
      console.log(response);
    });

  }

  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;

  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.test = function (news) {
    if(news.feat == undefined || news.feat == false){
      news['feat'] = 0;
    }else{
      news['feat'] = 1;
    }
    if(news.featurednews == undefined || news.featurednews == '' ){
      news['featurednews'] = 0;
    }
    $scope.news = angular.copy(orig);
  }

  $scope.checkcat = function (news) {
    console.log(news.category);
    console.log(news.tag);
  }

  $scope.saveNews = function(news) {

    console.log(news);
    console.log("save news");
    console.log(news.tag);
    news['status'] = 1;
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.isSaving = true;

    if(news.feat == undefined || news.feat == false){
      news['feat'] = 0;
    }else{
      news['feat'] = 1;
    }
    if(news.featurednews == undefined || news.featurednews == '' ){
      news['featurednews'] = 0;
    }

    $http({
      url: API_URL + "/news/create",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(news)
    }).success(function(error,response, status, headers, config) {
      console.log("saved");
      $scope.isSaving = false;
      $scope.alerts.push({ type: 'success', msg: 'News successfully saved!' });
      $scope.formNews.$setPristine(true)
      $scope.news.tag = [];
      $scope.news.category = [];
      $scope.vidpath = [];
      $scope.news = angular.copy(orig);
      loadtags();
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function(error, response, status, headers, config) {
      $scope.alerts.push({
        type: 'danger',
        msg: 'Something went wrong please check your fields'
      });
      $scope.isSaving = false;
      console.log(response);
      console.log(error);
    });
  }

  $scope.savedraft = function(news) {

    console.log(news);
    news['status'] = 2;
    console.log("save draft");
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.isSaving = true;

    if(news.feat == undefined || news.feat == false){
      news['feat'] = 0;
    }else{
      news['feat'] = 1;
    }
    if(news.featurednews == undefined || news.featurednews == '' ){
      news['featurednews'] = 0;
    }

    $http({
      url: API_URL + "/news/create",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(news)
    }).success(function(error,response, status, headers, config) {
      console.log("saved");
      $scope.isSaving = false;
      $scope.alerts.push({ type: 'success', msg: 'News successfully draft.' });
      $scope.formNews.$setPristine(true)
      $scope.news.tag = [];
      $scope.news.category = [];
      $scope.vidpath = [];
      $scope.news = angular.copy(orig);
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function(error, response, status, headers, config) {
      $scope.alerts.push({
        type: 'danger',
        msg: 'Something went wrong please check your fields'
      });
      $scope.isSaving = false;
      console.log(response);
      console.log(error);
    });
  }

  $scope.preview = function(news){
    $scope.author.map(function(val){
      if(news.author == val.authorid){
        news['authorname'] = val.name;
        news['authorimage'] = val.image;
        news['authorabout'] = val.about;
      }
    });

    var returnYoutubeThumb = function(item){
      var x= '';
      var thumb = {};
      if(item){
        var newdata = item;
        var x;
        x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
        thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
        thumb['yid'] = x[1];
        return x[1];
      }else{
        return x;
      }
    }

    if(news['videothumb'] != ''){
      var origvidthumb = news['videothumb'];
      news['videothumb'] = returnYoutubeThumb(news['videothumb']);
    }

    console.log(news);
    news.date = $filter('date')(news.date,'yyyy-MM-dd');
    window.open(BASE_URL + "/news/preview?" + $.param(news));
    news['videothumb'] = origvidthumb;
  }

  var loadImages = function() {
    $http({
      url: API_URL + "/news/listnewsimages",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.data = data;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadImages();


  var loadcategory = function() {

    $http({
      url: API_URL + "/news/listcategory",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.category = data;
    }).error(function(data) {
      $scope.status = status;
    });
  }

  loadcategory();

  var loadtags = function() {

    $http({
      url: API_URL + "/news/listtags",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (data, status, headers, config) {

      console.log('tagtags');
      // data = data;
      // $scope.tag = data;

      var newlist = [];
      data.map(function(val){
        newlist.push(val.tags);
      });
      $scope.select2Options = {
        'multiple': true,
        'simple_tags': true,
        'tags': newlist
      };
      $scope.tag = newlist;

    }).error(function (data, status, headers, config) {
      data = data;
    });
  }
  loadtags();



  var loadauthor = function() {
    console.log('tags author');

    $http({
      url: API_URL + "/news/listauthor",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (data, status, headers, config) {

      console.log('author');
      data = data;
      $scope.author = data;
    }).error(function (data, status, headers, config) {
      data = data;
    });
  }
  loadauthor();


  $scope.addcategory = function() {
    var modalInstance = $modal.open({
      templateUrl: 'addcategory.html',
      controller: addcategoryCTRL,
      resolve: {
      }
    });
  }
  var addcategoryCTRL = function($scope, $modalInstance) {

    console.log("add category");
    $scope.disablebutton = false;

    var categoryslugs = '';


    $scope.oncategorytitle = function convertToSlug(Text)
    {


      if(Text != null)
      {
        var text1 = Text.replace(/[^\w ]+/g,'');
        categoryslugs = angular.lowercase(text1.replace(/ +/g,'-'));
      }
      var cattitle = Text;
      $scope.alerts = [];

      $http({
        url: API_URL + "/news/categorytitle/"+ cattitle,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (response) {


        if (response.hasOwnProperty('categoryexist')) {
          $scope.alerts.push({type: 'danger', msg: 'The category is already exist. Please enter another category.'});
          $scope.disablebutton = true;
        } else{
          $scope.disablebutton = false;
        }

      }).error(function (response) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
      });


    }

    $scope.ok = function(category) {

      category['slugs'] = categoryslugs;
      $http({
        url: API_URL + "/news/savecategory",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(category)
      }).success(function(data, status, headers, config) {
        console.log("add category=====");
        loadcategory();
        $modalInstance.close();
      }).error(function(data, status, headers, config) {
        console.log("add category ewwww");
        $modalInstance.close();
      });
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }

  };

  
  $scope.addtags = function() {
    var modalInstance = $modal.open({
      templateUrl: 'addtags.html',
      controller: addtagsCTRL,
      resolve: {
      }
    });
  }


  var addtagsCTRL = function($scope, $modalInstance) {
    $scope.disablebutton = false;

    var tagsslugs = '';
    console.log('addtags');

    $scope.ontags = function convertToSlug(Text)
    {

      if(Text != null)
      {
        var text1 = Text.replace(/[^\w ]+/g,'');
        tagsslugs = angular.lowercase(text1.replace(/ +/g,'-'));
      }

      var tagstitle = Text;
      $scope.alerts = [];

      $http({
        url: API_URL + "/news/tagstitle/"+ tagstitle,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (response) {



        if (response.hasOwnProperty('tagsexist')) {
          $scope.alerts.push({type: 'danger', msg: 'The tag is already exist. Please enter another tag.'});
          $scope.disablebutton = true;
        } else{
          $scope.disablebutton = false;
        }

      }).error(function (response) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
      });


    }



    $scope.ok = function(tags) {

      tags['slugs'] = tagsslugs;
      $http({
        url: API_URL + "/news/savetags",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(tags)
      }).success(function(data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        loadtags();

      }).error(function(data, status, headers, config) {
        $modalInstance.close();
      });

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };



  /////////////////////////////////////////==========================//////////////////////////////////////////


$scope.selectthumbnail = function(type) {
  console.log('====thumbnails====');
  var modalInstance = $modal.open({
    templateUrl: 'selectImageThumbnails.html',
    controller: selectthumbnailCTRL,
    windowClass: 'app-modal-window',
    resolve: {
      type: function(){
        return type;
      }
    }
  }).result.then(function(res) {
    console.log("============================");
    console.log(res);
    if(res){
      var x = res.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      console.log(x);
      if(x==null){
        $scope.news.videothumb = '';
        $scope.news.imagethumb = res;
      }else{
        $scope.news.imagethumb = '';
        $scope.news.videothumb = res;
      }
    }
  });
};

var selectthumbnailCTRL = function($modalInstance, $scope, Config, type) {

  $scope.isSaving = false;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.s3link = Config.amazonlink;
  $scope.type = type;
  console.log('======type======');
  console.log(type);
  console.log('======type======');

  var loadImages = function() {
    $http({
      url: API_URL + "/news/listnewsimages",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.imagelist = data;
      $scope.imagelength = data.length;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadImages();

  var loadVideos = function() {
    $http({
      url: API_URL + "/news/listnewsvideo",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      for (var x in data){
        var newd = returnYoutubeThumb(data[x].video);
        data[x].videourl = newd.url;
        data[x].youtubeid = newd.yid;
      }
      // console.log(data);
      $scope.videolist = data;
      $scope.videoslength = data.length;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadVideos();

  var returnYoutubeThumb = function(item){
    var x= '';
    var thumb = {};
    if(item){
      var newdata = item;
      var x;
      x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
      thumb['yid'] = x[1];
      return thumb;
    }else{
      return x;
    }
  }

  $scope.selectimg = function(img) {
    $modalInstance.close(img);
  }

  $scope.selectvid = function(video) {
    $modalInstance.close(video);
  }

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  }

  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.$watch('files', function () {
    $scope.upload($scope.files);
  });

  $scope.upload = function (files)
  {
    console.log("=====upload======");

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }
        }
        else{
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
          },
          fields : {
              key: 'uploads/newsimages/' + file.name, // the key to store the file on S3, could be file name or customized
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private', // sets the access to the uploaded file in the bucket: private or public
              policy: Config.policy, // base64-encoded json policy (see article below)
              signature: Config.signature, // base64-encoded signature based on policy string (see article below)
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
            },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : filename
            };
            $http({
              url: API_URL + "/news/ajaxfileuploader",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
  };


  $scope.savevideo = function(embedvideo) {
    var embed = { 'embedvideo': embedvideo }
    if(embedvideo){
      var x = embedvideo.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      if(x==null){
        $scope.invalidvideo = true;
        $scope.video='';
      }else{
        $http({
          url: API_URL + "/news/savevideo",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(embed)
        }).success(function (data, status, headers, config) {
          loadVideos();
          $scope.imageloader=false;
          $scope.imagecontent=true;
          $scope.embedvideo='';
          $scope.invalidvideo = false;

        }).error(function (data, status, headers, config) {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        });
      }
    }
  }

  $scope.deletenewsimg = function(imageid) {
    var modalInstance = $modal.open({
      templateUrl: 'deletenewsimgModal.html',
      controller: deletenewsimgCTRL,
      resolve: {
        imageid: function() {
          return imageid;
        }
      }
    });
  }

  var deletenewsimgCTRL = function($scope, $modalInstance, imageid) {
    $scope.id = imageid;
    $scope.ok = function(id) {
      $http({
        url: API_URL + "/news/dltnewsimg",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(id)
      }).success(function (data, status, headers, config) {

        loadImages();
        $modalInstance.dismiss('cancel');
      }).error(function (data, status, headers, config) {
        data = data;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.deletenewsvid = function(videoid) {
    var modalInstance = $modal.open({
      templateUrl: 'deletenewsimgModal.html',
      controller: deletenewsvidCTRL,
      resolve: {
        videoid: function() {
          return videoid;
        }
      }
    });
  }

  var deletenewsvidCTRL = function($scope, $modalInstance, videoid) {

    $scope.id = videoid;
    $scope.ok = function(id) {
    $http({
      url: API_URL + "/news/dltnewsvid",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(id)
    }).success(function (data, status, headers, config) {

      loadVideos();
      $modalInstance.dismiss('cancel');
    }).error(function (data, status, headers, config) {
      data = data;
    });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.copyimage = function(imglink) {

    var text = Config.amazonlink + '/uploads/newsimages/' + imglink;

    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    if(pp != "" && pp !== null) {
      $modalInstance.dismiss('cancel');
    }
  }

  $scope.copyvid = function(vidlink) {
    var text = vidlink;
    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    if(pp != "" && pp !== null) {
      $modalInstance.dismiss('cancel');
    }
  }


};//end
/////////////////////////////==============================/////////////////////////////////

})