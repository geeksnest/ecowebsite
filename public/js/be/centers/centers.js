'use strict';

/* Controllers */

app.controller('centersCtrl', function($scope, $http, $modal, $state, $sce, $q){

  $scope.data = {};
  var off = 1;
  var keyword = null;

  var sortto = 'ASC';
  $scope.sortBy = sortto;

  var paginate = function(off, keyword) {
    $http({
      url: API_URL+"/center/centerlist/" + off + '/' + keyword + '/' +  $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sortto) {
    $scope.sortBy = sortto;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword) {
    var off = pageNo;
    paginate(off, keyword);
  };

  $scope.refresh = function() {
    $scope.searchtext = [];
    $scope.searchtext = null;
    var off = 1;
    var keyword = 'null';
    paginate(off, keyword);
  }



  $scope.alerts = [];
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }
  var successloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Added!' });
  }
  var updateloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Updated!' });
  }

  var deleteloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Page successfully Deleted!' });
  }
  var errorloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
  }

  $scope.manageCenter = function(id,opera){
    var modalInstance = $modal.open({
      templateUrl: 'manageCenter.html',
      controller: manageCenterCTRL,
      resolve: {
        data: function () {
          return {id:id,opera:opera}
        }
      }
    });
  }
  var manageCenterCTRL = function ($scope, $modalInstance, data, $state) {
    $scope.id = data.id;
    $scope.opera = data.opera;    
    $scope.cancelBot = "Cancel";
    if(data.opera != 'Add'){
      $http({
        url: API_URL + "/center/getcenter/" + data.id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data) {
        $scope.cn = data[0];
        console.log(data);
      });
    }
    $scope.addcenter = function (cn) {
      $scope.alerts = [];
      $http({
        url: API_URL + "/center/addcenter",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(cn)
      }).success(function (response) {
        $scope.alerts.push({type: 'success', msg: 'Center successfully added.'});
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $scope.cancelBot = "Close";
        paginate(off, keyword);
        cn.centername = '';
      }).error(function (response) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      });
    };
    $scope.updateCenter = function (cn) {
      $scope.alerts = [];
      $http({
        url: API_URL + "/center/updatecenter",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(cn)
      }).success(function (response) {
        $scope.alerts.push({type: 'success', msg: 'Center successfully updated.'});
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $scope.cancelBot = "Close";
        paginate(off, keyword);
      }).error(function (response) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      });
    };
    $scope.deleteCenter = function (cn) {
      $scope.alerts = [];
      $http({
        url: API_URL + "/center/deletecenter/" + cn.id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data) {
        $scope.alerts.push({type: 'danger', msg: 'Center deleted.'});
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $scope.cancelBot = "Close";
        paginate(off, keyword);
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }










})