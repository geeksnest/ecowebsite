'use strict';

/* Controllers */

app.controller('ManagePagesCtrl', function($scope, $http, $modal, $state, $sce, $q){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;  
  
  var sort = 'pageid';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  var paginate = function(off, keyword) {
    $http({
      url: API_URL+"/pages/manage/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword) {
    var off = pageNo;
    paginate(off, keyword);
  };

  $scope.refresh = function() {
    var off = 1;
    $scope.searchtext = [];
    paginate(off, keyword);
  }



  $scope.alerts = [];
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }
  var successloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Added!' });
  }
  var updateloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Updated!' });
  }

  var deleteloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Page successfully Deleted!' });
  }
  var errorloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
  }

  var updatepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
    $scope.ok = function (pageid) {
      $scope.pageid = pageid;
      $state.go('editpage', {pageid: pageid });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.updatepage = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'updatepage.html',
      controller: updatepageInstanceCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }

  var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
    $scope.ok = function (pageid) {
      var page = {'page': pageid};
      $http({
        url: API_URL + "/page/pagedelete/" + pageid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(page)
      }).success(function (data, status, headers, config) {
        alertclose();
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
        deleteloadalert();
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.deletepage = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'deletepage.html',
      controller: deletepageInstanceCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }



  $scope.setstatus = function (stat,pageid,keyword,off) {
    console.log(stat);

    $scope.currentstatusshow = pageid;
    $http({
      url: API_URL + "/page/updatepagestatus/" + stat + '/' + pageid,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var i = 2;
      setInterval(function(){
        i--;
        if(i == 0)
        {
          $scope.currentstatusshow = 0;
          paginate(off, keyword);
        }
      },100)
    }).error(function (data, status, headers, config) {

    });
  }


})