'use strict';

/* Controllers */

app.controller('editPageCtrl', function ($scope, $http, $modal, $stateParams, $sce, $q, $timeout, Config, Upload) {

  $scope.imageloader=false;
  $scope.imagecontent=true;

  $scope.tfsize = [];
  $scope.dfsize = [];
  for (var x = 30; x <= 45; x++) {
    $scope.tfsize.push({'val': x})
  }

  for (var y = 12; y <= 30; y++) {
    $scope.dfsize.push({'val': y})
  } 


  $scope.onpagetitle = function convertToSlug(Text){
    if(Text != null) {
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
  }

  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/pageimages/';
    $scope.page.banner = Text.substring(texttocut.length);
  }

  //Execute Here
  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  }

var cked ;

var updateme = function(page){
if ($scope.page.status == true || $scope.page.status == 1)
    {
      $scope.page.status = true;
    }
    else
    {
      $scope.page.status = false;
    }
    // console.log(page['body']);
    $http({
      url: API_URL + "/pages/updatepage",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(page)
    }).success(function (response, headers, status) {

      if (response.hasOwnProperty('pagealreadyexist')) {
        $scope.alerts.push({type: 'danger', msg: 'The page title is already exist. Please enter another page title.'});

      } else{
        $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
      }
      // $('body').scrollTop(0);
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function (response, headers, status) {
      $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});

    });
}
  $scope.updatepage = function (page){
    closeAlert();
    cked = CKEDITOR.instances['myeditor'].getData();
    page['body'] = cked;
     if (page['banneropt']==true)
    {
      if (page['banner']==undefined || page['banner']==null || page['banner'] == "") {
        $scope.n = true;
        $('#banner').focus();
      }
      else{
        $scope.n = false
        updateme(page);
      }

    }
    else{
      updateme(page);
    }

};


  $scope.reset = function(){
    loadpageinfo();
  }

  var loadpageinfo = function (pageid) {
    $http({
      url: API_URL + "/pages/editpage/" + $stateParams.pageid ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.page = data;
      // console.log(data.banner)
      if (data.banneropt=='true') {
        $scope.page.banneropt = true;
      }else{
        $scope.page.banneropt = false;
      }
      if (data.btncolor) {
        $scope.page.btncolor = data.btncolor;
      }else{
        $scope.page.btncolor ='#ff9d0f';
      }
    }).error(function (data, status, headers, config) {

    });
  }
  loadpageinfo();

  var loadImages = function(){
    $http({
      url: API_URL + "/pages/listpageimages",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
      $scope.data = data;
    }).error(function (data) {
      $scope.status = status;
    });
  }
  loadImages();


  $scope.$watch('files', function () {
    $scope.upload($scope.files);

  });



  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.upload = function (files)
  {
    $scope.alertss = [];

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }
        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/pageimages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : filename
            };
            $http({
              url: API_URL + "/pages/ajaxfileuploader",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });

        }
      }
    }
  };

  $scope.deletepageimg = function(pageid) {

    var modalInstance = $modal.open({
      templateUrl: 'deletepageimgModal.html',
      controller: deletepageimgCTRL,
      resolve: {
        pageid: function() {
          return pageid;
        }
      }
    });
  }

  var deletepageimgCTRL = function($scope, $modalInstance, pageid) {
    $scope.pageid = pageid;
    $scope.ok = function(pageid) {
      $http({
        url: API_URL + "/pages/dltpagephoto",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(pageid)
      }).success(function (data, status, headers, config) {

        loadImages();
        $modalInstance.dismiss('cancel');
      }).error(function (data, status, headers, config) {
        data = data;
      });


    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.banners = function convertToSlug(Text)
  {
    $scope.page.banners = Text;
  }
})
