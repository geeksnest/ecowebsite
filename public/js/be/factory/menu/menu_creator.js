app.factory('Createmenu', function($http, $q){
  	return {

  		data: {},        
        add: function(menudata,callback){
            $http({
                url: API_URL + "/menu/addmenu",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(menudata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },        
        savemenu: function(menu,menuID,name,callback){
            $http({
                url: API_URL + "/menu/savemenu/" + menu + "/" + menuID + "/" +name,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(menu)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },        
        addsub: function(subdata,callback){
            $http({
                url: API_URL + "/menu/subaddmenu",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(subdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },      
        updatesubmenu: function(link,callback){
            $http({
                url: API_URL + "/menu/updatesubmenu",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(link)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },
        addsubmenu: function(link,callback){
            $http({
                url: API_URL + "/menu/addsubmenu",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(link)
            }).success(function (data, status, headers, config) {
                callback(data);

            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });

        },
        menulist: function(num, off, keyword, callback){
            $http({
                url: API_URL + "/menu/menulist/"+num+"/"+off+"/"+keyword,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
         
        submenulist: function(menuID,callback){
            $http({
                url: API_URL + "/menu/submenulist/" + menuID,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        submenuname: function(submenuID,callback){
            $http({
                url: API_URL + "/menu/submenuname/" + submenuID,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        delete: function(menuID, callback){
            $http({
                url: API_URL + "/menu/delete/" + menuID,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        deletesubmenu: function(submenuID, callback){
            $http({
                url: API_URL + "/menu/subdelete/" + submenuID,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        pagelist: function(callback){
            $http({
                url: API_URL + "/menu/pagelist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        categorylist: function(callback){
            $http({
                url: API_URL + "/menu/categorylist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        taglist: function(callback){
            $http({
                url: API_URL + "/menu/taglist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        postlist: function(callback){
            $http({
                url: API_URL + "/menu/postlist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
    }
   
})