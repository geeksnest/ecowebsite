'use strict';

/* Controllers */

app.controller('createeventCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload, $filter) {

  $scope.isSaving = false;

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.events = {};
  $scope.master = {};

  $scope.addPrice = false;
  $scope.prices = [];
  $scope.price = {};
  $scope.showUpdate = false;
  $scope.currentEditPrice = 99999;

  var orig = angular.copy($scope.master);


  $scope.chkprices = function(prices) {
    console.log("================");
    console.log(prices);
    console.log("================");    
  }

  $scope.addprice = function(price) {
    $scope.prices.push(price);
    console.log($scope.prices);
    $scope.formevents.amounts.$setPristine();
    $scope.price = {};
  }

  $scope.updatePrice = function(price) {
    console.log($scope.prices);
    $scope.prices[$scope.currentEditPrice].amounts = price.amounts;

    $scope.formevents.amounts.$setPristine();
    $scope.price = {};
    $scope.currentEditPrice = 99999;
    $scope.showUpdate = false;
  }

  $scope.editPrice = function(index) {
    console.log(index);
    $scope.currentEditPrice = index;
    $scope.showUpdate = true;
  }

  $scope.delete = function(index) {
    $scope.prices.splice(index, 1);
  }

  $scope.priceCancel = function() {
    console.log('Click Cancel');
    if (!$scope.showUpdate) {
      $scope.addPrice = false;
    }
    $scope.currentEditPrice = 99999;
    $scope.price = {};
    $scope.showUpdate = false;
  }



  $scope.addCname = false;
  $scope.cnames = [];
  $scope.cname = {};
  $scope.cnameUpdate = false;
  $scope.currentEditCname = 99999;

  $scope.addCname = function(cname) {
    $scope.cnames.push(cname);
    console.log($scope.cnames);
    $scope.formevents.centernames.$setPristine();
    $scope.cname = {};
  }

  $scope.updateCname = function(cname) {
    console.log($scope.cnames);
    $scope.cnames[$scope.currentEditCname].centernames = cname.centernames;

    $scope.formevents.centernames.$setPristine();
    $scope.cname = {};
    $scope.currentEditCname = 99999;
    $scope.cnameUpdate = false;
  }

  $scope.editCname = function(index) {
    console.log(index);
    $scope.currentEditCname = index;
    $scope.cnameUpdate = true;
  }

  $scope.deleteCname = function(index) {
    $scope.cnames.splice(index, 1);
  }

  $scope.cancelCname = function() {
    console.log('Click Cancel');
    if (!$scope.cnameUpdate) {
      $scope.addPrice = false;
    }
    $scope.currentEditCname = 99999;
    $scope.cname = {};
    $scope.cnameUpdate = false;
  }


  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/newsimages/';
    $scope.events.banner = Text.substring(texttocut.length);
  }

  $scope.thumbnails = "image";

  $scope.imagethumbnails = function(){
    $scope.thumbnails = "image";
  }

  $scope.events = {
    title: ""
  };

  $scope.onnewstitle = function convertToSlug(Text)
  {
    // if(Text != null)
    // {
    //   var text1 = Text.replace(/[^\w ]+/g,'');
    //   $scope.events.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    // }

    // var eventtitle = $scope.events.title;
    // console.log($scope.events.title);
    // $scope.alerts = [];

    // $http({
    //   url: API_URL + "/news/checknewstitles/"+ newstitle,
    //   method: "GET",
    //   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    // }).success(function (response) {

    //   if (response.hasOwnProperty('error')) {
    //     $scope.alerts.push({type: 'danger', msg: 'The news title is already exist. Please enter a different title.'});
    //     $scope.formNews.title.$setValidity("buttonsubmit",false);
    //   } else{
    //     $scope.formNews.title.$setValidity("buttonsubmit",true);
    //   }

    // }).error(function(response) {
    //   console.log(response);
    // });

  }

 

  $scope.checkcat = function (news) {
    console.log(news.category);
    console.log(news.tag);
  }

  $scope.publishEvent = function(events,prices,cnames) {

    $scope.alerts = [];
    // $scope.isSaving = true;
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    console.log(prices);
    console.log(cnames);
    console.log("publishEvent");
    events['amounts'] = prices;
    events['centernames'] = cnames;

    if(events['otheramount'] == true){
      events['otheramount'] = 'yes';
    }else{
      events['otheramount'] = 'no';
    }
    
    console.log(events);    

    $http({
      url: API_URL + "/events/publishEvent",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(events)
    }).success(function(error,response, status, headers, config) {
      console.log("saved");
      $scope.isSaving = false;
      $scope.alerts.push({ type: 'success', msg: 'Events successfully published.' });
      $scope.formevents.$setPristine(true)
      $scope.cnames.length = 0;  
      $scope.prices.length = 0;  
      $scope.events = angular.copy(orig);
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function(error, response, status, headers, config) {
      $scope.alerts.push({
        type: 'danger',
        msg: 'Something went wrong please check your fields'
      });
      $scope.isSaving = false;
      console.log(response);
      console.log(error);
    });
  }

 



$scope.infoimages = function(type) {
  var modalInstance = $modal.open({
    templateUrl: 'helperModal.html',
    controller: helperCTRL,
    windowClass: 'helper-modal-window',
    resolve: {
      type: function(){
        return type;
      }
    }
  })
};
var helperCTRL = function($modalInstance, $scope, Config, type) {
  console.log(type);
  
  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  }
}


$scope.eventsBanner = function(type) {
  console.log('====thumbnails====');
  var modalInstance = $modal.open({
    templateUrl: 'eventsBanner.html',
    controller: eventsBannerCTRL,
    windowClass: 'app-modal-window',
    resolve: {
      type: function(){
        return type;
      }
    }
  }).result.then(function(res) {
    console.log("============================");
    console.log(res);
    if(res){      
        $scope.events.banner = res;
    }
  });
};

var eventsBannerCTRL = function($modalInstance, $scope, Config, type) {
  console.log('banner events');

  $scope.isSaving = false;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.s3link = Config.amazonlink;
  $scope.type = type;
  console.log('======type======');
  console.log(type);
  console.log('======type======');

  var loadImages = function() {
    $http({
      url: API_URL + "/events/listeventsbanner",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.imagelist = data;
      $scope.imagelength = data.length;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadImages();
  

  $scope.selectimg = function(img) {
    $modalInstance.close(img);
  }

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  }

  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.$watch('files', function () {
    $scope.upload($scope.files);
  });

  $scope.upload = function (files)
  {
    console.log("=====upload======");

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
            },
            fields : {
                key: 'uploads/eventsbanner/' + file.name, // the key to store the file on S3, could be file name or customized
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                policy: Config.policy, // base64-encoded json policy (see article below)
                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
              },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : filename
            };
            $http({
              url: API_URL + "/events/addeventsbanner",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
  };

  $scope.deletenewsimg = function(imageid) {
    var modalInstance = $modal.open({
      templateUrl: 'deletenewsimgModal.html',
      controller: deletenewsimgCTRL,
      resolve: {
        imageid: function() {
          return imageid;
        }
      }
    });
  }

  var deletenewsimgCTRL = function($scope, $modalInstance, imageid) {
    $scope.id = imageid;
    $scope.ok = function(id) {
      $http({
        url: API_URL + "/news/dltnewsimg",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(id)
      }).success(function (data, status, headers, config) {

        loadImages();
        $modalInstance.dismiss('cancel');
      }).error(function (data, status, headers, config) {
        data = data;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }; 


};
/////////////////////////////===============end===============/////////////////////////////////

})

app.directive('onlyDigits', function () {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
      if (!ngModel) return;
      ngModel.$parsers.unshift(function (inputValue) {
        var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
        ngModel.$viewValue = digits;
        ngModel.$render();
        return digits;
      });
    }
  };
});