'use strict';

/* Controllers */

app.controller('eventlistCtrl', function($scope, $http, $modal, $state, $sce, $q){

  $scope.data = {};
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';
  
  var sort = 'num';
  var sortto = 'ASC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  var paginate = function(off, keyword) {
    $http({
      url: API_URL+"/events/eventlist/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword) {
    var off = pageNo;
    paginate(off, keyword);
  };

  $scope.refresh = function() {
    var off = 1;
    $scope.searchtext = [];
    paginate(off, keyword);
  }



  $scope.alerts = [];
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }
  var successloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Added!' });
  }
  var updateloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Updated!' });
  }

  var deleteloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Page successfully Deleted!' });
  }
  var errorloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
  }

  $scope.fullview = function(eventID){
    $scope.eventID
    var modalInstance = $modal.open({
      templateUrl: 'viewModal.html',
      controller: viewInstanceCTRL,
      resolve: {
        eventID: function () {
          return eventID
        }
      }
    });
  }
  var viewInstanceCTRL = function ($scope, $modalInstance, eventID, $state) {
    $scope.eventID = eventID;
    $scope.ok = function (eventID) {
      $scope.eventID = eventID;
      $state.go('eventcontents', {eventID: eventID });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.viewcollections = function(eventID){
    $scope.eventID
    var modalInstance = $modal.open({
      templateUrl: 'viewcollectionsModal.html',
      controller: viewcollectionsInstanceCTRL,
      resolve: {
        eventID: function () {
          return eventID
        }
      }
    });
  }
  var viewcollectionsInstanceCTRL = function ($scope, $modalInstance, eventID, $state) {
    $scope.eventID = eventID;
    $scope.ok = function (eventID) {
      $scope.eventID = eventID;
      $state.go('eventscollection', {eventID: eventID });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }


  $scope.deleteEvent = function(eventID){
    $scope.eventID
    var modalInstance = $modal.open({
      templateUrl: 'deleteEventModal.html',
      controller: deleteEventInstanceCTRL,
      resolve: {
        eventID: function () {
          return eventID
        }
      }
    });
  }
  var deleteEventInstanceCTRL = function ($scope, $modalInstance, eventID, $state) {
    $scope.eventID = eventID;
    $scope.ok = function (eventID) {
      $http({
        url: API_URL + "/events/deleteEvent/" + eventID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        alertclose();
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
        deleteloadalert();
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }



  $scope.setstatus = function (stat,eventID,keyword,off) {
    console.log(stat);

    $scope.currentstatusshow = eventID;
    $http({
      url: API_URL + "/events/updateEventStatus/" + stat + '/' + eventID,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var i = 2;
      setInterval(function(){
        i--;
        if(i == 0)
        {
          $scope.currentstatusshow = 0;
          paginate(off, keyword);
        }
      },100)
    }).error(function (data, status, headers, config) {

    });
  }


})