'use strict';

/* Controllers */
app.controller("taglistCtrl", function ($scope, $http, $anchorScroll,$modal, Countries,store, MDYform, Config, Upload, $filter, $timeout, $sce, $q, $window) {

	console.log('===index===');

  $scope.amazonlink = Config.amazonlink; 

  var url = window.location.href;
  var auth = url.match(/\/medialibrary\/tag\/(.*)+/);
  console.log(auth[1]);

  var offset = 0;
  var num = 15;
  $scope.loading = false;
  $scope.loadingGIF = false;

  var loadMorevideo = function(){    
    $http({
      url: API_URL+"/medialibrary/tagslist/"+offset+"/"+num+"/"+auth[1],
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.data = data.data;
      $scope.tagsname = data.tagsname[0].tags;
      $scope.loadingGIF = false;
      if(data.count == data.data.length){
        $scope.loading = true;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMorevideo();

  $scope.toview = function(slugs){
    console.log('click');
    $window.location = '/medialibrary/' + slugs;
  }
  $scope.loadmorebot = function(){
    num = num + num;
    $scope.loadingGIF = true;
    loadMorevideo();
  }

});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});
