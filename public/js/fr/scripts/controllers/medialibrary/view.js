'use strict';

/* Controllers */
app.controller("viewvideoCtrl", function ($scope, $http, $anchorScroll, $location, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {


  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    // console.log(token);
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
    $scope.username = usernameAgent;
  }else{    
     $scope.userAgent = "NOUSER";  
  }  

  $scope.amazonlink = Config.amazonlink;
 
  $scope.loadingg = false; 

  //get the slugs
  var url = window.location.href;
  var auth = url.match(/\/medialibrary\/(.*)+/);
  var slugs = auth[1]; 
  $scope.convertToDate = function (stringDate){
    var dateOut = new Date(stringDate);
    dateOut.setDate(dateOut.getDate() + 1);
    return dateOut;
  };

  $scope.like = function(vidid,usrid){
    if(usrid == 'NOUSER'){
      $window.location = '/login?videolike='+slugs;  
    }else{
      $http({
        url: API_URL+"/medialibrary/likedislike/" + vidid + "/" + usrid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        loadvideo();
        $scope.usrlike = data;
      });
    }
  }

  //get data
  var loadvideo = function(){
    $http({
      url: API_URL+"/medialibrary/viewvideo/" + slugs,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      if(data != "NODATA"){
        $scope.media = data[0]; 
        $scope.video = data[0].video; 
        $scope.videoid = data[0].id;
        $scope.views = data[0].viewscount;
        $http({
          url: API_URL+"/medialibrary/videolikes/" + data[0].id + "/" + $scope.userAgent,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
          $scope.countlikes = data.getlikes;
          $scope.usrlike = data.usrlike;
        });
        if(data[0].videotype == 'upload'){
          var playerInstance = jwplayer("myElement");
          playerInstance.setup({
            file: $scope.amazonlink+"/videos/"+data[0].video,
            image: $scope.amazonlink+"/videos/thumbs/"+data[0].videothumb,
            title: data[0].title,
            description: data[0].title,
            mediaid: data[0].id
          });
        }else{
          $scope.preview = $sce.trustAsHtml(data[0].embed);
        }
      }else{
        $window.location = '/page404';
      }
    }).error(function (data, status, headers, config) {
    });
  }
  if(slugs == 'null'){
    $window.location = '/page404';
  }else{
    loadvideo();
  }


  $timeout(function(){
    $http({
      url: API_URL+"/medialibrary/addvideoviews/" + $scope.videoid,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.views = data;
    });
  },30000);
  

});