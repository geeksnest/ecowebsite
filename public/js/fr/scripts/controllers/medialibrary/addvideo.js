'use strict';

/* Controllers */
app.controller("addvideoCtrl", function($scope, $http, $anchorScroll, $location, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {

  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
  }else{    
    $window.location = '/login?medialibrary=donatecontent';  
  }
  $scope.master = {};
  var orig = angular.copy($scope.master);
  $scope.media = {};
  $scope.tag = [];
  $scope.media.type = 'upload';
  $scope.showpreview = false;
  $scope.youtube = function(url){
    $scope.resultget = [];
    var x = url.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
    if(x==null){
      $scope.resultget.push({type: 'danger', msg: 'Embed link is invalid'});      
      $scope.showpreview = false;
      $scope.showForm = false;
      $scope.preview = null;
    }else{
      $scope.resultget = [];
      $scope.preview = $sce.trustAsHtml(url);
      $scope.showpreview = true;
      $scope.showForm = true;
    }    
  }
  $scope.errorget = '';
  $scope.loadingMsg = 'Saving video Please wait...';
  $scope.closeAlert1 = function(index) {
    $scope.resultget.splice(index, 1);
  };
  $scope.cNTS = [];  
  $scope.resultget = [];
  $('form').validate({
    rules: {
      name: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true
      },
      description: {
        required: true,
      },
      title: {
        required: true
      },
      agree: {
        required: true
      },
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $scope.chkbox= function(_gdata){
    if(_gdata){
      $scope.chkerror = true;
    }else{
       $scope.chkerror = false;
    }
  }

  $scope.save = function (media,file,thumbImg){
    // console.log(media.tag);
    // $scope.errorget = '';
    // $scope.successget = ''; 
    // $scope.resultget = [];
    // $scope.cNTS = [];
    // console.log(file)
    // console.log(thumbImg)
  if(
    media.name==""             || media.name==undefined
    || media.title==""         || media.title==undefined
    || media.description==""   || media.description==undefined
    || media.phone==""         || media.phone==undefined 
    || media.email==""         || media.email==undefined
    || media.agree==false      || media.agree==undefined
    || media.tag==undefined    || media.tag==""
    || media.category==""      || media.category==undefined
    )
  {
    $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill out all required fields before submitting'}];
    $scope.chkerror = true;
    $scope.n = true;
    try {
        if($scope.media.description){
         $scope.n = false;
       }else{
        $scope.n = true;
        $('#desc').focus();
        $('#desc').blur();
      }
    }
    catch (e) {
      $scope.n = true;
    }
    try {
        if($scope.media.tag == "" || !$scope.media.tag || $scope.media.tag==undefined){
        $scope.t = true;
        $('#tags').focus();
        $('#tags').blur();
       }else{
         $scope.t = false;
         console.log("mali")
      }
    }
    catch (e) {
      $scope.n = true;
    }
    try {
        if($scope.media.category == "" || !$scope.media.category || $scope.media.category==undefined){
         $scope.cat = true;
         $('#cats').focus();
         $('#cats').blur();

       }else{
        $scope.cat = false;
      }
    }
    catch (e) {
      $scope.n = true;
    }
  }
  else if(!file || file==undefined){
    $scope.alerts =[{type: 'danger', msg: 'Please choose video to upload.'}];
  }
  else if(!thumbImg || thumbImg==undefined){
    $scope.alerts =[{type: 'danger', msg: 'Please choose image to upload.'}];
  }
  else{
      
          $scope.errorget = '';
          $scope.alerts = [];
          $scope.loadingg = true;
          $timeout(function(){
            $scope.cate=true;
          }, 1000);
          $scope.loadingMsg = 'uploading video...';  
          var fileExtension = '.' + file.name.split('.').pop();
          var fileExtensionThumb = '.' + thumbImg.name.split('.').pop();
          // rename the file with a sufficiently random value and add the file extension back
          var randomNane =  Math.random().toString(36).substring(7) + new Date().getTime();
          var renamedFile =  randomNane + fileExtension;
          var renamedFileThumb = randomNane + fileExtensionThumb;
          var promises;
          promises = Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'videos/' + renamedFile,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          }).progress(function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            var log = progressPercentage;
            $scope.loadingMsg = "uploading video ("+log+"%)"; 
          })
          promises.then(function(data){
            console.log('uploading video thumbnail..');

            var promisesThumb;
            promisesThumb = Upload.upload({

              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'videos/thumbs/' + renamedFileThumb,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": thumbImg.type != '' ? thumbImg.type : 'application/octet-stream'
              },
              file: thumbImg
            })
            promisesThumb.then(function(data){
              console.log('video uploaded..');
              console.log('saving data..');
              $scope.loadingMsg = 'saving project please wait...';
              media['video'] = renamedFile;
              media['videothumb'] = renamedFileThumb;
              save();
            });
          });
var save = function(){
  var slugs = media.title;
  var text = slugs.replace(/[^\w ]+/g,'');
  var slugs = angular.lowercase(text.replace(/ +/g,'-'));
  media['slugs'] = slugs;
  $http({
    url: API_URL + "/medialibrary/add",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(media)
  }).success(function (data, status, headers, config) {
    $scope.loadingg = false;
    $scope.cate=false;
    $scope.isSaving = false;
    $location.hash('successget');
    $anchorScroll();
    $scope.cNTS = [];
    $scope.closeAlert1(); 
    $scope.media.tag = [];
    $scope.media.category = [];
    if(file){
      $scope.thumbImg = [];     
      $scope.videoToUpload = [];
    }
    $scope.preview = '';
    $scope.hideBig = false;
    $scope.showForm = false;
    console.log('data saved..');
    $location.hash('');
    $scope.media = angular.copy(orig);
    loadtags();
    $scope.media.type = 'upload';
    $scope.thumbImg = [];
    $scope.videoToUpload = []; 
    $scope.imageselected = false;
    $scope.form.$setPristine();
    $scope.resultget = [{type: 'success', msg: 'Thank you. Your information has been received. We will contact you shortly.'}];
      }).error(function (data, status, headers, config) {
        $scope.status = status;
        console.log(data);
      });
    }
        
  }
    

   
}


  $scope.videoAlerts = [];
  $scope.closeAlert = function(index) {
    $scope.videoAlerts.splice(index, 1);
  };
  $scope.hideBig = false;
  $scope.showForm = false;
  $scope.prepare = function(file) {    
    if (file && file.length) {
      // if (file[0].size > 20000000) {
      //   $scope.videoAlerts = [{
      //     type: 'danger',
      //     msg: 'Please resize the video below 20 MB and try again.'
      //   }];       
      //   $scope.videoToUpload = [];
      //   $scope.showForm = false;
      //   $scope.hideBig = false;
      // }else{
        if (file[0].type != "video/mp4") {
          $scope.videoAlerts = [{
            type: 'danger',
            msg: 'Please format your video to mp4 format and try again.'
          }];       
          $scope.videoToUpload = [];
          $scope.showForm = false;
          $scope.hideBig = false;
        } else {
          $scope.videoToUpload = file[0];
          $scope.closeAlert();
          $scope.hideBig = true;
          $scope.showForm = true;
        }
      }      
    // }
  }
  $scope.imageselected = false;
  $scope.prepareThumb = function(file) {
    console.log(file);
    if (file && file.length) {
      console.log(file[0].type);
      if(file[0].type == "image/jpeg" || file[0].type == "image/png" || file[0].type == "image/gif"){
        if (file[0].size > 2000000) {
          console.log('File is too big!');
          $scope.videoAlerts = [{
            type: 'danger',
            msg: 'The image "' + file[0].name + '" is too big. Please resize the image below 2MB.'
          }];       
          $scope.thumbImg = [];
        } else {
          console.log("below maximum");
          $scope.thumbImg = file[0];
          $scope.closeAlert();
          $scope.imageselected = true;
        }
      }else{
        $scope.videoAlerts = [{
          type: 'danger',
          msg: 'Invalid image format. Please select jpg, png and gif format only.'
        }];       
        $scope.thumbImg = [];
      }
    }
  }

  var loadcategory = function() {
    $http({
      url: API_URL + "/medialibrary/listcategoryall",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.category = data;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadcategory();

  var loadtags = function() {
    $http({
      url: API_URL + "/medialibrary/listtagsall",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (data, status, headers, config) {
      var taglist = [];
      data.map(function(val){
        taglist.push(val.tags);
      });
      $scope.select2Options = {
        'multiple': true,
        'simple_tags': true,
        'tags': taglist
      };
      $scope.tag = taglist;
    }).error(function (data, status, headers, config) {
      data = data;
    });
  }
  loadtags();

  // Editor options.
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false,
    toolbarGroups: [
        { name: 'editing',     groups: [ 'selection', 'spellchecker' ] },
        { name: 'colors' },
        { name: 'insert', groups: [ 'Image', 'Youtube', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'links' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'tools',    groups: [ 'mode' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] },
        { name: 'document',    groups: [ 'doctools' ] },
        { name: 'styles' }
        ]
  };

  // Called when the editor is completely ready.
  $scope.onReady = function () {
    // ...
  };

}).directive('phone', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

      ngModelCtrl.$parsers.push(function(viewValue) {
              // console.log(viewValue)
              if (viewValue!=undefined) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
              }


            });

      ngModelCtrl.$render = function() {
        $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
      };

      $element.bind('change', listener);
      $element.bind('keydown', function(event) {
        var key = event.keyCode;
        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
          return;
        }
        $browser.defer(listener);
      });

      $element.bind('paste cut', function() {
        $browser.defer(listener);
      });
    }

  };
}).filter('tel', function () {
  return function (tel) {
          // console.log(tel);

          if (!tel){ 
            return ''; 
          }else{
           var value = tel.toString().trim().replace(/^\+/, '');

           if (value.match(/[^0-9]/)) {
            return tel;
          }

          var country, city, number;

          switch (value.length) {
            case 1:
            case 2:
            case 3:
            city = value;
            break;

            default:
            city = value.slice(0, 3);
            number = value.slice(3);
          }

          if(number){
            if(number.length>3){
              number = number.slice(0, 4) + ' ' + number.slice(4,7);
            }
            else{
              number = number;
            }

            return ("(" + city + ") " + number).trim();
          }
          else{
            return "(" + city;
          }
        }



      };
    }); 