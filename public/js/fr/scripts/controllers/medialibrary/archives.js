'use strict';

/* Controllers */
app.controller("archiveslistCtrl", function ($scope, $http, $anchorScroll,$modal, Countries,store, MDYform, Config, Upload, $filter, $timeout, $sce, $q, $window) {

	console.log('===index===');

  $scope.amazonlink = Config.amazonlink; 

  var url = window.location.href;
  var auth = url.match(/\/medialibrary\/archives\/(.*)+/);

  var offset = 0;
  var num = 15;
  $scope.loading = false;
  $scope.loadingGIF = false;
  var arch = auth[1].split("-");
  $scope.archives = arch[0] + ' ' + arch[1];

  var loadMorevideo = function(){    
    $http({
      url: API_URL+"/medialibrary/archivelist/"+offset+"/"+num+"/"+auth[1],
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.data = data.data;
      $scope.loadingGIF = false;
      if(data.count == data.data.length){
        $scope.loading = true;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMorevideo();

  $scope.toview = function(slugs){
    console.log('click');
    $window.location = '/medialibrary/' + slugs;
  }
  $scope.loadmorebot = function(){
    num = num + num;
    $scope.loadingGIF = true;
    loadMorevideo();
  }

});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});
