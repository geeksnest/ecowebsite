'use strict';

/* Controllers */
app.controller("viewProjListCtrl", function($scope, $http, $anchorScroll, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q) {

	console.log('===index===');

	$scope.amazonlink = Config.amazonlink;
  
  //Percentage Formula
  // percentage = (value/totalvalue)* 100
  

  $scope.loadingGIFn = true;
  var loadNewprojects = function(){

    $http({
      url: API_URL+"/project/felist",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.loadingGIFn = false;
      $scope.newProj = data.data;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadNewprojects();



  var offset = 0;
  var num = 4;
  $scope.loading = false;
  $scope.loadingGIF = true;
  $scope.loadingf = false;
  $scope.loadingGIFf = true;

  var loadMoreProjects = function(){    
    $http({
      url: API_URL+"/project/felistmore/"+offset+"/"+num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.oyaaa = data.data;
      $scope.allCount = data.count; 
      $scope.loadingGIF = false;
      if(data.count == data.data.length){
        $scope.loading = true;
      }else{
        $scope.loading = false;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMoreProjects();


  var offsetz = 0;
  var numz = 4;
  var loadMoreProjectsf = function(){    
    $http({
      url: API_URL+"/project/felistmoref/"+offsetz+"/"+numz,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data.length);
      $scope.finished = data.data;
      $scope.allCountf = data.count; 
      $scope.loadingGIFf = false;
      if(data.count == data.data.length){
        $scope.loadingf = true;
      }else{
        $scope.loadingf = false;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMoreProjectsf();

  $scope.loadmorebot = function(){
    num = num + num;
    $scope.loadingGIF = true;
    $scope.loading = true;
    loadMoreProjects();
  }
  $scope.loadmorebotf = function(){
    numz = numz + numz;
    $scope.loadingGIFf = true;
    $scope.loadingf = true;
    loadMoreProjectsf();
  }


  $scope.predicate = 'projTotalPercentage';
  $scope.reverse = true;
  
  $scope.random = function(){
    return 0.5 - Math.random();
  };


  var checkExpired = function () {
    $http({
      url: API_URL+"/projects/checkExpired",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      loadMoreProjects();
      //loadprojects();
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  checkExpired();
	

});
