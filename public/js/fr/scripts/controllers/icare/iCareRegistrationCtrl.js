


app.controller("iCareRegistrationCtrl", function ($scope, $http, Countries, MDY, MDYform, $timeout) {
  console.log("registered");
  $scope.usernametaken = '';
  $scope.emailtaken = '';
  $scope.notyetconfirm = '';
  $scope.notyetcompleted = '';
  $scope.success = false;
  $scope.process = false;
  $scope.countries = Countries.list();
  $scope.dataCentername = '';
  $scope.day = MDY.day();
  $scope.month = MDY.month();
  $scope.year = MDY.year();
  $scope.passmin = false;
  $scope.invalidemail = false;
  $scope.emailtaken = false;
  $scope.notyetconfirm = false;
  $scope.notyetcompleted = false;
  $scope.comparepass = false;

  $scope.retypepass = function (user) {
    console.log(user.password);
    console.log(user.repassword);
    if(user.password != user.repassword){        
      $scope.comparepass = true;
    }else{        
      $scope.comparepass = false;
    }       
  }; 

    
  $scope.emailcheck = function (user) {
    console.log("checking for email");
    console.log(user.email);
    $scope.invalidemail = false;
    $scope.emailtaken = false;
    $scope.notyetconfirm = false;
    $scope.notyetcompleted = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.invalidemail = true;
    }else{                   
      $scope.invalidemail = false;
    }

    if(user.email == undefined){
      $scope.invalidemail = false;
    }

    $http.post(API_URL + '/members/registration/check', user).success(function (response) {

      if (response.hasOwnProperty('emailtaken') || response.hasOwnProperty('notyetconfirm') || response.hasOwnProperty('notyetcompleted') ) {

        if (response.hasOwnProperty('emailtaken')) {
          $scope.emailtaken = true;
        } else {
          $scope.emailtaken = false;
        }
        if (response.hasOwnProperty('notyetconfirm')) {
          $scope.notyetconfirm =true;
        } else {
          $scope.notyetconfirm = false;
        }
        if (response.hasOwnProperty('notyetcompleted')) {
          $scope.notyetcompleted = true;
        } else {
          $scope.notyetcompleted = false;
        }
        $scope.process = false;
        $scope.passmin = false;
      }

    });
  };

  //for resendconfirmationAction
  $scope.chtr = function (user) {
    $http.post(API_URL + '/members/confirmation/resend', user).success(function (response) {
      if (response.hasOwnProperty('confirmation')) {
        $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <br/><span style="color:#000;font-size:30px;"></span> <div style="margin-top:-10px;padding:20px;font-size:15px;">The confirmation email has been successfully re-send to your email. Please check your inbox.<a href="" onclick="javascript:window.location.reload()"  tyle="position: absolute; top: 15px; right: 30px; text-align:center;">Close</a></div></div>', {
          opacity: 80,
          overlayCss: {backgroundColor: "#fff"},
          overlayClose: false
        });
      }  
      $scope.response = response;
    });
  };
  //for resendcompletion
  $scope.chtrc = function (user) {

    $http.post(API_URL + '/members/completion/resend', user).success(function (response) {
      if (response.hasOwnProperty('completion')) {
        $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <br/><span style="color:#000;font-size:30px;"></span> <div style="margin-top:-10px;padding:20px;font-size:15px;">The confirmation email has been successfully re-send to your email. Please check your inbox.<a href="" onclick="javascript:window.location.reload()" style="position: absolute; top: 15px; right: 30px; text-align:center;">Close</a></div></div>', {
          opacity: 80,
          overlayCss: {backgroundColor: "#fff"},
          overlayClose: false
        });
      }  
      $scope.response = response;  
    });
  };

  // Here starts the 
  $scope.typetransact2 = '';
  $scope.userecheckreg = {};
  $scope.userccreg = {};
  $scope.pp = {};
  $scope.errorpayment='';

  var d = new Date();
  var n = d.getFullYear();
  $scope.donday = MDYform.day();
  $scope.donmonth = MDYform.month();
  $scope.donyear = MDYform.year();

  $scope.t3 = '';
  $scope.a3 = '';
  $scope.srt = '';

  $scope.t3paypal = [
  {'text': 'Daily', 'val': 'D'},
  {'text': 'Monthly', 'val': 'M'},
  ]

  // load centername to select item from centername table
  $http({
    url: API_URL + "/bnb/cnamelist",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {
    $scope.dataCentername = data;
  }).error(function (data) {
    $scope.status = status;
  });
  $scope.passmin = false;
  var oriUser = angular.copy($scope.user);
  var oriuserccreg= angular.copy($scope.userccreg);
  var oriuserecheckreg= angular.copy($scope.userecheckreg);

  //only  mail chimp
  $scope.regWd = function (email, fname, lname) {
    console.log(email,fname,lname)
    var info = [];
    info['email'] = email;
    info['fname'] = fname;
    info['lname'] = lname;
    console.log(info);
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email);

    if(email==null || email==undefined || fname==null || fname==undefined || lname==null || lname==undefined){
      $scope.invalidemail = false;
      $scope.required = true;
      if(validemail == false){
          $scope.invalidemail = true;
        }
    }
      else{
        $scope.required = false;
        // else{                   
          $scope.invalidemail = false;
          $scope.process = true;
          $http.post(API_URL + '/members/registration/newsletter', info).success(function (response,data) {
            console.log(data);
            console.log(response);
            if(response.sent == "ok"){ 
              $scope.process = false;       
              window.location = "/iCare2020/registered";
            }
          });
        // }
      }
  }
  $scope.register = function (user, amount, reg_inden, typetransact2) {
    console.log(user);
    console.log(amount);
    console.log(reg_inden);
    console.log(typetransact2);
    user['reg_inden'] = reg_inden;

    if (user.password.length < 6) {
      $scope.passmin = true;
      return
    } else {
      $scope.process = true;
      $http.post(API_URL + '/members/registration/check', user).success(function (response) {

        if (response.hasOwnProperty('usernametaken') || response.hasOwnProperty('emailtaken')) {
          if (response.hasOwnProperty('usernametaken')) {
            $scope.usernametaken = response.usernametaken;
          } else {
            $scope.usernametaken = '';
          }
          if (response.hasOwnProperty('emailtaken')) {
            $scope.emailtaken = response.emailtaken;
          } else {
            $scope.emailtaken = '';
          }
          $scope.process = false;
          $scope.passmin = false;
        } else {
          // CREDIT CARD CHECK IF CREDIT CARD
          if(typetransact2 == ''){
            $scope.userccreg['amount'] = amount;
            $scope.userccreg['email'] = user.email;
            $scope.userccreg['fName'] = user.fname;
            $scope.userccreg['lName'] = user.lname;
            $http.post(API_URL + '/donate/creditcard', $scope.userccreg).success(function (response) {
              console.log(response);
              if($scope.userccreg.recurpayment){
                if(response.code=='I00001'){
                  $scope.back_button = 1;
                  saveUser(user, amount);
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.text;
                }
              }else{
                if(response.hasOwnProperty('success')){
                  $scope.back_button = 1;
                  saveUser(user, amount);
                }else if(response.error == null){
                  $scope.process = false;
                  $scope.errorpayment  = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                }else{
                  $scope.process = false;
                  $scope.errorpayment  = response.error;
                }
              }
            });
          }else if(typetransact2=='ach2'){
            $scope.userecheckreg['amount'] = amount;
            $scope.userecheckreg['email'] = user.email;
            $scope.userecheckreg['fName'] = user.fname;
            $scope.userecheckreg['lName'] = user.lname;
            $http.post(API_URL + '/donate/echeck', $scope.userecheckreg).success(function (response) {
              if($scope.userecheckreg.recurpayment2){
                if(response.code=='I00001'){
                  saveUser(user, amount);
                  $scope.reccur_message = "Your donation has been successfully processed.";
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.text;
                }
              }else{
                if(response.hasOwnProperty('success')){
                  saveUser(user, amount);
                  $scope.reccur_message = response.success;
                }else if(response.error == null){
                  $scope.process = false;
                  $scope.errorpayment= 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.error;
                }
              }
            });
          }else if(typetransact2=='paypal2'){            
            var tempID =  Math.random().toString(36).substring(7) + new Date().getTime();
            var customval = tempID + " icare";
            $('#paypalcustom').val(customval);
            user['tempID'] = tempID;
            saveTempUser(user, amount);            
          }
        }
        $scope.response = response;
      });
    }
  }

  var saveTempUser = function(user,amount){

    return $http.post(API_URL + '/members/tempReg', user).success(function (response) {
      $scope.usernametaken = '';
      $scope.emailtaken = '';
      $scope.success = true;      
      if($scope.pp.recurpayment3){
        $('#paypalreoccur').submit();
      }else{
        $('#paypalsingle').submit();
      }
      
    });
  }

  var saveUser = function(user, amount){

    return $http.post(API_URL + '/members/registration', user).success(function (response) {
      $scope.usernametaken = '';
      $scope.emailtaken = '';
      $scope.success = true;      
      $scope.user = angular.copy(oriUser);
      $scope.userccreg = angular.copy(oriuserccreg);
      $scope.userecheckreg = angular.copy(oriuserecheckreg);
      window.location = "/registrationcomplete/" + amount;      
    });
  }


});
