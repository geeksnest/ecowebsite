'use strict';

/* Controllers */

app.controller('HeroesOnDutyCtrl', function ($scope, $location, $http,  store, jwtHelper, $window, Config, MDYform, CallheroesFactory) {
  $('form').validate({
    rules: {
      require: {
        required: true
      },
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true
      },
      skill: {
        required: true
      },
      service: {
        required: true
      },
      commit: {
        required: true
      },
      date: {
        required: true
      },
      day: {
        required: true
      },
      month: {
        required: true
      },
      selyear: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

 $scope.gndr = function(_gdata){

    $scope.gender = false;
  }

$scope.saved =function(_gdata){
  $scope.alerts=[];
  if(_gdata == undefined || Object.keys(_gdata).length!=11){
    $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
    try {
      if($scope.user.gender){
       $scope.gender = false;
     }else{
        $scope.gender = true;
     }
   }
   catch (e) {
    $scope.gender = true;
  }
  try {
    if($scope.user.byear && $scope.user.bmonth && $scope.user.bday){
     $scope.nbday = false;
   }else{
    $scope.nbday = true;
  }
}
catch (e) {
  $scope.nbday = true;
}
}else if(
      _gdata.name==""         || _gdata.name== undefined      || _gdata.name== null 
      || _gdata.email==""     || _gdata.email== undefined     || _gdata.email== null 
      || _gdata.phone==""     || _gdata.phone== undefined     || _gdata.phone== null 
      || _gdata.leadtrain=="" || _gdata.leadtrain== undefined || _gdata.leadtrain== null 
      || _gdata.skill==""     || _gdata.skill== undefined     || _gdata.skill== null 
      || _gdata.service==""   || _gdata.service== undefined   || _gdata.service== null  
      || _gdata.commit==""    || _gdata.commit== undefined    || _gdata.commit== null 
      ){
     $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
}else{

  $scope.alerts.splice(0, 1);
  $scope.isSaving = true;
  $scope.loader = true;
  CallheroesFactory.saveHeroesOnDuty(_gdata, function(_rdata){
    if(_rdata.success){
      $scope.gender = false;
      $scope.nbday = false;
       $scope.loader = false;
      $scope.isSaving = false;
      $scope.form.$setPristine();
      $scope.user = "";
      $scope.alerts =[{type: 'success', msg: 'Your Request has been Sent!'}];
    }
  });
}
$scope.closeAlert = function (index) {
  $scope.alerts.splice(index, 1);
};

}

}).controller('HeroesCallCtrl', function ($scope, $location, $http,  store, jwtHelper, $window, Config, MDYform, CallheroesFactory) {
  $('form').validate({
    rules: {
      organization: {
        required: true
      },
      taxid: {
        required: true
      },
      phone: {
        required: true,
      },
      address: {
        required: true
      },
      helplocaiton: {
        required: true
      },
      reasonhelp: {
        required: true
      },
      whyhelp: {
        required: true
      },
      qualification: {
        required: true
      },
      agree: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $scope.chkbox= function(_gdata){

    if(_gdata){
      $scope.chkerror = true;
    }else{
       $scope.chkerror = false;
    }

  }



  $scope.callHereSave =function(_gdata){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    if(_gdata == undefined || Object.keys(_gdata).length!=9){
      $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
      $scope.chkerror = true;
    }else if(!_gdata.agree){
      $scope.alerts =[{type: 'danger', msg: 'Click the checkbox to certify that the above information is correct.'}];
      $scope.chkerror = true;
    }else if(
      _gdata.address==""         || _gdata.address== undefined      || _gdata.address== null 
      || _gdata.helplocaiton=="" || _gdata.helplocaiton== undefined || _gdata.helplocaiton== null 
      || _gdata.organization=="" || _gdata.organization== undefined || _gdata.organization== null 
      || _gdata.phone==""        || _gdata.phone== undefined        || _gdata.phone== null 
      || _gdata.qualification==""|| _gdata.qualification== undefined|| _gdata.qualification== null 
      || _gdata.reasonhelp==""   || _gdata.reasonhelp== undefined   || _gdata.reasonhelp== null 
      || _gdata.taxid==""        || _gdata.taxid== undefined        || _gdata.taxid== null  
      || _gdata.whyhelp==""      || _gdata.whyhelp== undefined      || _gdata.whyhelp== null 
      ){
     $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
   }else{
    $scope.alerts.splice(0, 1);
    $scope.chkerror = false;
    $scope.isSaving = true;
    $scope.loader = true;
      CallheroesFactory.saveHeroes(_gdata, function(_rdata){
        if(_rdata.success){
          console.log("success");
          $scope.isSaving = false;
          $scope.loader = false;
          $scope.form.$setPristine();
          $scope.user = "";
          $scope.alerts =[{type: 'success', msg: 'Your Request has been Sent!'}];
        }
      });
}
}

}).directive('phoneInput', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

            ngModelCtrl.$parsers.push(function(viewValue) {
               if (viewValue!=undefined) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
              }
            });

            ngModelCtrl.$render = function() {
              $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
              var key = event.keyCode;
              if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                return;
              }
              $browser.defer(listener);
            });

            $element.bind('paste cut', function() {
              $browser.defer(listener);
            });
          }

        };
      }).filter('tel', function () {
        return function (tel) {
          // console.log(tel);


          if (!tel){ 
            return ''; 
          }else{
           var value = tel.toString().trim().replace(/^\+/, '');

           if (value.match(/[^0-9]/)) {
            return tel;
          }

          var country, city, number;

          switch (value.length) {
            case 1:
            case 2:
            case 3:
            city = value;
            break;

            default:
            city = value.slice(0, 3);
            number = value.slice(3);
          }

          if(number){
            if(number.length>3){
              number = number.slice(0, 3) + ' ' + number.slice(3,7);
            }
            else{
              number = number;
            }

            return ("(" + city + ") " + number).trim();
          }
          else{
            return "(" + city;
          }
        }



      };
    });
