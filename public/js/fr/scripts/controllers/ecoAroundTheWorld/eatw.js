'use strict';

/* Controllers */

app.controller('AtwCtrl', function ($scope, $location, $http,  store, jwtHelper, $window, Config, MDYform, CallheroesFactory) {
  $('form').validate({
    rules: {
      organization: {
        required: true
      },
      taxid: {
        required: true
      },
      phone: {
        required: true,
      },
      address: {
        required: true
      },
      agree: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $scope.chkbox= function(_gdata){
    if(_gdata){
      $scope.chkerror = true;
    }else{
       $scope.chkerror = false;
    }

  }

  $scope.submitatw =function(_gdata){

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    if(_gdata == undefined || Object.keys(_gdata).length!=5){
      $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
      $scope.chkerror = true;
    }else if(!_gdata.agree){
      $scope.alerts =[{type: 'danger', msg: 'Click the checkbox to certify that the above information is correct.'}];
      $scope.chkerror = true;
    }else if(
      _gdata.address==""         || _gdata.address== undefined      || _gdata.address== null 
      || _gdata.org==""          || _gdata.org== undefined          || _gdata.org== null 
      || _gdata.phone==""    || _gdata.phone==undefined    || _gdata.phone==null 
      || _gdata.orgid==""        || _gdata.orgid== undefined        || _gdata.orgid== null 
      ){
     $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
   }else{
    $scope.alerts.splice(0, 1);
    $scope.chkerror = false;
    $scope.isSaving = true;
      CallheroesFactory.atwcreate(_gdata, function(data){
         console.log(data);
        if(data.success){
          $scope.isSaving = false;
          $scope.form.$setPristine();
          $scope.user = "";
          $scope.alerts =[{type: 'success', msg: 'Your information successfully sent.'}];
        }
      });
}
}

}).controller('FaCtrl', function ($scope, $location, $http,  store, jwtHelper, $window, Config, MDYform, CallheroesFactory) {
 
  $('form').validate({
    rules: {
      organization: {
        required: true
      },
      add1: {
        required: true
      },
      add2: {
        required: true
      },
      city: {
        required: true,
      },
      state: {
        required: true
      },
      zip: {
        required: true
      },
      country: {
        required: true
      },
      year: {
        required: true
      },
      web: {
        required: true
      },
      mission: {
        required: true
      },
      contactperson: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true
      },
      reason: {
        required: true
      },
      agree: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $scope.chkbox= function(_gdata){
    if(_gdata){
      $scope.chkerror = true;
    }else{
       $scope.chkerror = false;
    }
  }

  $scope.submitfa =function(_gdata){
    console.log(_gdata.agree)
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    if(_gdata == undefined || Object.keys(_gdata).length!=16){
      $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
      $scope.chkerror = true;
      try {
        if($scope.user.year){
         $scope.nbday = false;
       }else{
        $scope.nbday = true;
      }
    }
    catch (e) {
      $scope.nbday = true;
    }
    try {
      if($scope.user.cat){
       $scope.n = false;
     }else{
      $scope.n = true;
    }
  }
  catch (e) {
    $scope.n = true;
  }
    }
    else if(!_gdata.agree || _gdata.agree==false){
      $scope.alerts =[{type: 'danger', msg: 'Click the checkbox to certify that the above information is correct.'}];
      $scope.chkerror = true;
    }else{
    $scope.alerts.splice(0, 1);
    $scope.chkerror = false;
    $scope.isSaving = true;
    $scope.loader = true;
      CallheroesFactory.facreate(_gdata, function(data){
         console.log(data);
        if(data.success){
          $scope.isSaving = false;
          $scope.loader = false;
          $scope.form.$setPristine();
          $scope.user = "";
          $scope.alerts =[{type: 'success', msg: 'Thank you. Your information has been received. We will contact you shortly.'}];
        }

      });

  }
}

}).directive('phone', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

            ngModelCtrl.$parsers.push(function(viewValue) {
              // console.log(viewValue)
              if (viewValue!=undefined) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            }

            
            });

            ngModelCtrl.$render = function() {
              $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
              var key = event.keyCode;
              if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                return;
              }
              $browser.defer(listener);
            });

            $element.bind('paste cut', function() {
              $browser.defer(listener);
            });
          }

        };
      }).filter('tel', function () {
        return function (tel) {
          // console.log(tel);

          if (!tel){ 
            return ''; 
          }else{
           var value = tel.toString().trim().replace(/^\+/, '');

           if (value.match(/[^0-9]/)) {
            return tel;
          }

          var country, city, number;

          switch (value.length) {
            case 1:
            case 2:
            case 3:
            city = value;
            break;

            default:
            city = value.slice(0, 3);
            number = value.slice(3);
          }

          if(number){
            if(number.length>3){
              number = number.slice(0, 4) + ' ' + number.slice(4,7);
            }
            else{
              number = number;
            }

            return ("(" + city + ") " + number).trim();
          }
          else{
            return "(" + city;
          }
        }



      };
    });
