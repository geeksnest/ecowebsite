'use strict';

/* Controllers */
app.controller("Clubctrl", function ($http, $scope, $window, $parse, $sce) {
  console.log("Clubctrl list");
  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomoreclubs = false;

  var list = [];
  $scope.base_slugs = 'earth-citizens-clubs';
    // list news

    $scope.data = {};
    
    $scope.currentstatusshow = '';

    var loadclubs = function () {

      $http({
        url: API_URL+"/feclubs/clublist/" + num + '/' + offset + '/' + page,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data;

        if(data.length < 10){
          $scope.hideloadmore = true;
          $scope.nomoreclubs = true; 
        } else if (data.total_items <= page){
          $scope.nomoreclubs = true;   
          $scope.hideloadmore = true;
        } else {
          $scope.loading = false;
        }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadclubs();

  $scope.showmore = function(){
    $scope.loading = true;
    page = page + page;
    loadclubs();
  }   

}).controller("ClubDetailsctrl", function ($http, $scope, $window, $parse, $sce) {
  console.log("ClubDetailsctrl list");
  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomoreclubs = false;

  var list = [];
  $scope.base_slugs = 'earth-citizens-clubs';
    // list news

    $scope.data = {};
    
    $scope.currentstatusshow = '';

  //   var loadclubs = function () {

  //     $http({
  //       url: API_URL+"/feclubs/clubinfo/" + slugs,
  //       method: "GET",
  //       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  //     }).success(function (data, status, headers, config) {

  //       // console.log(data);
  //       $scope.data = data;

  //       if(data.length < 10){
  //         $scope.hideloadmore = true;
  //         $scope.nomoreclubs = true; 
  //       } else if (data.total_items <= page){
  //         $scope.nomoreclubs = true;   
  //         $scope.hideloadmore = true;
  //       } else {
  //         $scope.loading = false;
  //       }

  //   }).error(function (data, status, headers, config) {
  //     $scope.status = status;
  //   });
  // }
  // loadclubs();

  $scope.showmore = function(){
    $scope.loading = true;
    page = page + page;
    loadclubs();
  }   

});