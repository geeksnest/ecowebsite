app.factory('User', function($http, store, jwtHelper, Config, Upload){
    return {
        usernameExists: function(data, callback) {

            $http({
                url: API_URL + '/agent/usernameexist/' + data.username,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            return;
        },
        emailExists: function(data, callback) {
            $http({
                url: API_URL + '/agent/emailexist/' + data.email,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
            return;
        },
        register:function(user, callback){
            $http({
                url: API_URL + '/agent/register',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        login: function(user, callback){
            $http({
                url: API_URL + '/agent/login',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        getInfo: function(id, callback) {
            $http({
                url: API_URL + '/agent/get/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        uploadagentpic: function(file, id, callback) {
            var promises;
            promises = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/agentpic/' + id + '/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).then(function () {
                $http({
                    url: API_URL +'/agent/updateprofilepic',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ id: id, filename:file.name})
                }).success(function (data, status, headers, config) {
                    callback(data);
                }).error(function (data, status, headers, config) {
                    callback({'error':true});
                });
            })
        },
        updateprofile: function(data, callback) {
            $http({
                url: API_URL + '/agent/updateprofile',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        changeusername: function(id, data, callback) {
            $http({
                url: API_URL + '/agent/frontend/updateAcct/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        chckpassword: function(id, data, callback) {
            $http({
                url: API_URL + '/agent/checkpassword/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        changePass: function(id, data, callback) {
            $http({
                url: API_URL + '/agent/changepassword/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        loadgallery: function(id, callback) {
            $http({
                url: API_URL + '/agent/loadgallery/' + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        deleteMedia: function(id, data, callback){
            $http({
                url: API_URL + "/agent/deleteprofpic/" + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
    }
});
