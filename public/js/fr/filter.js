'use strict';

/* Controllers */

app.filter('returnYoutubeThumb', function(){
 
  return function (item) {
    if(item){
      var newdata = item;
      var x;
      x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      return 'http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
    }else{
      return x;
    }
  };
});

app.filter('returnImageThumb', function(appConfig){
  return function (item) {
    if(item){
      return appConfig.ImageLinK + "/uploads/newsimage/" + item;
    }else{
      return item;
    }
  };
});

app.filter('returnTrustedhtml', function($sce){
  return function (item) {
    return $sce.trustAsHtml(item);
  };
});

app.filter('putcomma', function(){    
  return function (item) {
    if(item){
      var parts = item.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
 };
});

app.filter('charToint', function(){    
  return function (item) {
    if(item){
      return parseInt(item);
    }
 };
});

app.filter('noteUnread', function(){    
  return function (item) {
    if(item == 0){
      return 'note-unread';
    }else{
      return '';
    }
  };
});

app.filter('dateToWord', function(){    
  return function (item) {
    if(item){
      var x = item.toString().split(" ");
      var y = x[0].toString().split("-"); 
      var month;      

      if(y[1] == '01'){
        month = 'Jan';
      }
      else if(y[1] == '02'){
        month = 'Feb';
      }
      else if(y[1] == '03'){
        month = 'Mar';
      }
      else if(y[1] == '04'){
        month = 'Apr';
      }
      else if(y[1] == '05'){
        month = 'May';
      }
      else if(y[1] == '06'){
        month = 'Jun';
      }
      else if(y[1] == '07'){
        month = 'Jul';
      }
      else if(y[1] == '08'){
        month = 'Aug';
      }
      else if(y[1] == '09'){
        month = 'Sep';
      }
      else if(y[1] == '10'){
        month = 'Oct';
      }
      else if(y[1] == '11'){
        month = 'Nov';
      }
      else if(y[1] == '12'){
        month = 'Dec';
      }
      return month +" "+ y[2] +", "+ y[0];
    
    
    }
 };
});

app.filter('capfirst', function() {
    return function(input) {
      // return input.charAt(0).toUpperCase() + input.substr(1).toLowerCase();
      return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
});


app.filter('day', function() {
  return function (item) {
    if(item){

      var newdata = item;
      var x;
      x = newdata.slice(8, 10);
      return x;
    }else{
      return x;
    }
  };
});

app.filter('year', function() {
  return function (item) {
    if(item){
      var newdata = item;
      var x;
      x = newdata.slice(0, 4);
      return x;
    }else{
      return x;
    }
  };
});

app.filter('getmonth', function() {
  return function (item) {
    if(item){
      var newdata = item;
      var x;
      x = newdata.slice(5, 7);
      if(x == '01'){
        y = 'Jan';
      }
      else if(x == '02'){
        y = 'Feb';
      }
      else if(x == '03'){
        y = 'Mar';
      }
      else if(x == '04'){
        y = 'Apr';
      }
      else if(x == '05'){
        y = 'May';
      }
      else if(x == '06'){
        y = 'Jun';
      }
      else if(x == '07'){
        y = 'Jul';
      }
      else if(x == '08'){
        y = 'Aug';
      }
      else if(x == '09'){
        y = 'Sep';
      }
      else if(x == '10'){
        y = 'Oct';
      }
      else if(x == '11'){
        y = 'Nov';
      }
      else if(x == '12'){
        y = 'Dec';
      }
      return y;
    }else{
      return x;
    }
  };
});